#!/bin/bash

title=$(echo $1 | sed 's/ /-/g' | tr '[:upper:]' '[:lower:]') # replace potential spaces with dashes

t=$(date +%Y-%m-%d) # get current date

hugo new posts/$title.md


# move after creating the file (otherwise it will have the wrong name)
mv content/posts/$title.md content/posts/$t-$title.md

echo "Renamed to content/posts/$t-$title.md"

open content/posts/$t-$title.md