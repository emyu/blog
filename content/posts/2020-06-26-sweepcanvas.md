---
title: "SweepCanvas"
date: 2020-06-26T16:04:08+02:00
draft: false
tags: ["Paper"]
summary: "In context sketching to create 3D models in the context of an indoor environment from one picture of said environment (RGB-Depth)."
link: ""
---

[Video demo](https://www.youtube.com/watch?v=Xnp3_eMYXj0)

## Principle

Users draw planar curves to create swept surfaces: they draw both a profile curve and a trajectory curve along which the profiel curve is swept to create the surface.

Their method finds suitable candidate planes on which to place these planar curves so that they can figure out where they lie in depth, in the picture.

## Method

* Cluster pixels to a number of extracted planes, for a given picture
* When the user draws a profile stroke and a trajectory stroke, the system will try to assign the best pair of planes from the candidate planes. They consider criteria such as how much the stroke lies in a plane (not super clear what they mean by that), and the fact that the 2 planes are probably orthogonal. In this selection of planes, they don't fix position necessarily, only plane normal direction. They try to infer position of the plane in space by other types of snapping, such as snapping to edges and corners of objects in the image.

## Evaluation

* Evaluating their plane candidates extraction method against other methods such as RANSAC, and a baseline system where the user manually specifies the planes. They show that their method is superior by looking at some quantitative measures during use such as time to complete a task and number of undos.
* Pilot study with users of different level of experience wrt 3D modelling, they measure actions count again, on 2 tasks with a target and 1 free task.
