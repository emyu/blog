---
title: "Dynamic Deep Octree for High Resolution Volumetric Painting in Virtual Reality"
date: 2022-02-21T15:26:31+01:00
draft: false
tags: ["Paper"]
summary: "Volumetric 3D painting with an efficient Octree representation."
link: "http://graphics.ewha.ac.kr/canvox/"
---

* Starts from the assessment that surface based VR painting is unsatisfactory regarding color mixing, stroke recoloring, erasing, stroke intersections
* The challenge is to maintain an octree for efficient rendering of the strokes (by raycasting in the volumetric scene), while allowing for interactive creation of new strokes
  * Needs an efficient structure to access the neighboring cells of a voxel (to follow a ray)
  * Needs an efficient way to update the tree locally when a stroke is added (it is too slow to recreate the whole tree)
* They have both a CPU-side and GPU-side octree, where the CPU octree is dynamically updated, and then the updated cells of the tree are sent to the GPU and updated there while preventing rendering latency
* Also since their volume is huge, they prevent numerical error propagation in ray casting by working in cell coordinates instead of world coordinates