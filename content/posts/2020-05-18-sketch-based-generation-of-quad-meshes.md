---
title: "Sketch Based Generation and Editing of Quad Meshes"
date: 2020-05-18T11:01:32+02:00
draft: false
tags: ["Paper"]
summary: "Interface to control edge flows and singularities on quad meshes of existing 3D models by sketching curves corresponding to edge flow directions."
link: "http://research.nii.ac.jp/~takayama/sketch-retopo/sketch-retopo-siggraph2013.pdf"
---

[Video demo](https://www.youtube.com/watch?v=-OTl_TtAm1o)

The user can sketch on the surface to define edges of quad patches. Quad patches are generated with previous methods, with user controllable number of subdivisions per patch edge. The user can also move around the singularities if there are any.

Adding to this interaction, the authors also propose features to make patch creation faster:

* Spine sketching to define a patch by sketching its center-line and setting its radius. The patch created in this way snaps to nearby patch edges or corners.
* Autocompletion: real-time suggestions of likely patch candidates in the proximity of existing patch boundaries
* Cylinder sketching: automatically create strokes around a cylindrical feature once 2 loops are joined by a stroke. A disk patch corresponding to the cap of the cylinder is also automatically added. This can be useful for small bumps too.
* Automatic alignement of strokes to geometric features
* Corner type editing

## Implementation

* Curve network is represented in a manner similar to half-edges on a triangle mesh, each curve segment between intersections having 2 halfchains.
* Loop orientation detection: make sure that a loop is oriented counterclockwise when viewed from the exterior of the surface to avoid generating 2 overlapping patches.
* They parameterize the surface near the input stroke such that the parameterization follows the stroke (http://papervideos.s3.amazonaws.com/StrokeParamEG13.pdf). This allows them to measure nearby curves in this 2D parameter space.
