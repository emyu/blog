---
title: "Structure Preserving Reshape for Textured Architectural Scenes"
date: 2020-03-06T10:11:10+01:00
draft: false
tags: ["Paper"]
summary: "Interactive mesh and texture deformation based on user input while keeping the original look of the mesh and its texture."
link: "https://onlinelibrary.wiley.com/doi/pdf/10.1111/j.1467-8659.2009.01386.x"
---

## Mesh deformation

With this method the user can deform an architectural model by moving handles on the mesh to new position. Then a new position for all vertices of the mesh will be solved while minimising change in edge directions (keep angles between planar faces), changes in coplanarity (contacts between different parts of the mesh), and edge length changes.

Their method divides those constraints into soft and hard constraints: edge direction is a hard constraint while edge length is a soft one. However, they tolerate small inaccuracies in the hard constraints so they simply use a large weighting term on the hard constraints and add them to the least square optimization.

## Optimization 

They solve their optimization by pre-factoring the sparse left hand side matrix from the normal equation (defined by the constraints, needs only to be computed once on the whole mesh) using Cholesky factorisation. This allows for interactive solving.

## Texture deformation

To generate a believable texture for the deformed mesh, they deform the initial texture such that stretching is focused on the regions where the texture is stochastic. To find the correct texture mapping (grid of texture coordinates) to do so, they solve an optimization to minimize deformation of the edges of the grid in zones and direction where the texture varies a lot.

To further preserve and repeat details of the texture in a believable way, they extract detail tiles from the original texture and put them back on the texture after the deformation.

