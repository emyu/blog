---
title: "Video Doodles Examples Classification"
date: 2022-03-21T15:33:14+01:00
draft: false
tags: ["Video Doodles"]
summary: "List all kinds of video doodles observed in the examples, with the goal of establishing a classification."
link: ""
---



| Example clip                                                 | Camera | Canvases | Canvas anchor(s) wrt scene                                   | Canvas geometry(s)                                           | Sketch 2D animation                                          | Specific difficulty?                                         |
| ------------------------------------------------------------ | ------ | -------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| [Musicians playing in a band](https://youtu.be/W3lHbVSL_84?t=14) | Fixed  | Multiple | Fixed                                                        | Planar (view-facing)                                         | yes                                                          |                                                              |
| [People sitting in restaurant](https://youtu.be/W3lHbVSL_84?t=17) | Fixed  | Single   | Fixed                                                        | Planar (view-facing)                                         | yes                                                          |                                                              |
| [Metro with fashion show](https://youtu.be/zU9sPFMTX9g?t=7)  | Moving | Multiple | Fixed (background, people sitting) and moving in depth (models) | Planar (diverse orientations)                                | yes                                                          | Occlusion effect between sketches (model occludes sketched public in background) |
| [Kid playing the piano](https://youtu.be/zU9sPFMTX9g?t=22)   | Moving | Multiple | Fixed                                                        | Planar (piano) and potentially curved (background)           | yes (complex animation on the piano to make it look 3D)      | Occlusion effect between sketches (piano occludes background sketch)<br />Precise occlusion: sketch between hand and table |
| [Kid with stethoscope](https://youtu.be/it-Vt2M_-ZU?t=9)     | Moving | Multiple | Moving (rotation), tracking face                             | Head thing could be curved all around the head, planar stethoscope | depends what is done by auto 3D effects (head band rotation could be faked) | Precise occlusion: sketch between hand and body<br />Very 3D motion, rotation |
| [Mom with cape](https://youtu.be/it-Vt2M_-ZU?t=17)           | Moving | Single   | Moving (translation with depth), tracking body               | Planar (with 2D animation for the cape) or 3D animated cape  | yes (cape unrolling and wiggles)                             |                                                              |
| [Elevator diving](https://youtu.be/it-Vt2M_-ZU?t=27)         | Fixed  | Multiple | Moving (translation in screen space), tracking moving object + free transformations (like the fish) | Planar (view facing)                                         | yes                                                          | Occlusion effect between sketches (fish occludes diver)      |
| [Truck on road](https://youtu.be/it-Vt2M_-ZU?t=38)           | Moving | Single   | Moving (translation), tracking moving object                 | Planar (view facing)                                         | no (very little)                                             |                                                              |
| [Girl writing (close up)](https://youtu.be/it-Vt2M_-ZU?t=56) | Moving | Multiple | Fixed (writing on paper) and moving (pen)                    | Planar (some orientation for the paper, and view facing for the pen) | yes                                                          | Precise occlusion: sketch between different fingers          |
| [Man walking up stairs](https://youtu.be/it-Vt2M_-ZU?t=96)   | Moving | Multiple | Moving (translation) tracking body (legs)                    | Planar (view facing)                                         | no (very little)                                             |                                                              |

Anything with a fixed camera and simple occlusions can get away with fixed planar canvases.

Where do we draw the limit between "3D effects" that are powered by the method VS those that are done via 2D sketching? Eg: [the book page turn](https://youtu.be/zU9sPFMTX9g?t=3) can be seen as a pure 2D animation, or as a 3D effect where one canvas (the page) occludes another (the first part of the book), the piano 3D aspect, the kid doctor head band, the cape.

=> we need to keep it scoped, focus more on the aspects of solving for transformations of the canvases in space, rather than very complex canvas geometries. Differentiate from sketch-based modelling, Sweep Canvas, etc.