---
title: "Zippables"
date: 2021-04-23T16:13:14+02:00
draft: false
tags: ["Paper"]
summary: "Fabricating 3D objects with fabric that assemble via a zipper. They propose a method to generate a curved path for the zipper on a target surface."
link: ""
---

* The zipper curve must cover the surface uniformly, and they shouldn't curve excessively
* They observe that it is easy to create such a curve on a cylinder, by simply spiralling up the cylinder with regular slope
* To deal with complex objects they first decompose the object in cylindrical parts (manually). Each cylindrical part is parameterized to satisfy some conditions (eg: alignment on both sides of the cut up cylinder, flat bottom and top sections, alignment between the parameterization of neighboring parts) while minimizing distortions between the 3D and flat states.
* Most parts do not have open cylinder topology, so they have one "boundary" of the cylinder that is just a point or line on the surface (callend "open boundary"), and the other boundary is the interface with another part.
* Tracing the single full zipper curve is done by following a path that goes through all the parts (chosen manually among proposals) in a specific way, adding Fermat spirals when encountering an open boundary.

