---
title: "Some sketches"
date: 2020-02-25T17:53:35+01:00
draft: false
tags: ["VR Sketching Project"]
summary: "Some quick sketches I did to get a better grasp of the current state of the project, the capabilities of the snapping implemented and reflect on 'what comes next'"
link: ""
---

{{< figure src="media/02-25/iron-sketch.gif" caption="Half iron" width="600px">}}

{{< figure src="media/02-25/boat-sketch.gif" caption="A toy boat" width="600px">}}

{{< figure src="media/02-25/lilibug.gif" caption="Asymetric ladybug" width="600px">}}
