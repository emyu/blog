---
title: "Lazy Brush"
date: 2020-11-18T18:06:09+01:00
draft: false
tags: ["Paper"]
summary: "Interactive coloring tool for bitmap sketches. They pose the problem as finding a labelling for each pixel that minimizes an energy combining smoothness of the labelling across nearby pixels and a data term that corresponds to the user-labelled pixels using scribbles of color. They solve this as a multiway cut problem, with a fast iterative approximation."
link: "https://dcgi.fel.cvut.cz/home/sykorad/lazybrush.html"
---

* Their energy is similar to Potts model: find the labelling of each pixel $c$ that minimizes $E(c) = \sum V_{p,q}(c_p, c_q) + \sum D_p(c_p)$
* The first term $V$ encourages a smooth labelling: neighboring pixels should have the same labelling except at pixels corresponding to dark outlines (the labelling discontinuity should happen precisely at these pixels)
* The data term $D$ is set according to the color scribbles that the user draws. The scribbles are soft, meaning that it's ok if the user does slight imprecisions in the labelling of a few pixels, the rule of majority they use to define $D$ guarantees that the regions are filled with a stroke color that has most of its pixels lying in the region. This term depends only on these sparse user labels, they don't attempt to match similar patterns/intensity, to respect locality of the effect of a scribble.
* The locality of the labelling also enables them to use a greedy approximation method to find the solution of the multiway cut problem in the case of more than 2 labels (their method is also used in smart scribbles). This gives them interactive results

