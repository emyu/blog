---
title: "Slices: A Shape-proxy Based on Planar Sections"
date: 2020-10-19T11:46:59+02:00
draft: false
tags: ["Paper"]
summary: "Algorithm to generate planar sections that represent a 3D model, where the choice of section planes is guided by user-generated data."
link: "http://www.dgp.toronto.edu/~mccrae/projects/slices/planarcuts.pdf"
---

## Planar sections proxies created by humans

* They ask 18 users to create models made of planar sections for a variety of models (19: planes, cups, chairs, tables, donkey, etc) in an interface where they just choose the planes and contours are created by intersecting with the model
* They give a soft max number of planes that users should respect, to show that a small number of planes is enough to represent many models
* They measure distance between planes and group nearby planes to show that most users agree on which planes to choose
* Most planes chosen are aligned with geometric features of the shape (PCA axis, ridge/valley feature, symmetry plane)
* Some user chosen planes do not match any kind of feature but are due to prior knowledge of the shape (curved seat of a chair is replaced by a single plane, leading to unfaithful proxy shape)

## Algorithm

* Plane definition and selection is done in plane-space defined by $(d, \theta, \phi)$, $(\theta, \phi)$ representing the normal vector of the plane and $d$ its position in space. They discretize that space into bins in order to aggregate fuzzy "votes" on a single plane candidate (similar to Hough Transform concept).
* They define a set of features of the shape that could generate appropriate planar sections (PCA axis, XYZ planes, ridge/valley features aligned planes, symmetry plane). Each of these feature generates either a single plane, or a "zone" of the plane-space, for example if only normal direction is constrained but not the position $d$.
* They generate all of these planes and gather weighted votes over the bins
* The selection of planes in the final proxy is done by iteratively selecting the bin with most votes, and stopping when the remaining planes have too little votes
* To determine how much weight to give to each kind of feature, they learn from the user generated data. From all the user generated models, they derive a "mean" model called super proxy by keeping only the most agreed on planes. Then they generate the initial set of all features and adjust the weights such that the planes in the proxy must be chosen (their weighted votes must be above threshold) and such that other planes must not be chosen (weighted votes below threshold). They relax these constraints by removing those that correspond to proxy planes with low feature satisfaction (meaning that they're not very significant in describing the shape)
* Evaluation: recognition performance on the generated planar proxy shapes, compared to full 3D model and artist generated proxy shape. They measure recognition success and time needed for the task.
