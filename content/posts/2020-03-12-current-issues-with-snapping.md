---
title: "Current issues with snapping"
date: 2020-03-12T16:04:18+01:00
draft: false
tags: ["VR Sketching Project"]
summary: "Illustrations of failure cases with the basic snapping method used in the prototype."
link: ""
---

{{< figure src="media/03-12/wrong-intersection-constraint.gif" caption="" width="600px">}}

Here 3 intersection constraints are detected, based on proximity between the traced curve and the existing curves and grid point. Enforcing all 3 constraints leads to this ugly result. What the user expects is that the curve should only snap to 2 places: the intersection with the nearly orthogonal stroke, and the grid point (corresponding to the endpoint of the other existing stroke).

{{< figure src="media/03-12/bad-control-point-selection.gif" caption="" width="600px">}}

Here there are 2 issues:

* The position constraint on the intersection with the previous line is applied on a badly chosen control point, so G1 continuity is lost
* This curve is close to being planar, but wasn't constrained to the plane

