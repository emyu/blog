---
title: "Design Driven Quadrangulation of Closed 3D Curves"
date: 2020-03-16T17:10:02+01:00
draft: false
tags: ["Paper"]
summary: "Method to create nice quad meshes for a given input curve network, based on how viewers envision the surface surrounded by a close loop of curves as a dense network of lines that smoothly interpolate between matching pairs of input curves."
link: "https://www.cs.ubc.ca/labs/imager/tr/2012/Quadrangulation/"
---

The authors present a method to create a quad-only mesh from any 3D curve network, provided that the cycles are known. The most interesting part for me is the quadrangulation algorithm (how to get 4-sided patches), but they also explain in more details how to find a good quad-mesh from that, while minimizing T-junctions at the boundaries of the quads.

From closed curves loop to quadrangulation:

1. Initial segmentation of the curves at the corners (curvature discontinuity).
2. Find matching pairs of curve segments, st the segment pair surrounds a zone of uniform flow. Iteratively:
   * match stable pairs (the cost of a pairing dependent on how similar the 2 segments are, how far, and most importantly how curved the resulting flow lines would be)
   * attempt to split segments from the most costly matches
3. Stop when further splitting doesn't improve significantly the average pair cost function and each segment has a match (even number of segments)
4. From the matching pairs, find intersecting poly-chords (curves connecting matching segments). Those intersections define a quad cycle (so a vertex in the dual graph of the quad network) and the segments define edges of the graph. From the graph, they deduce connectivity of the quads and can then compute the interior vertices if any.

The interior curves on the surface seen in the figures are obtained as uv-isolines using a discrete Coons formulation.

While the overall method is fairly complex to grasp, the idea that the surface can be imagined by "sweeping" segments of curves to their matching pairs is quite intuitive. Maybe we could use this idea, combined with a user interaction to indicate the match between such pairs by a connecting stroke, to recover the surface.

{{< figure src="media/design-driven-quads.png" caption="Some quad meshes from this method and from an artist with overlaid thick red stroke that could correspond to a user sketched stroke to indicate intention to create a surface" width="400px">}}
