---
title: "Video Doodles User Studies"
date: 2022-06-28T10:05:51+02:00
draft: false
tags: ["Video Doodles"]
summary: ""
link: ""
---

## Hypothesis

* H1. Most video doodles involve setting 3D trajectories in scene space.
* H2. A 2D editing interface is simpler to use for novices in 3D content creation.
* H3. Our editing interface with automatic trajectory inference in scene space is easier and faster to use than a 2D editing interface with keyframe interpolation in screen space, and than a 3D editing interface with interpolation in scene space.
* H4. Our editing interface with scene space tracking is easier and faster to use than an editing interface with screen space tracking only.
* Target users: 2D animators (familiar with After Effects, Blender grease pencil)



## How to verify

* Verifying H1: by using a classification of N videos from /r/reallifedoodles, to sort them into those that can be supported by considering only trajectory based effects (those that we support), and others.
* Verifying H2: find relevant related work, maybe sketch based modelling, ask Cuong
* Verifying H3, H4: could be done with concept testing. We talk with a few after effects users (focused on 2D animation tasks) and present them wireframes of our UI, or a somewhat staged version (eg, manually defined 3D trajectories).
* Final system test: on a single task (eg editing a video to add static in scene and moving sketches) compare 3 systems
  * Our full system with trajectory inference in scene space
  * A system with a 3D UI, where users can define 3D trajectories manually (like in Blender, AE)
  * A system with a 2D UI, where users can define 2D trajectories manually and by using 2D tracking targets (like in AE)

NB: the alternative systems can probably be implemented as sub-systems of our full system, so that the comparison can be done within the same user interface.