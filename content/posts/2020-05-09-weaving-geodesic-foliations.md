---
title: "Weaving Geodesic Foliations"
date: 2020-05-09T13:32:09+02:00
draft: false
tags: ["Paper"]
summary: "A method to compute quasi-geodesic foliations on triangle meshes. They use this method as a piece of a pipeline to create triaxial weaves from meshes."
link: "https://www.cs.utexas.edu/~josh/papers/weaving.pdf"
---

## Finding discrete geodesic foliations

(ie a family of curves on the surface that are all approximately geodesic curves)

* First they find an almost geodesic discrete vector field by constraining the field to be curl-free and minimizing its deviation from unit length (they relax the unit-norm constraint because otherwise there is no solution). They also add terms to optimize smoothness of the field.
* Then from the vector field found, they recover a periodic scalar field $\theta$ on the vertices whose level sets are geodesics. The gradient of $\theta$ must be parallel to the vector field that is a 90 degrees rotation of the previous vector field (see figure 4). They solve by first finding a rescaling $s$ of this rotated vector field such that the resulting vector field satisfies the condition to be a gradient of a scalar function on the vertices. Then they solve for $\theta$ while adjusting $s$ such that $\nabla \theta$ agrees with the rescaled vector field as much as possible.
* The geodesics are isolines of $\theta$

## Triaxial weaves design

Ribbons in a weaved objects must lie along geodesics of the target surface, as they bend with difficulty in-plane (they would twist).

* First compute a 6-RoSy field on the surface. This places singularities in a manner that minimises distortion of the field. This gives a branched cover of the surface.
* Compute geodesic foliation on each connected component of the branched cover, starting with the vector field from the 6-RoSy field
* Some tricks to extract and pair ribbons
* Physical simulation with elastic rods for the ribbons to find the final shape
