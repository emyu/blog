---
title: "Volumetric Vr Brushes Ideas"
date: 2022-02-23T17:25:06+01:00
draft: false
tags: ["Physical 3D Paint"]
summary: "More concrete ideas"
link: ""
---

## Volumetric brushes

Volumetric brushes seem necessary to be able to have

* View consistent volume look
* Paint mixing effects
* Transparency

Alternatives that we should avoid to intersect with:

* Billboarded strokes with smooth shape change depending on stroke length in screen space (Example-based brushes)
  * can reproduce traditional media very convincingly
  * it is not view consistent but maybe that does not really matter?
  * they don't show handling opacity and color mixing between overlapping strokes in the paper (not even normal alpha blending)
  * their example-based approach affords very little control to the user (the appearance does not vary according to variations in the way the brush is used)

* Generating geometry from a shader when rasterizing a tube or line mesh
  * can do some fibers, particles
  * Weaknesses: compositing will have depth fighting, no paint mixing, transparency not well handled
* Tube geometry with bump map / normal map
  * could do the oil brush look, provided with appropriate maps, could do things like dense brushes
  * cannot handle "gaps" and droplets effects, like those from sparse brush hairs

## Defining the volumetric brushes as localized cells

Inspired by approaches like NerfTex, Volumetric Billboards, we can consider volumetric cells aligned with the strokes.

Each cell is rendered in a volumetric way (either VB or ray marching) by querying a volumetric function. The function returns directly appearance data (color, alpha) and maybe a normal to do some kind of slight small scale lighting effects --> just like the micro texture of brush strokes catches the light in oil paintings.

Alternatives:

* Adaptive octree of the whole 3D space
  * can do paint mixing, erasing parts of strokes, probably things like blurring or smudging zones. Basically very similar to a 2D canvas conceptually, so there are no unexpected limitations or artifacts
  * Weaknesses: resolution is limited, at a very zoomed in stage we can see the voxels, brushes cannot have high frequency details, needs efficient implementation to be able to ray march the whole scene (adaptive octree)
* Gigavoxels
  * Would be hard to maintain such a data structure for dynamic fully volumetric stroke creation (would be akin to the adaptive octree paper I think)
  * In essence the cells we envision leverages the same broad idea: not all of space needs to have defined volume data, when there is a lot of empty space
* Dreams fully volumetric renderer
  * They can do very detailed splats that have a paint brush texture, very nice transparency effects. The overall look we can hope to achieve will probably be on-par with theirs. They can also do all volumetric editing, erasing, recoloring, sculpting etc.
  * Cons: splat-based renderer so there can't be long strokes that follow a path, each stroke is made up of short flecks. I think the flecks are 2D, but not sure.
* Adobe Medium. Find out what they can do. Seems like the look is very not painterly, very opaque, no high-frequency details.

## How to define the brushes

### Capture of real brushes

* 2D to 3D synthesis
* Could maybe work with a NerF like training, but by optimising a loss that looks at feature similarity and not direct reprojection
* Or work with slices along the main axis, like Henzler. And use VGG features

### Procedural brushes

* Procedural texture synthesis

### Capture of 3D geometry

* Fit a function / MLP to some 3D geometry (+ some boundary consistency objectives)
* Fit some 2D cross-sections

Qs:

* How to enforce continuity of the volumes across cells in the same stroke?
  * the volume of a cell is conditionned on the previous one?
  * example-based model synthesis, wave function collapse? Most of these consider a finite set of tiles/blocks to be assembled, whereas we want more like infinite variations
  * blend boundaries with Poisson blending? => need to solve a linear system, need to discretize, probably quite heavy in 3D
  * Use continuous noise functions (like Perlin noise) to generate the texture (as in Henzler paper)
  * if we have defined cross-sections, we could use Lipschitz Regularization to make the interpolating cross-sections vary smoothly from one section to the next (parameter t is along the stroke). Problem: how to define the cross-sections? A smooth interpolation may lack interesting high-frequency details?
  * tiling some looping pattern? with addition of local noise to break repetition maybe?
* How to introduce variations on parts of the stroke?
  * A stroke is subdivided into many cells, and the vertices of the cell store some attributes relating to the variations
* Do we want opaque strokes or semi transparent volumes? I am not sure we can have both, since shading (eg oil paint with shiny appearance) will need some solid surface whereas transparency is achieved through marching through a volume. Transaprency may not be super compatible with crisp high level details, since the transprency makes them less obvious.

## Brush control and variation

* 1 brush should define many variations in appearance
  * some properties could be mapped to secondary user motions, like painting speed, controller orientation
  * some properties could be driven stochastically
* Would be cool if the user can exert some control over the brush appearance after creation, eg smudging, impasto
* Ideas:
  * Procedural variations on the volumetric function? Eg displace the lookup vector with noise
  * Use filtering to create smudging effects (eg mip map levels in VB, train MLP to do filtering as in NeRFTex)
* We could have a scene level "canvas" texture that can be changed, and when rendering the stroke cells we can also read into that texture and take it into account to add variations to the brush appearance

## Potential issues / unknowns

* Rendering performance
  * will we need an extremely well optimized / hardware optimized renderer?
  * a cool looking painting should contain a truly huge amount of strokes
  * what about very big strokes? --> lots of ray marching / lots of slices to rasterize

## Reading

* Brush models in 2D
  * By capture
  * By synthesis
* Texture synthesis / Neural texture synthesis
* Art papers on VR painting
* Volumetric rendering

## Preliminary experiments

* ~~Volumetric rendering of a cell: ray tracing to the cell, then ray marching inside the cell and querying a volumetric function (eg a procedural shader)~~
* Rasterized cell with volumetric rendering inside (in fragment shader?)
* Rendering multiple intersecting cells: can color mixing work out visually?
* Train a MLP to fit some cross-sections + smoothness objective
* Can Henzler method work on strokes? Maybe we could take slices around the axis of the stroke during training