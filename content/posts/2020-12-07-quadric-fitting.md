---
title: "Quadric Fitting"
date: 2020-12-07T17:51:06+01:00
draft: false
tags: ["Surfacing Project"]
summary: "Some result on fitting quadric surfaces to stroke samples. The error metric is a first order approximation of the mean-squared distance from points to the surface."
link: ""
markup: "mmark"
---

## Quadric surface fitting

* 10 parameters in column vector $C$ (1 * 10): $$ f(p) = C^T \cdot \begin{bmatrix}1 && x && y && z && x^2 && xy && xz && y^2 && yz && z^2 \end{bmatrix}^T $$
* Using a first-order approximation for the distance to the surface $$f: dist(p, f)^2 \approx \frac{f(p)^2}{||\nabla f(p)||^2}$$
* This boils down to finding the eigenvector for the smallest eigenvalue of a generalized eigenvalue problem (10*10 matrix)
* The surface $$f$$ is implicit, so we polygonize it by marching cube to visualize it (this is the most time consuming step)

## Stroke selection UI

This web app can be used to manually select strokes (copy list of stroke IDs to clipboard), for testing purposes: http://www-sop.inria.fr/members/Emilie.Yu/CASSIE-dev/

## Some shapes can successfully be fitted by quadrics

Given a good segmentation, simple shapes can be well reproduced by a set of quadrics. If we imagine cutting up each surface at the boundary strokes and stitching them together, the result could work.

{{< figure src="media/12-07/sewing-1.png" caption="" width="300px">}}

{{< figure src="media/12-07/sewing-2.png" caption="" width="300px">}}

{{< figure src="media/12-07/sewing-3.png" caption="" width="300px">}}

However it is not clear how this "cutting" could be done. One option is to keep implicit representation and try to blend the distance fields, but we would need an inside/outside labelling for each surface. This would only support closed shapes (like the sewing machine) and not thin shells (eg: the car, the hat).

## Least-squares: sensitive to outliers

When fitting points with a quadric, gradually adding points such that some points become outliers of a quadric model yields bad result since the least-squares fitting smears error at all the data points, instead of excluding the outliers.

{{< figure src="media/12-07/car-surf-A.png" caption="A - subset of strokes (6 strokes)" width="600px">}}

{{< figure src="media/12-07/car-points-A.png" caption="A - Samples (color = error)" width="300px">}}

{{< figure src="media/12-07/car-surf-B.png" caption="B - subset of strokes (9 strokes)" width="600px">}}

{{< figure src="media/12-07/car-points-B.png" caption="B - Samples (color = error)" width="300px">}}

{{< figure src="media/12-07/car-surf-C.png" caption="C - subset of strokes (10 strokes)" width="600px">}}

{{< figure src="media/12-07/car-points-C.png" caption="C - Samples (color = error)" width="300px">}}

## Almost planar shapes

Strokes that lie close to a plane with some noise can't be fitted by a quadric model (we don't get a plane as a result). Maybe we could try first fitting a plane model to handle these cases.

{{< figure src="media/12-07/car-flat-surf.png" caption="Subset of strokes (5 strokes) that lie on a plane" width="600px">}}

{{< figure src="media/12-07/car-flat-points.png" caption="Samples" width="300px">}}

The rim of the hat can never be fitted correctly by a quadric:

{{< figure src="media/12-07/hat-surf-1.png" caption="" width="400px">}}

{{< figure src="media/12-07/hat-points-1.png" caption="Samples" width="100px">}}

{{< figure src="media/12-07/hat-surf-2.png" caption="" width="400px">}}

{{< figure src="media/12-07/hat-points-2.png" caption="Samples" width="100px">}}

{{< figure src="media/12-07/hat-surf-3.png" caption="" width="400px">}}

{{< figure src="media/12-07/hat-points-3.png" caption="Samples" width="100px">}}

However pieces of the rim could be fitted by a surface model defined by $$ (3 + 3 + 6) $$ variables:

* A supporting plane $$(\vec{n}, p_0)$$
* A height-field of quadratic form (in the local coordinate system of the plane): $$ z = a_0 + a_1 x + a_2 y + a_3 x y + a_4 x^2 + a_5 y^2 $$

