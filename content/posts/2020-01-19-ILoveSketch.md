---
title: "ILoveSketch"
date: 2020-01-19T17:03:40+01:00
draft: false
tags: ["Paper"]
summary: "**Seok-Hyung Bae et al., UIST 2008**. ILoveSketch is a 2D sketching tool to help professional designers create concepts with 3D curves, with affordances close to those of traditional pen and paper sketching."
link: "https://dl.acm.org/doi/pdf/10.1145/1449715.1449740"
---



## Sketching interface

The authors introduce a number of interaction ideas based on gesture of the pen on the tablet with semantic meaning, to do without the need for having an explicit interface with menus. Example: delete stroke is a scribble, undo is a loop gesture, confirm is a single dot.

They help sketching of curves with a drying ink metaphor: when user draws a curve, it can be corrected by redrawing it before the "ink dries". If the user corrects the curve by a new stroke, the final curve will be a weighted average of the multiple strokes. Tangential strokes (when the user "continues" an existing stroke) are connected automatically, or by redrawing their junction.

It is easier to draw in some orientations, therefore the system will automatically correct the orientation of the drawing canvas depending on a stroke, so that the inferred next stroke can be drawn more comfortably. This was found to be frustrating by the user.

## Draw curves in 3D

To resolve where the user intends to place a curve sketched in 2D in 3D space, the authors provide methods based on epipolar geometry and on projection on sketch surfaces.

2 methods based on epipolar geometry are proposed:

* From a single view point, the user sketches a pair of curves that are symmetric wrt a given plane
* The user sketches the same curve from 2 view points

Users can also draw on a pre-defined planar or curved sketch surface:

* An axis-aligned plane (xy, xz, yz)
* A surface extruded from an existing curve, extruded along an axis or along an aribtrary direction

To help users draw on a selected sketch surface, the system will automatically rotate the view to maximize sketchability (find a view where the sketch surface has a large visible projection, see Appendix for how they compute "sketchability").

## User study

Their user study focused on 1 test participant that used the system a lot. He was a professional designer. They did so because the system does have a learnign curve and is intended to be for professionals, not for casual users.
