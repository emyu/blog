---
title: "A Linear Variational System for Modelling From Curves"
date: 2020-11-13T15:46:46+01:00
draft: false
tags: ["Paper"]
summary: "Inflate a 2D sketch into a 3D mesh that can be deformed by specifying position and first derivative constraints at the curves of the sketch. Contrary to FiberMesh, they employ a linear biharmonic formulation, which is fast and more robust than FiberMesh's iterative non-linear method."
link: "http://graphics.berkeley.edu/papers/Andrews-ALV-2011-09/Andrews-ALV-2011-09.pdf"
---

* They define *Cauchy constraints* at points on the curves $u$ (that correspond to mesh vertices). These constraints correspond to a position and first derivative constraint. It is defined by introducing "ghost vertices" $g_i$, and multiple Laplacian $\Delta u_i$ (dubbed "micro-Laplacian") for each neighboring vertex $v_i$ by imagining multiple local prolongations of the mesh. The ghost vertices $g_i$ correspond to each neighbor $v_i$ mirrored across $u$. Concretely, this has the effect of giving freedom to control the surface orientation separately on each sides of the constraint vertex.
* The shape of the surface at such a Cauchy constraint point is defined by constraining the ghost vertex positions $g_i$. They also provide a way to infer $g_i$ for all the neighbors based on easily understandable parameters: crease sharpness, orientation and stretch.
* They detail the algorithm used to construct the bi-laplacian matrix with the micro-Laplacians for vertices with Cauchy constraint.
* 2D sketch to 3D mesh is done by constrained triangulation of the closed curve sketch, then specifying Cauchy constraints on some curves to inflate the shape (unconstrained curves get lifted with the mesh).
* They also propose a way to fit a set of curves to an existing mesh from which they can reconstruct approximately the surface with their forward method.
