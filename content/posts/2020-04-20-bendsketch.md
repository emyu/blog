---
title: "BendSketch"
date: 2020-04-20T16:42:15+02:00
draft: false
tags: ["Paper"]
summary: "Reconstruction of a 3D shape from an annotated 2D sketch composed of different types of strokes such as boundary, sharp feature and strokes along principal curvature directions."
link: "https://haopan.github.io/bendsketch.html"
---

* Segment strokes and classify segments as concave or convex depending on orientation of nearby segments
* Triangulate the 2D domain with the boundary and feature strokes as constraints
* Minimizing the BendField energy, they find the directions of the curvature field (using the directions of bending strokes as constraints). They solve by using the PolyVector representation and do iterative linear optimisations. More efficient than in BendField paper because discretization is coarser (mesh not pixels) and thanks to the PolyVector encoding of the cross-field.
* Iteratively optimize for the curvature magnitude scalar fields and for the surface (as a height-field over the 2D domain). Curvature magnitude is estimated at the bending strokes by using the previous surface estimate, then propagated smoothly over the domain. Surface is optimized to match the curvature field (directions and magnitudes).
* They propose [an interactive multi-view sketching application](https://www.youtube.com/watch?v=vPkt55wNZhg), by matching different surface patches sketched from multiple view points.

