

---
title: "Agile 3D Sketching With Air Scaffolding"
date: 2020-01-12T15:25:40+01:00
draft: false
tags: ["Paper"]
summary: "**Yongkwan Kim, Sang-Gyun An, Joon Hyub Lee, Seok-Hyung Bae, CHI 2018**. Using hand movements users can construct a rough 3D model made of curved pieces of planes (curve lattices). They then relies on the curves and points of the scaffold to select 2D drawing planes, on which they can draw using a pen and tablet."
link: "https://dl.acm.org/doi/pdf/10.1145/3173574.3173812"
---


## Building a scaffold from hand gestures

Leap motion system is used to track hand movements.

To create scaffolds that really match the user's intent, **meaningful motion** is separated from transitory motion (meaningful motion is detected when the movement of the palm is close to tangential to the surface described by the palm posture). Furthermore, the scaffold is created by **accumulation of similar movements**, and a patch will only be created if the user repeatedely describes it. They use the metaphor of a 3D printing nozzle instead of the hand, that can accumulate material until it solidifies or melt previously created solid parts.

The scaffold can be later modified by the user, and to help see how new parts are positionned relative to old parts, **visual feedback** appears on the scaffold in places where the hand touches the old scaffold.

## Benefits of the method

As described by users (design students) after testing.

* Describing a shape with hand movements is intuitive
* Using hand movements to start creating a shape is less daunting than starting directly from curves, it helps setting the scale and proportions of the object as a whole
* Describing the 3D shape with the scaffolds make it easier to create a concept with a minimal sketch, less strokes
* Ambiguity of the starting scaffold stimulates creativity as they can correspond to different end concepts

## Future work

* Allow users to draw 3D non planar curves by combining with other methods such as FiberMesh and ILoveSketch
* Extend to AR/VR instead of hand tracking to build the scaffolds (but still use tablet and pen to draw curves)