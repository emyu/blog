---
title: "Update: drawing on surfaces"
date: 2020-06-22T15:26:45+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "First results"
link: ""
---

## Detect detail strokes

* Drawn in vicinity of surface patch
* Edges of control points polygon are not collinear to the surface normal

{{< figure src="media/06-22/06-22-detail-stroke.png" caption="" width="400px">}}

Detail strokes are allowed to snap on stuff in the same way as other strokes.

## Constrain stroke to lie on surface patch

* Applied as a post-process to the rest of the beautification procedure

{{< figure src="media/06-22/06-22-draw-on-surface.png" caption="" width="">}}

* Constrain control points to closest point on surface (found with Unity's physics engine)

## Demo

{{< video src="media/06-22/06-22-draw-on-surface.mp4" caption="" width="">}}
