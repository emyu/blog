---
title: "3D Motion Extraction From Video"
date: 2022-06-06T15:33:19+02:00
draft: false
tags: ["Video Doodles"]
summary: ""
link: ""
---

## Extracting 2D motion from video

What we want:

* Input: a frame sequence
* Output: a list of particles/feature points tracked across multiple frames
  * 1 particle: [{frame_id, x, y}, ...]

Potential solutions:

* Optical flow
  * Dense data, 1 image per frame pair, not robust to occlusion boundaries, how to choose feature points?
  * Easy with `tuls`
  * We could try to keep only particles that have a "long enough" lifespan without crossing an occlusion boundary (detected by a sudden change of depth in time?)
* Particle video
  * Implementation?
* Colmap feature matches
  * Need to install colmap... => on sensei? or use scripts from `video-depth-playground`
  * Only pair-wise matches, I think it may ignore points on dynamic objects? To test