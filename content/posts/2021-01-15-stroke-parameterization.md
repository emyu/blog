---
title: "Stroke Parameterization"
date: 2021-01-15T14:36:10+01:00
draft: false
tags: ["Paper"]
summary: "Generate a 2D parameterization in the neighborhood of a curve lying on a 3D surface. With this parameterization, textures can be applied on the surface along the stroke, and it is possible to analyse surface properties such as curvature in 2D space, for example to snap the curve to a ridge feature."
link: "https://www.dgp.toronto.edu/~rms/pubs/StrokeParamEG13.html"
---

* The method to create a parameterization on the surface is based on approximating the exponential map in a neighborhood around the stroke. It improves on the Discrete Exponential Map method, which incrementally computes the geodesic distance and direction vector in tangent space for points away from the source $p$. It starts at the neighbors of $p$, which can be directly projected in the tangent space, then for a neighbour of $p$, $q$, the neighbors of $q$ can be projected in the tangent space of $q$ and can then be transported to the tangent space of $p$ by aligning frames with a rotation. The geodesic distance and the paths from the source to a given point are given by Djikstra on the mesh graph.
* They improve on that by removing the need to always transport back to the tangent space of the source point. Instead, they propagate the frame of this tangent space across neighbors by parallel transport. This makes the computation of a tangent space vector at a point a local operation, that depend only on the neighbors of this point and the information (frames and coordinates) that have been propagated to them from the source. It also becomes easy to deal with multiple sources, simply by taking averages of the information coming from different sources/neighbors.
* They handle overlaps of strokes by cloning the zones of the mesh where there is an overlap.