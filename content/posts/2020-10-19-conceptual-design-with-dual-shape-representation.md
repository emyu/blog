---
title: "Conceptual design and modification of freeform surfaces using dual shape representations in augmented reality environments"
date: 2020-10-19T13:25:12+02:00
draft: false
tags: ["Paper"]
summary: "Using a dual representation of point cloud and parametric surface, they enable users to visualize both and maintain a coupled representation through multiple editions of the surface."
link: "https://ideal.umd.edu/assets/pdfs/fuge_orbay_yumer_kara_JCAD2012.pdf"
---

* The user can create point clouds by moving their hand through air (with a tracked glove). They can edit the point cloud by pusing on zones of it and the modification is propagated smoothly to a defined zone
* A quadratic parametric surface is fitted to the points (fitting z from the best-fitted plane) by weighted least squares (weight = pressure value on a button while sketching with hand)
* They compute residual distance for each point, such that it will be possible to update the point positions when the surface is updated (by setting the points as their residual distance from the new surface)
* It is possible to modify the surface by sketching strokes mid-air, this creates some new points which are used to fit the surface again. The stroke points are incorporated to the curve cloud (some of them at least) by projecting them so they lie precisely on the newly fitted surface, to preserve the residual matching approach of the initial points
