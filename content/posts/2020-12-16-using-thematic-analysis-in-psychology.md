---
title: "Using Thematic Analysis in Psychology"
date: 2020-12-16T11:12:16+01:00
draft: false
tags: ["Paper"]
summary: "Psychology paper detailing a principled approach for thematic analysis of data, defining it as a valid research methodology and outlining the important aspects to be wary of when using it."
link: ""
---

* Many methods for qualitative analysis of data exist in psychology, but they are often oriented towards more specific use cases, whereas thematic analysis aims to be a general purpose tool.
* This paper seeks to formalize this method to prevent the critique that "anything goes" for such qualitative analysis.
* They establish a clear methodology to analyze the data, identify themes in it and analyze them. This is contrary to the belief that themes naturally "emerge" from the data. They emphasize the active role of the researcher in identifying the themes and reporting them.
* This process is always done with a specific research question in mind, so it is also important to be clear what this question is. However the research question can evolve through the coding process, depending on what is discovered in the data.
* A number of choices must be made to adapt the method to the particularity of each problem/research question:
  * What themes are most relevant to the research question? How prevalent must a theme be to be reported?
  * A broad description of the data (aims to report themes that reflect the entire data set) VS a more focused analysis on a small number of themes
  * Finding themes from the data VS coding the data wrt an existing theory
  * Sticking to semantic meaning of what people say VS look at the underlying ideas behind what is said
* Step by step guide (p.11-18):

{{< figure src="media/thematic-analysis-table-1.png" caption="" width="">}}

* Pitfalls to avoid (p.19-21):

{{< figure src="media/thematic-analysis-table-2.png" caption="" width="">}}
