---
title: "BendFields"
date: 2020-04-16T11:02:59+02:00
draft: false
tags: ["Paper"]
summary: "Estimating normals of a 3D object represented by an input bitmap rough sketch, using a non-orthogonal 2D cross field constrained by the curves from the input sketch."
link: "https://www-sop.inria.fr/reves/Basilic/2015/IBB15/"
---

## BendField energy

The goal is to find a smooth cross field aligned with the constraints given by tangent directions of lines of the input sketch. Instead of using a harmonic energy to define smoothness (that tends to flatten the surface away from the constraints), they use another energy.

This energy is derived from the fact that the goal is to extrapolate the curves sketched by the artist, which can be described as "regularized curvature lines", ie lines that follow the principal curvature directions in areas of high curvature, then follow geodesic curves in areas of isotropic curvature.

The cross field is discretized on the pixel grid of the input bitmap.

## Solving

* Find the 2D non-orthogonal cross-field with first a greedy mixed-integer optimization (to find the directions and the integer values corresponding to transitions between adjacent crosses), then refine with a nonlinear optimization (minimize the BendField energy).
* Lift solution to 3D (knowing that the crossfield should become orthogonal) and compute the normal from the cross product of the cross-field directions

