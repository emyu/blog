---
title: "Sketching in 3d Study"
date: 2020-10-21T11:26:36+02:00
draft: false
tags: ["Thoughts"]
summary: "Outline of a project to gather data in a controlled manner about how people use 3D sketching as an ideation and concepting tool, for design of 3D objects."
link: ""
---

## Motivation

### 3D sketching is not yet well studied as a representation medium

* View-dependent strokes compose a large proportion of sketched strokes in 2D (TODO: #) and are the most widely agreed on strokes between artists --> Where would people draw lines in a view independent context?
* Do artists agree on sets of curves to represent a 3D model? Or is there more leeway in the choice of representative curves as 3D affords more direct understanding of depth than 2D?

### Potential applications of the findings and dataset

* Understand how strokes relate to the envisioned surface for the artist, in order to devise more transparent surfacing tools that don't require artists to conform to a pre-defined and unique assumption of how strokes map to surface (eg: curve network in CASSIE). Previous work showed sucess in using perceptual insights on how people envision the surface from a clean curve network (Design driven quadrangulation, Finding cycles, Flow aligned surfacing). We could extend those methods to support a wider range of strokes.
* Existing collections of 3D sketches have been done with 2D tools so there may be a bias towards representing the objects with planar curves only (that are easier to draw in ILoveSketch/EverybodyLovesSketch/Analytical Drawings). We could provide a collection of truly 3D sketches to benchmark existing and future methods.
* If we have sketches registered to 3D model: guide creation of synthetic data for 3D sketches from existing 3D models, by devising rules that can be algorithmically replicated (a la Flowrep but include new rules that can take into account more delicate surface details like sharp features and work better on organic objects such as the rabbit from Fig. 22)

## Hypotheses

* Strokes sketched to represent a 3D object can be divided in a finite set of categories
* Artists agree on which strokes to use (at least in some of the categories)
* There is a mapping between the sketch and the surface that it represents. This mapping depends on each stroke semantic information (label) and geometry.
* The semantic information is crucial in obtaining this mapping (ie: there is value in creating a surfacing method that caters to a multitude of stroke type for the same sketch).

## Protocol ideas

### Participants

* Recruit professional designers or 3D artists with minimum experience of VR sketching. What's good in covid times is that designers that are available for us (ie have a VR headset at home) are most likely familiar with VR sketching or modelling.

### Tasks

* Freehand sketching with possibility for stroke edition and adjustments (oversketching, control points). Reduce as much as possible friction to create exactly the strokes the artist has in mind, while not providing any guidance or influence from the tool. TODO: define which tools should be available (eg: planar sketching? mirroring?)
* Avoid tracing while allowing correspondance between multiple participants: sketching the same object given planar reference images of it (multiple views), potentially provide reference solid (eg: foot for shoe, car tires, body for garment). Maybe allow for post-sketch registration of lines on a 3D model (could do some kind of semi-automatic thing where the user marks some correspondances, then the model is deformed based on those correspondances, in an iterative manner until they are satisfied with the alignment).
* Ideation --> concept sketch: create a new design given some prompts (type of object, characteristics), first with an ideation sketch then with a clean sketch to explain the design to other people. Use multiple layers, such that both sketches are registered by construction (this is a common practice in both traditional sketching and VR sketching). If the artist creates the 3D model too, it provides a sketch/model correspondence
* Choice of models: objects with sharp features and large smooth parts + texture details (motorcycle, car, running shoe, computer mouse), thin parts (chair legs, laces, umbrella), tubular parts from large to thin (eg: wine glass), something that would benefit from sketching a skeletal structure (lizard, tree)

{{< figure src="media/10-21/moto.jpg" caption="Presence of feature curves of different sharpness" width="200px">}}

{{< figure src="media/10-21/egg-chair.jpg" caption="On this very smooth chair, the surface cannot be represented fairly only through sharp features" width="200px">}}

{{< figure src="media/10-21/shoe.jpg" caption="The textured details on this shoe represent its identity as much as the high-level shape of it. Also there are very fine bumps and cavities on the sole part" width="200px">}}

{{< figure src="media/10-21/chaise-masters.jpg" caption="Thin structures on the legs and back rest (that kind of smoothly blend into a larger surface at the seat)" width="200px">}}

### Analysis

* Strokes registered on model: try to explain strokes (label them) based on

  * local surface curvature (ridges, valleys, cross sections)
  * stroke length and topology (cycles for cross sections or distinct zones on the surface)
  * stroke neighbourhood (is the stroke meant to be connected to others nearby, forming a network?)
  * on-surface, in-surface
* Compare proportions of each stroke category between models and participants
* Compare individual strokes between users to measure agreement. Potential methods (provided that all sketches are aligned):
  * Discretize space in voxels and measure votes per voxel
  * Cluster strokes by similarity (would need to be robust to a single stroke to multiple strokes matching)
* Time dependent analysis: sketch construction order

## Reservations / potentially disappointing results

* Is sketching in 3D already a common practice such that we can
  * Find enough participants skilled in it
  * Establish patterns and generalise observations across participants and to the general artist population, thanks to clear perceptual preferences and potentially existing "best practices" in this field. Did artists already find the "best" way to do it? This may not be the case at a very early stage of learning the technique
* Maybe the answer to "where do people draw strokes" is very simple: they draw all non view-dependent strokes that are commonly known from 2D sketches (crease lines and cross-sections)

