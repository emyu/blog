---
title: "Modeling flow features with user-guided streamline parameterization"
date: 2020-10-29T10:14:55+01:00
draft: false
tags: ["Paper"]
summary: "Sketch-based method to create local parameterization of a surface such that deformations that follow the flowlines of the parameterization can be applied."
link: "http://vdel.me.cmu.edu/vdelresource/publications/2014cad/paper.pdf"
---

* On a given triangle mesh, the user can define multiple constraint strokes
* This creates directional constraints at some faces of the mesh, then they generate a smoothly varying vector field aligned with those
* From the vector field they find a parameterization with isolines aligned with the vector field
* Once the parameterization is found, they allow the user to edit the surface by displacing the isoline correpsonding to the stroke, with different kind of fall-off shapes along u and v, to get different deformation effects (sharp crease, smooth bump)
