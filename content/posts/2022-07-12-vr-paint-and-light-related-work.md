---
title: "Vr Paint and Light Related Work"
date: 2022-07-12T11:40:37+02:00
draft: false
tags: []
summary: ""
link: ""
---

* [2D Shading for Cel Animation](https://dl.acm.org/doi/abs/10.1145/3229147.3229148)
* [StyLit: illumination-guided example-based stylization of 3D renderings](https://dl.acm.org/doi/abs/10.1145/2897824.2925948)