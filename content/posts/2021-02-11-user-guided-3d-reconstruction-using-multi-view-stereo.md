---
title: "User Guided 3D Reconstruction Using Multi View Stereo"
date: 2021-02-11T14:55:42+01:00
draft: false
tags: ["Paper"]
summary: "Interactive surface reconstruction method, using MVS to recover object geometry and texture, and manual input to disambiguate visibility of the object in a given picture, and indicate desired quad-mesh topology."
link: "https://dl.acm.org/doi/10.1145/3384382.3384530"
---

* The user creates quads in a 2D interface, on a given view of the set of input pictures, and their method is responsible for finding correct world space position for the vertices of the quad, positioning it correctly in other views (the interface displays 2 other views, along with the main view in which interaction takes place).
* Finding the correct position in depth for each vertex is done in 2 steps
  * A coarse exhaustive search for each new vertex added by the user: here they look at a discrete set of possible positions of the vertex along the viewing ray, and choose the position which gives the best photo-consistency score (meaning that the quad contains similarly colored pixels in different views).
  * A global optimization step: once each vertex is approximately placed by the coarse search and that a whole zone is covered by quads, they run a global optimization on all vertices at once (and even additional vertices added by subdivision). This optimizes the same photo-consistency score, along with a smoothness term (neighboring quads should have similar normals). They approximate the gradient wrt the depth of each vertex using central differences and solve with gradient descent.
* The costliest computation in their method is to compute the photo-consistency score for each quad (they take a number of samples in the quad and compare their color to another set of samples from a different view). They implement this on the GPU so that it can be easily parallelized, accelerating eg their exhaustive search, as they can compute the scores for many vertex configurations at once.
* They argue that their method is superior to automatic reconstruction + retopo, since the amount of work to get a mesh with correct topology from an automatic reconstruction would be about the same or more.

