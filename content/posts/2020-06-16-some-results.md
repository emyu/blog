---
title: "Some Results"
date: 2020-06-16T18:01:42+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Attempt at a plane with wing, shows that sharp features are not well treated at all by the system."
link: ""
---

The plane sketch doesn't get surfaced reliably because of the non smooth boundary between plane body and wing, which leads to unreliable normals at those nodes lying on the boundary.

{{< video src="media/06-16/06-16-plane-cycle-failures.mp4" caption="" width="">}}

On this more conservative sketch with a wing that smoothly blends with the body, we can see that the surfacing is more successful:

{{< video src="media/06-16/06-16-plane-cycles-success.mp4" caption="" width="">}}

Here is another medium complex sketch, the usual chair:

{{< video src="media/06-16/06-16-chair.mp4" caption="" width="">}}

All heuristics on cycle detection combined result in a more reliable algorithm which fails less often than before (only one failure in this whole chair sketch).
