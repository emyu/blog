---
title: "Anisotropic Polygonal Remeshing"
date: 2020-04-16T11:03:10+02:00
draft: false
tags: ["Paper"]
summary: "Remeshing method to create a quad dominant mesh with edges aligned with the principal directions of curvature in anisotropic regions."
link: "https://hal.inria.fr/inria-00071778/document"
---

* Estimation of curvature tensor by vertex on the 3D mesh
* Conformal mapping from the 3D mesh to a 2D parameter space, the curvature tensor is also mapped in this way and is represented as a 2x2 matrix at each vertex. It is linearly interpolated between vertices to get a continuous function over parameter space
* The tensor field is smoothed by gaussian filtering to keep only the global geometry of the surface
* Umbilic points (points where the tensor field is symmetric, ie there is no well defined principal curvature directions) are detected using the tensor field. They are also classified into one of  2 categories: wedge or trisector, depending on how the tensor field flows around it.
* Lines of curvatures are sampled on the parameter space, starting at "seeds" and following either the maximum or minimum curvature direction. The umbilic points are all potential seeds, and seeds get added iteratively orthogonally to traced lines. The density of lines in one direction (ie max) is determined locally depending on the curvature in the other principal direction (ie min). Those lines are sampled until everything is covered with the desired density of lines
* This whole process is done by performing constrained Delaunay triangulations with the existing lines of curvatures and user selected features as constraints. Using such a structure is very efficient for proximity queries, and it is also the way they end up finding the vertices formed by intersections of the curvature lines
* The mesh is decimated and faces are created
