---
title: "User Guided 3d Sketch Surfacing: Related Work"
date: 2021-06-16T15:25:11+02:00
draft: false
tags: ["Surfacing Project"]
summary: "List of related work for this project."
link: ""
---

## Mesh segmentation

* Variational Shape Approximation, Cohen-Steiner et al.
* Variational Mesh Segmentation via Quadric Fitting, Yan et al.
* Feature-aligned segmentation using correlation clustering, Zhuang et al.
  * We could apply their method by subtituting the detected ridges/valleys on the mesh by the projected strokes. However, this method may over segment since it is unable to decide if 2 zones should be merged because they could be fitted by the same model.
  * Main difference between their context and ours: strokes do not necessarily define boundaries in a sketch, some strokes may represent a detail that lies smoothly on the underlying surface patch. By fitting smooth surfaces and evaluating fitting error, we can detect when nearby zones separated by a stroke can be clustered together. Additionally we have a term that penalizes having too many models to prevent over segmenting. In our case over segmenting can lead to averse effects, such as introducing kinks in a surface that should be smooth.
* Hierarchical mesh segmentation based on fitting primitives, Attene et al.

## Interactive surfacing/meshing/mesh segmentation

* State of the Art in Surface Reconstruction from Point Clouds, Alliez
* SmartBoxes for Interactive Urban Reconstruction
* O-snap: Optimization-based snapping for modeling architecture
* Interactive Topology-aware Surface Reconstruction
* Anisotropic geodesics for live-wire mesh segmentation

