---
title: "NPR and the Science of Art"
date: 2020-05-05T09:43:15+02:00
draft: false
tags: ["Paper"]
summary: "Discussion regarding how NPR can provide insights on the science of art, ie help understand how artists create art and how observers respond to it."
link: ""
---

## Proposing new theories related to art

NPR can give new insight into art theory through:

* Study of how artists create a work in a generative way, ie they describe how to create certain types of art. If we can explain how to do it, it may bring us closer to understanding it.
* Perceptual studies and formulations of mathematical models for how people respond to art

## Difficulty of evaluation

Aaron argues that experimental methods currently used to support NPR methods (and therefore a certain theory of how art is generated or perceived) are used inconsistently and sometimes poorly.

* Seeing quantitative studies as a necessary thing to do, without thinking of whether it is meaningful.
* Review process is not favorable to lack of quantitative evaluation. Studies constitute evidence not proof, other means of providing evidence should be valid
* Difficult to measure aesthetics: proxy measures (ie effect of a rendering style on memorization), asking artists (he says that this is not of much value)
* One example of methodology: express a set of rules for how the algorithm produces images such that these rules can be evaluated on existing artworks

## Some early theories

* Art creation as an optimization of a set of objective functions
* Randomness: human art is not deterministic but rather a sampling from a distribution
* Perception of art/images is probabilistic: we interpret around ambiguities, actually ambiguity may be a part of what makes artistic images interesting
* As observers we are able to jointly interpret a novel style of representation and what object is depicted
