---
title: "Ideas Video Doodling Project"
date: 2022-02-08T11:41:04+01:00
draft: false
tags: ["Video Doodles"]
summary: "First ideas and questions on the project with Rubaiat and Wilmot."
link: ""
---

## Inspirations

* Video doodling tutorials:
  * Both 3D-looking and planar sketches on and around a person and objects, frame by frame sketching: https://www.youtube.com/watch?v=x3rZezo13j0
  * 2D sketches with affine transformations (keyframes): https://www.youtube.com/watch?v=CXFQQld9RRQ
  * Doodling on pictures and videos. On picture is 3D-looking with occlusions, on video is simple screen-space overlay. https://www.youtube.com/watch?v=Kc_h0R8rjzw
* Artists:
  * https://www.instagram.com/sean_charmatz/
  * YT vloggers that annotate videos
    * https://youtu.be/V6uof6Wf-BM?t=329 some manual tracking to follow simple camera motions, no occlusion management. Doodling and "frame-by-frame drawn" look is super important to the visual style of the artist: https://youtu.be/V6uof6Wf-BM?t=100
* Microsoft ads: https://www.youtube.com/watch?v=W3lHbVSL_84
  https://www.youtube.com/watch?v=zU9sPFMTX9g
  https://www.youtube.com/watch?v=it-Vt2M_-ZU

Current workflows:

* Capture, edit and then sketch. Sketching is part of post-process.
* A lot of frame by frame adjustments
* All done in screen-space, 3D effects are faked by manual work with eye-balled sketching and affine transformations

Some challenges:

* Occlusion: present in both stills and animated footage, and affects both 3D and planar sketches
* Sketch tracking an object in videos
  * eg scribbles on/around a person should follow them
  * when tracking in screen space is sufficient --> rotoscoping
  * are there cases where tracking HAS to be 3D aware?
* Interpolation between sketched keyframes
  * 3D transformation (same stroke moves in space)
  * sketch change (just 2D sketch interpolation)
* Interface
  * how to sketch in 3D from a tablet interface?
    * use the tablet as a proxy and position it to place canvases in space? (Pronto) Does that make sense in a post-processing context (editing in a different setting than the capture)?
    * sketch in 2D and indicate desired occlusions somehow (eg roughly "erase" occluded parts of strokes on a frame), deduce 3D from occlusions + captured depth
  * how to define animations in sync with a video?
    * sketching all keyframes
    * transform canvases, 3D transformations
    * separate canvas animations (3D) from sketch animation (2D frame-by-frame or sketched-keyframes) --> help with tedious placement and masking tasks but preserve hand-drawn feel

## Related work

* Video x Sketches
  * Line-Drawing Video Stylization
  * Keyframe-Based Tracking for Rotoscoping and Animation
  * Look for more rotoscoping related papers
  * ~~PoseTween Hong Bo Fu~~
* Video annotations
  * ~~Video Object Annotation, Navigation, and Composition, Goldman et al. 2008~~
  * Directly Manipulating Videos

* Sketch animation / Strokes interpolation
  * K-sketch

* 3D scene from video + AR
  * ~~Pronto: Rapid Augmented Reality Video Prototyping Using Sketches and Enaction~~
* Depth from mobile device
  * DepthLab: Real-time 3D Interaction with Depth Maps for Mobile Augmented Reality
  * ~~Johannes Kopf~~
* AR sketches
  * ~~SymbiosisSketch~~
  * ~~Mobi3DSketch~~
* 3D sketch in 2D interface
  * ~~Skippy~~
  * Local layering McCran
  * LayerPaint: A Multi-Layer Interactive 3D Painting Interface

## Existing technical bricks

* Depth and camera poses estimation from a RGB video (note: these methods need a moving camera)
  * Consistent Depth of Moving Objects in Video
  * Robust Consistent Video Depth Estimation
    * Quality: depth is pretty low fidelity in general (eg no details in background), but moving human subjects seem to be pretty well delimited, except near points of contact where it sticks back in a rather big zone (could be due to how they model moving objects by smoothly scaling the depth with a deforming spline grid)
    * Pretty slow, big part of runtime is taken by optical flow.
    * Prior: animated parts of the image are expected to be semantically segmented as humans, cars, animals
* Depth estimation from a RGB picture to create 3D photos
  * One-shot 3D photography
* 3D vision stuff
  * If a canvas is supposed to be fixed, given its position on 2 keyframes in 2D and the camera parameters in those frames, we can infer its depth
* 2D vision
  * A stroke can track pixels in the image forwards/backwards in time, or interpolate between keyframes while following the moving pixels (rotoscoping)

## Cool 3D effects

* Occlusion
  * climbing wall annotations, sketches around the kid
* Sketch that appears fixed in world space while the camera is moving away/towards it (scales appropriately)
  * the bridge monster
* Sketch that tracks a dynamic object that moves in depth
  * the door handle doodle

## Unknowns

* How robust can our method be to poorly inferred depth and camera paths?
  * To what extent can we rely on the "3D" information inferred by using those inputs? Eg, placing a canvas on 2 keyframes in 2D space => inferring a static 3D position
  * What if the inferred depth is too coarse and the desired occlusions can't happen? Eg, a sketch passing in-between 2 objects that don't have the same depth IRL but have the same inferred depth. Can we consider correcting depth? How to propagate to other frames?
* How simple can the canvas geometry be but still be versatile enough?
  * Heightfields? won't work for things that wrap around an object (eg hoop around person)
* What is the most complex sketch geometry we expect to support?
  * Where are we on a range from Skippy curves to planar sketches only?
* What is the fallback for unsupported sketch geometry?