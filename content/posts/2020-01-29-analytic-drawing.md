---
title: "Analytic Drawing of 3D Scaffolds"
date: 2020-01-08T13:14:40+01:00
draft: false
tags: ["Paper"]
summary: "**Ryan Schmidt et al., TOG 2009**. A method inspired by traditional analytical drawing technique to create 3D curve networks while the user draws on a tablet."
link: "https://dl.acm.org/doi/abs/10.1145/1618452.1618495"
---

## Knowledge about drawing

Strokes in 2D are **ambiguous in depth**.

Analytic drawing methods are used by designer to lift this ambiguity using a **3D scaffold**. The scaffold is a set of guiding lines that form a simple 3D volume and that is unambiguous as it respects certain rules: **all lines are parallel or perpendicular to each other.**

**Positional and directional hints:** Artists will tend to draw new curves starting from or ending at existing endpoints or intersections. They will also tend to draw lines through an existing endpoint or perpendicular to existing lines, if it’s a curve it may likely have a scaffold line as tangent.

Free-hand sketches are **imprecise** so to represent this, one stroke can be modelled as a sample from some distribution of strokes (each endpoint position is modelled as a sample from a uniform Gaussian distribution with some uncertainty radius that’s a **function of drawing speed**).

The method relies on this knowledge about drawing techniques to infer 3D curves from a 2D imprecise projection drawn by the user.

## Method

**Goal:** fit the “best” 3D curve/line to the 2D stroke drawn by the user. The best 3D curve is the one that is most probably what the user intended.

Line segments and curves are solved with different methods, as they can be subject to different types of constraints. Line segments are inferred by considering only the positions of the endpoints, as it is there that the sketched line is most reliable. Curves are represented as cubic bezier splines, and constraints can be found all along the curve.

1. Enumerate all constraint sets that the inferred 3D curve could satisfy
2. For each possible 3D curve, compute its “fitness” function based on how well it matches the 2D sketch, how well it fits into the existing drawing (is it of the same length as existing lines? Of a parallel/perpendicular direction?), how “nice” it is for a curve (it should preferentiably have a certain total absolute curvature), and how well it satisfies existing constraints (different constraints have an empirical weight).
3. Choose the best fit 3D curve (this can also be done by a vote after repeating the process for a number of samples drawn from the distribution caused by mechanical drawing error)

## Tricks

- Display ground shadow of the curves to give better 3D perception from a single view

## Inspiration for future work

- Explicit tangent manipulation interface to edit curves
- Interface to choose between multiple possible inferred 3D curves when uncertainty is high
- It can be hard to construct the right scaffold for beginners
- Infer surfaces

