---
title: "Volumetric Billboards"
date: 2022-02-23T10:19:49+01:00
draft: false
tags: ["Papers"]
summary: "Rasterization for volumetric objects."
link: "http://phildec.users.sourceforge.net/Research/VolumetricBillboards.php"

---

There are 3 aspects to this paper:

* Volume data representation: volumetric data is stored as 3D textures, where each voxel contains RGBA values, possibly normal information. The volumetric data is appearance data, with lighting baked in. The volumetric objects are placed in the 3D scene as prism-shaped cells that have 3D texture coordinates to read in the 3D textures.
* Volume data generation: they propose a method to generate these 3D textures for any 3D object (eg starting from the mesh of a tree). They render slices of the object from 6 view directions and combine these in the voxel grid. Additionnally they generate different mip-map levels for the 3D textures, so that rendering will not have aliasing.
* Volume data rasterization: they have an efficient algorithm to rasterize the volumetric images.
  * By computing view-plane aligned slices of all prism cells, they can rasterize each slice back to front and composite them as they go.
  * They have an efficient way of computing plane-prisms intersections.
  * Since all cells are rendered at once, intersecting cells are not a problem, but they have to enforce a deterministic drawing order in case of such intersection, to avoid popping.
  * Their slices have adaptive size, bigger when further from the camera