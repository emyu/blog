---
title: "Efficient Decomposition of Image and Mesh Graphs by Lifted Multicuts"
date: 2020-11-10T09:54:02+01:00
draft: false
tags: ["Paper"]
summary: "A method to segment images or meshes, by finding the solution to the minimum cost lifted multicut problem of the graph of pixel grid or mesh connectivity. They provide a general method to do segmentation of a set based on cost measures between elements."
link: ""
---

Their main contribution is to augment the multicut problem with long range terms, such that the connexion of nodes in the graph that are not neighbours can influence the partition, which makes segmentation more robust for images and meshes. They define a way to compute the cost to assign these nodes in dinstinct components. Secondly they also propose more efficient implementations of multicut algorithms, that generalise to their "lifted multicut" formulation (with long range terms).

Also the main author provides a graph library with implementation of efficient minimum cost (lifted) multicut problem, and other algorithms: http://www.andres.sc/graph.html

**Multicut:** a set of edges $E' \subseteq E$ of a graph $G = (V, E)$ such that removing the edges $E'$ from $G$ decomposes it into distinct components. And the edges removed $E'$ correspond precisely to the cuts between the components.

**Minimum cost multicut problem:** take as input a graph and a cost on each edge for putting the incident nodes into separate components (eg for images: the cost should be low if there is a high probability of a boundary between 2 pixels) and it outputs the 01-labelling of edges such that the edge is labelled 1 if it is a "cut".
