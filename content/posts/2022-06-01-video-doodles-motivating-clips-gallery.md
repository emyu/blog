---
title: "Video Doodles Motivating Clips Gallery"
date: 2022-06-01T11:43:07+02:00
draft: false
tags: ["Video Doodles"]
summary: "Gallery of videos"
link: ""
---

## Examples from the internet

### Kid playing with imaginary decor and toy trails

{{< video src="media/2022-05-23-video-doodles-design-space/kid_tracking_toy.mp4" caption="" width="600px">}}


### Kid with swirl

{{< video src="media/2022-05-23-video-doodles-design-space/kid_swirl.mp4" caption="" width="600px">}}


### (Slightly) moving camera with 2D animation loop and shadow

{{< video src="media/2022-05-23-video-doodles-design-space/brill_bird_shadow2.mp4" caption="" width="600px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/brill_naga_occlusion.mp4" caption="" width="600px">}}

### Animated creatures and particles

{{< video src="media/2022-05-23-video-doodles-design-space/dby_creatures.mp4" caption="" width="600px">}}

### In-scene text insertion

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_wheel.mp4" caption="" width="600px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_rotation.mp4" caption="" width="600px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_reflexion.mp4" caption="" width="600px">}}

### Microsoft ads

{{< video src="media/2022-05-23-video-doodles-design-space/microsoft-1.mp4" caption="" width="600px">}}
{{< video src="media/2022-05-23-video-doodles-design-space/microsoft-2.mp4" caption="" width="600px">}}
{{< video src="media/2022-05-23-video-doodles-design-space/microsoft-3.mp4" caption="" width="600px">}}

### Animated lines traced over objects in the scene and labels

{{< video src="media/2022-05-23-video-doodles-design-space/3d_animated_line.mp4" caption="" width="600px">}}

## Examples from related work

### Procedural rain particles that bounce off umbrella (from *Secondary Motion for Performed 2D Animation*)

{{< figure src="media/2022-05-23-video-doodles-design-space/collision.gif" caption="" width="600px">}}

## Manually (poorly) created examples for illustration

{{< video src="media/2022-05-23-video-doodles-design-space/family_run_crown.mp4" caption="" width="300px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/head_turning_1.mp4" caption="" width="600px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/playground.mp4" caption="" width="600px">}}

{{< video src="media/2022-05-23-video-doodles-design-space/family_run.mp4" caption="" width="300px">}}