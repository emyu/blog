---
title: "Usability Evaluation Considered Harmful"
date: 2020-04-28T09:10:18+02:00
draft: false
tags: ["Paper"]
summary: ""
link: "https://www.billbuxton.com/usabilityHarmful.pdf"
---

* Usability evaluation has weaknesses:

  * Doing a project/posing a research question FOR a controllable usability evaluation, because it is favored by CHI community to have one in the paper
  * Evaluating a particular case where the new method is better = existence proof and not really testing the hypothesis
  * Replication is almost never attempted because unpopular in the community

* Usability evaluation can cause harm to innovations if it comes too early

  * An early sketch of a design is by definition not fleshed out enough to be evaluated in this way
  * Usability evaluation encourages "getting the design right" (improving the same idea) instead of "getting the right design" (trying out several different ideas)
  * Can't evaluate what's really useful and not either what could become useful in the near future due to effects of cultural adoption

* Solutions:

  * As a community, recognize other methods of evaluation as valid (case studies, scenario of use, self critique)
  * Formulate usability evaluations more rigorously as hypothesis testing and encourage replications

  
