---
title: "An Interface for Creating and Manipulating Curves using a High Degree-of-Freedom Curve Input Device"
date: 2020-04-08T15:01:57+02:00
draft: false
tags: ["Paper"]
summary: "Using a tangible flexible and tracked piece of tape as an interface for curve creation and manipulation."
link: "http://www.dgp.toronto.edu/~ravin/papers/chi2003_curvemanipulation.pdf"
---

Tape and 2 buttons as the only input device for their system. They developped a number of distinct semantic gestures that map to actions in the system, such as scaling, switching between smooth curve and a sharp corner, etc.

They add the possibility to edit existing curves by entering editing mode implicitly when the Tape is close to an existing curve, and they automatically determine which type of edition is desired by the user.

They extend their 2D curve drawing system by adding possibility to draw on planes and on extruded curve surfaces. In this way they get the possibility to draw curves in 3D "for free".
