---
title: "Expert Interviews Examples"
date: 2020-10-28T14:46:22+01:00
draft: false
tags: ["Paper"]
summary: "Compilation of papers in which I found some kind of expert interview used to motivate the work."
link: ""
---

* [Experimental Evaluation of Sketching on Surfaces in VR](https://www.dropbox.com/s/w7l85hceft0hrxc/chi17_vr_sketching.pdf?raw=1), Arora et al.
  * Observations of expert designers using Tiltbrush (mostly for the first time)
  * Get insight on what the main challenges are in VR sketching
* [VRSketchIn: Exploring the Design Space of Pen and Tablet Interaction for 3D Sketching in Virtual Reality](https://www.uni-ulm.de/in/mi/hci/projects/vrsketchin-exploring-the-design-space-of-pen-and-tablet-interaction-for-3d-sketching-in-virtual-reality/), Drey et al.
  * Semi-controlled interviews of professionals in "some form of CAD tool" (animator, mechanical engineer, architect, product designer)
  * Understand how sketching (2D sketching) is used in their current workflow and identify which step could most benefit from being done with a mix of 2D and 3D input (identified as being ideation)
* [Extending Manual Drawing Practices with Artist-Centric Programming Tools, Jacobs et al.](https://joelbrandt.com/publications/jacobs-chi2018-extending-manual-drawing.pdf)
  * Interviews of artists (both working primarily with manual and programming tools)
  * Understand what's important to them in their art creation practice to guide the design of their tool, Dynamic Brushes
* [Selective Undo Support for Painting Applications](http://www.cs.cmu.edu/~faulring/papers/aquamarine-chi15.pdf)
  * Interview of a variety of expert Photoshop users to understand how they use the undo/redo functionality
  * 3 parts:
    * They work on one of their own task, think aloud and answer to some guiding questions by the interviewer
    * They were given a task to put them in a position where selective undo would have been useful
    * The interviewer explained what selective undo is and asks whether it would be useful for them. Then they gave examples of selective undo scenario and asked what the participant would expect to happen, to see what would be most natural to them
