---
title: "SurfaceBrush"
date: 2020-12-14T16:13:20+01:00
draft: false
tags: ["Paper"]
summary: "Consolidate a dense 3D sketch composed of ribbons into a manifold surface."
link: "https://www.cs.ubc.ca/labs/imager/tr/2019/SurfaceBrush/"
---

* Related work is very rich and provides good examples of how to position the work compared to previous ones.
* Sec. 3 describes the particularities of dense ribbon sketches
  * Normals/tangent directions of the strokes are consistent with the surface represented and locally consistent between strokes
  * No normal orientation consistency (orientation is dictated by artist position rather than intentionally set)
  * Mention the use of skeletal strokes for thin geometry
  * They observe rather good accuracy of the strokes wrt the intended surface (qualitatively)
* The core problem is to match vertices from the strokes, between which triangle strips can be created. The constraint is that they desire the mesh to be manifold.
* They solve this with multiple steps:
  * For each stroke find its dominant neighboring strokes (max 2 of those, one right and one left, in the local frame defined by the stroke normal), as those that have the most vertex-to-vertex matches with it. This creates a restricted set of vertex for each stroke, among which to look for vertex matches. This implicitly encourages matches to be persistent along strokes.
  * For each stroke, look for per-vertex matches among the corresponding restricted set by maximizing a matching score defined on the matches, defined as a sum of:
    * A score for one match: proximity, tangent alignments, lying in the tangent plane of the respective normals)
    * A score that looks at 2 consecutive matches: geometric persistency, ie the segments between 2 consecutive matches should be aligned
  * This is solved using a dynamic programming framework.
  * Once the matches are found for all strokes, they create triangle strips between consecutive matches
  * Then they extract a manifold mesh (potentially with open boundary) by casting the problem of selecting the correct triangles from the set of undecided triangles (that create non-manifold configurations), as a multicut problem.
  * The 3 steps of finding matches, generating triangle strips and extracting a manifold mesh are done again but looking only at the remaining open boundaries of the mesh, and loosening some conditions in order to permit more matches.
