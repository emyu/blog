---
title: "Bounded Biharmonic Weights"
date: 2021-04-08T13:48:57+02:00
draft: false
tags: ["Paper"]
summary: "Computing blending weights for deformation given disparate types of handles like points, skeletons, cages. They solve for the weights by minimizing the Laplacian energy of the weights, and constraining them to respect the user-given handles while staying bounded (inequality constraint)."
link: "https://igl.ethz.ch/projects/bbw/bounded-biharmonic-weights-siggraph-2011-jacobson-et-al.pdf"
---
