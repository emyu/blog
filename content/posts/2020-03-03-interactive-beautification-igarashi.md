---
title: "Interactive Beautification Igarashi"
date: 2020-03-03T16:17:23+01:00
draft: false
tags: ["Paper"]
summary: "This precursor to ShipShape introduces interactive beautification for line segments, to have free-hand strokes satisfy a number of potential geometric relations relative to the existing strokes in the sketch."
link: "https://dl.acm.org/doi/pdf/10.1145/1281500.1281529"
---

The method finds geometric constraints in the drawing for each new stroke by searching for similarities between the new line segment and older line segments. From these constraints, it generates candidates by combining constraints together sequentially (as well as maintaining a state where the constraint is not applied) until all constraints have been applied. This builds a kind of tree where the leaves are combinations of compatible constraints deducted from all possible constraints. In their case these are simply 2 coordinates that define the position of a vertex.

{{< figure src="media/interactive-beautification-constraints-tree.png" caption="" width="400px">}}

Because their constraints are only linear equations, they solve it simply by Gaussian elimination.

Once they find candidates, they evaluate each candidate. It is not specified how exactly they compute the score, except that a candidate close to the input stroke should be evaluated highly. The best candidate is the main beautification suggestion, but the other candidates are also displayed in a different way, so the user can choose one of those if it matches their intent better.
