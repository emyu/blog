---
title: "Master Thesis Project Plan"
date: 2020-04-01T14:58:39+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Project plan for the master thesis, explaining the motivation behind the project and detailing a few goals that we have"
link: ""
---

[Project plan for the master thesis, explaining the motivation behind the project and detailing a few goals that we have.](media/project-plan.pdf)
