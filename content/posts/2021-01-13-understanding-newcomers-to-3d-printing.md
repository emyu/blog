---
title: "Understanding Newcomers to 3d Printing"
date: 2021-01-13T13:23:55+01:00
draft: false
tags: ["Paper"]
summary: ""
link: "https://hci.cs.sfu.ca/HudsonCHI16.pdf"
---

* Motivation: study a specific population (novices), because they are a good proxy to understand how the general public will use 3D printers when/if they are democratized.
* Focus: motivations, workflows and barriers
* Analysis method: inductive analysis
* Participants: 3 categories
  * casual makers (people who have tried to use a 3D printer in a library or school)
  * operators (people whose task is to help the casual makers)
  * experts
* By hearing from these different perspectives they can point down more easily what is difficult for the novices, since those cannot necessarily know what was the reason they failed to do something or were blocked.