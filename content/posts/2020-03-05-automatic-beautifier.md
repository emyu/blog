---
title: "An Automatic Beautifier for Drawings and Illustrations"
date: 2020-03-05T09:19:12+01:00
draft: false
tags: ["Paper"]
summary: "This early beautification method works on a finished drawing to enforce some regularity constraints such as slopes, length and collinearity of sides and alignments of points."
link: "https://dl.acm.org/doi/pdf/10.1145/325165.325240"
---

Their method has 2 main steps:

* Clustering of elements based on similarity (eg slope of lines, point position) to find which constraints could be applicable. This gives a set of equations tying some variables together.
* Solving these equations. They first compute a penalty for each equation, equal to the maximum change in one of its variable assuming that the others remain at their original values. They then process the equations in order of increasing penalty. In this way they prefer equations requiring only a small change to the original sketch. 

To solve the equations, they use [this algorithm](https://onlinelibrary.wiley.com/doi/pdf/10.1002/spe.4380141208) by taking one equation at a time, plugging in the linear combination representation of each variable and simplifying it. Then one variable is made dependent of the others and they move on to the next equation. However they state that they cancel this operation if it would cause this newly dependent variable to move too far from its original position. In this case they ignore the equation.

They introduce the concept of "negative constraints" to avoid undesirable beautification that leads to edges collapsing on top of each other for example. To enforce those they simply freeze certain values to the original instead of constraining them to be equal to each other.
