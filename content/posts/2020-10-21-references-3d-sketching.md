---
title: "References 3d Sketching"
date: 2020-10-21T11:12:36+02:00
draft: false
tags: ["Ressource"]
summary: "Trying to gather a list of examples of artists using sketching in 3D."
link: ""
---

## Sketch as a preamble to modelling the surface

* Durk - Spaceship turtle (0:00 to 2:56) : https://youtu.be/o1oSfeSnP_o

{{< figure src="media/10-27/01-durk.png" caption="Rough network" width="400px">}}

* James Robins - car sketches and 3D models: https://sketchfab.com/3DTensn

{{< figure src="media/10-27/02-james-robbins.png" caption="Rough network" width="400px">}}

* Nick Baker - object design: https://sketchfab.com/nickbaker (yt channel: https://www.youtube.com/channel/UCYnrmnFId9CZNpOwucvTRBA/)

{{< figure src="media/10-27/03-nick-baker.png" caption="Rough network" width="400px">}}

* Car design: https://sketchfab.com/I_draw_cars_in_3D

{{< figure src="media/10-27/04-idrawcars.png" caption="Rough network" width="400px">}}

* James Robbins - car process: https://youtu.be/SqZZ5amRqSc?t=561

{{< figure src="media/10-27/13-james-robbins.png" caption="The volume defined by the curves is used for form finding" width="400px">}}

## Sketch as final product

* Horse (the mixed 2D-3D is interesting): https://skfb.ly/6CLNn

{{< figure src="media/10-27/07-horse.png" caption="A lot of strokes look like the contour strokes from viewing the horse perpendicular to its symmetry plane (they coincide with a cross-section of the horse)" width="400px">}}

* Žofia Babčanová - shoe (tutorial): https://youtu.be/9t_8Wf1emhs

{{< figure src="media/10-27/05-sofia.png" caption="Thick curves as a design element / pattern" width="400px">}}

* Joey Khamis - shoes: https://www.joeykhamisdesign.com/gravity-sketch / process video: https://youtu.be/zHDcFx5DeJE

{{< figure src="media/10-27/06-joey-khamis.png" caption="The volume defined by the curves is used for form finding" width="400px">}}

## Paintings in a 3D NPR kinda style

* Liz Edwards - portraits: https://sketchfab.com/lizedwards

{{< figure src="media/10-27/08-liz-edwards.png" caption="" width="400px">}}

{{< figure src="media/10-27/09-liz-edwards.png" caption="" width="400px">}}

* Jama Jurabaev - vehicles: https://sketchfab.com/jamajurabaev

{{< figure src="media/10-27/10-jama-jurabeav.png" caption="" width="400px">}}

* VRHuman - a cabin: https://skfb.ly/6QZrO

{{< figure src="media/10-27/11-vrhuman.png" caption="" width="400px">}}

* Nick Ladd - town: https://sketchfab.com/Nicktheladd

{{< figure src="media/10-27/12-nick-ladd.png" caption="" width="400px">}}

