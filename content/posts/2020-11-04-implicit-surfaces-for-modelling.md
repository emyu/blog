---
title: "Implicit Surfaces for Modelling"
date: 2020-11-04T19:06:07+01:00
draft: false
tags: ["Paper"]
summary: "Summary of what I learnt reading Ryan Schmidt's master thesis."
link: "http://www.unknownroad.com/publications/RyanSchmidtMScThesis06.pdf"
---

## Skeletal primitives with bounded fields

* Fields that have global influence are not practical for interactive modeling, since we expect that a primitive should only affect the local area.
* A field is bounded if it is uniformly 0 outside some finite distance from the skeletal primitive. The surface is defined as the level set of some non-zero value.
* Definition: composition of a function $g$ with compact support and a distance field (distance to the skeletal primitive): $f(p) = g(d(p))$

## Introducing C1 discontinuities

* CSG operations such as union can be used to introduce discontinuity at the boundary where the 2 objects meet
* However the basic Ricci union operator (min of the fields) creates C1 discontinuities everywhere on the field, not only on the surface. So blend operations will not successfully smoothly blend the shapes since the field is not smooth away from the surface. Using [Barthe et al. blending](https://onlinelibrary.wiley.com/doi/pdf/10.1111/1467-8659.t01-1-00643), the discontinuity is present only on the surface and not elsewhere on the field.

## Normalization of the field

* Distance fields are normalized, meaning that their gradient is 1 everywhere (except in places where the gradient would not be well-defined due to C1 discontinuities, such as points equidistant from the primitive/skeleton)
* Most fields obtained by combining scalar fields with some operators will not be normalized
* The more normalized a field is and the easier it is to visualize it (quick convergence of ray marching), and blending operations are more consistent.

## Constructive modeling framework VS global field approach

* Constructive modeling composes simple primitive shapes (that have a known scalar field)
  * Distance field, F-rep: the surface is the zero isocontour, the fields have infinite support (and the composition operators too)
  * Skeletal primitives: the surface is at an offset of a potential field (non-zero iso-value). They are bounded. They use a different set of blending operators as F-reps. Convolution surfaces are a special case where the skeleton is convolved with a kernel function
* Point set interpolation fit a smooth field to a set of input points, usually with the surface at zero iso-value
  * Variational implicit surface: interpolate/approximate the points using a weighted sum of basis functions (eg: RBF). Has been used in combination with 2D sketching to fit a field to an input sketch
  * A variant of the VIS: use compactly-supported radial basis functions
  * Multi-level partition of unity implicit surfaces (MPU) [[Ohtake et al, 2005]](https://dl.acm.org/doi/pdf/10.1145/1198555.1198649): fit groups of points separately and blend the results together. This enables them to treat inputs with a large number of points and reconstruct sharp features. (This approach has been used to estimate ridges/valleys on the surface: [[Ohtake et al, 2004]](https://dl.acm.org/doi/pdf/10.1145/1186562.1015768))
  * Implicit Moving Least Squares: similar to MPU but can exactly interpolate a triangle mesh

## Making evaluation of $f(p)$ fast

* To visualize the surface, $f$ needs to be evaluated many times, over the whole domain (eg for polygonization)
* Multiple techniques have been proposed to make this faster, and Ryan Schmidt specifically focuses on making it robust to a large number of primitives and a tree of blending and CSG operations. He proposes a caching method, associated with the BlobTree evaluation scheme (where composition operations are nodes of a tree, and primitives are the leaves), in order to avoid evaluating some sub-parts of the tree many times if they are unchanged by editing operations. For that the fields need to be bounded, and the composition operations have compact support.

## Bounded and smooth scalar field from a 2D contour

* Compute an approximation of the distance field to the 2D contour (done on a discrete grid)
* Take samples on the estimated iso-contours (on the curve, offset from the curve along the normal, and far from the curve at the bound where we wish the field to be zero)
* Fit a $C^2$ implicit function to the samples by variational interpolation (solve the dense matrix to find coeff of the linear system that arises from the thin-plate spline definition of $f$ and the constraints from the samples $f(x_i) = c_i$)
* Sharp features are detected by looking at high tangent direction change and a special distance field is used in a small disc around such features. This distance field preserves sharp features (normalized implicit polygon).
