---
title: "Smooth Interpolation of Curve Networks with Surface Normals"
date: 2020-02-20T13:42:45+01:00
draft: false
tags: ["Paper"]
summary: "This method reconstructs a smooth surface interpolating a sparse closed curve network, using data from normals along the curves."
link: "https://hal.inria.fr/hal-01342487/document"
---

## Detect cycles

Cycle detection is made simpler by using the normals. At a node, the adjacent segments can be sorted wrt the orientation given by the normal. Further reading on cycle detection: [Zhuang 2013](https://dl.acm.org/doi/pdf/10.1145/2508363.2508423).

## Create surface patches

Once cycles are found, the closed curve defined by each cycle is projected on a plane and triangulated to form a flat surface patch ([Triangle](https://www.researchgate.net/profile/Jonathan_Shewchuk/publication/2592160_Triangle_Engineering_a_2D_Quality_Mesh_Generator_and_Delaunay_Triangulator/links/00b495358a6416512f000000/Triangle-Engineering-a-2D-Quality-Mesh-Generator-and-Delaunay-Triangulator.pdf) tool).

## Find the smooth interpolating surface

First, an initial guess for vertex positions $\pmb{v^*}$ and normals $\pmb{n^*}$ is obtained by solving biharmonic systems. The cotangent Laplacian is used to discretize the laplacian.

From the initial guess, the discrete mean curvature vector at every vertex is computed $h(\pmb{v_i^*})$. This can later be used in the optimization to force the resulting position to match this estimate of the mean curvature.

The energy functional that is minimized is composed of 3 terms:

* $\sum_v ||\Delta^2\pmb{v}||^2$ to minimize bending energy
* $\sum_v || \Delta \pmb{v} + 2h(\pmb{v^*})\pmb{n^*} ||^2$  to match the estimated mean curvature and estimated normals
* $\sum_{v_s} || \pmb{v_s} - \pmb{v_s^c} ||^2$ to match the soft constraints on vertices $v_s^c$

The optimization amounts to solving a sparse linear system of equations.

