---
title: "Shipshape"
date: 2020-02-13T14:13:55+01:00
draft: false
tags: ["Paper"]
summary: "This paper presents a framework for interactive beautification of 2D sketches. Their system supports general Bézier curves as input and takes into account implicit geometric relationships between a new stroke and existing strokes in the drawing (eg parallelism, length equality, geometric transformation)."
link: "https://dcgi.fel.cvut.cz/home/sykorad/Fiser15-SBIM.pdf"
---

This system proposed by Fiser et al. works by considering how to beautify each new stroke as it is drawn by the user, while taking into account the set of all existing paths already on the drawing.

## A set of possible geometric relationships

The authors define an extensible set of geometric rules that can be evaluated separately from each other.

{{< figure src="media/shipshape-rules.png" caption="">}}

To evaluate the likelihood of a path satisfying a rule, a normalized measure is used for all rules such that 2 rules of a different nature can be compared to each other. The appendix describes the likelihood functions used for each rule.

## Sampled paths for fast geometric calculations

To simplify geometric calculations, the paths (once resolved) are stored along with a set of discrete samples on the path, and the calculations for inter-strokes distance are performed on decimated polylines rather than on the Bézier curve. This polyline is obtained by sampling the path equidistantly, then applying the [Ramer-Douglas-Peucker algorithm](https://en.wikipedia.org/wiki/Ramer%E2%80%93Douglas%E2%80%93Peucker_algorithm) to keep a minimum amount of samples to satisfy a given precision.

{{< figure src="media/shipshape-sampled-paths.png" caption="" width="500px">}}

{{< figure src="media/shipshape-distance.png" caption="Using the polyline of a resolved path (grey) to compute the discrete Fréchet distance with the new path (blue)" width="300px">}}

Such a path representation for distance computation could be interesting for my project as I also need to deal with computing distances between curves (to snap to scaffolds or to other curves).

## Searching through all possible combinations of rules

The input path can be found to match a combination of different rules. To make sure that the most likely rule chain is applied to beautify the stroke, the system represents the possible rule chainings as branches of a tree. Choosing a final beautified stroke boils down to choosing a leaf node. This choice is based on the likelihood of the total chained transformation, which is pretty much a product of individual rules' likelihoods.

{{< figure src="media/shipshape-tree.png" caption="" width="500px">}}

The tree of possible chained rules is traversed in a best-first manner, paths that have the best overall likelihood are explored first. The tree can also be pruned using rule combinations that have already been discovered. The algorithm searches for the best 10 unique combinations (the interface gives the user the choice between multiple beautified results).

## Breaking a curve with corners into multiple segments

If a curve is drawn with hard corners (imagine a triangle drawn in one stroke) then the system detects the corner points (by finding local maxima in the angles between adjacent tangents) and breaks the strokes into multiple strokes at these points. The endpoints of these strokes are constrained to be snapped at the endpoint. That enables the user to draw a regular triangle in one stroke. The corners can then be reused as snapping points for later strokes as they are endpoints.

## Layer idea

Not a major idea from this paper, but the authors mention that users could easily avoid the constraints imposed by ShipShape by using a different layer in their Illustrator drawing. The concept of layers could be interesting in our project to separate scaffolds, shape defining lines and illustrative details (intended to be projected on the shape). This could help us apply different processings to these different families of strokes.
