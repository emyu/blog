---
title: "FreeDrawer"
date: 2020-05-09T13:33:40+02:00
draft: false
tags: ["Paper"]
summary: "A 3D sketching system on the responsive workbench, where curves are sketched in the air, and automatically stitched into a network such that surfaces can be created from the cycles in the network."
link: "https://dl.acm.org/doi/pdf/10.1145/505008.505041"
---

More technical details in: [Conceptual Free-form Styling on the Responsive Workbench](https://dl.acm.org/doi/pdf/10.1145/502390.502406)

They use variational modeling techniques on curves and surfaces, minimizing energies corresponding to fairness to smooth, or minimizing a "point to point attractor" energy to pull a curve towards a desired point. They also use an energy term to preserve the shape of the initial curve.

Features:

* Automatic stitching of input curves into the curve network
* Curve and surface smoothing, sharpening and pulling towards a point
* Two-handed manipulations: the non-dominant hand can move the modeling coordinate system, but also create drawing planes on which the curves are projected or apply global and local symmetry planes
* A tool selection UI that is displayed floating in front of the hand
