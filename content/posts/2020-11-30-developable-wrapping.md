---
title: "Shape Approximation by Developable Wrapping"
date: 2020-11-30T18:04:54+01:00
draft: false
tags: ["Paper"]
summary: "Method to approximate a mesh by a set of developpable surfaces that cover it entirely."
link: "https://igl.ethz.ch/projects/developable-wrapping/developable-wrapping-paper.pdf"
---

* Two challenges
  * Globally, place developpable patches on the mesh
  * Locally fit the patches to the mesh
* They use discrete orthogonal geodesic nets (DOG) to represent developable surfaces
* To fit the patches to the mesh without over-constraining it, they proceed in multiple steps
  * Initialize the patch by constraining a coordinate curve of the patch to lie on a geodesic of the mesh
  * Gradually add soft constraints by looking in a growing neighborhood around the curve
* They place the patches on the mesh by sampling geodesic curves on the mesh, that they then use for initialisation of the fitting
  * First they sample an over-complete set of geodesics on the mesh by sampling points and directions and tracing geodesics from those
  * Then they select a subset of geodesics that cover the mesh by associating a ruled surface to each geodesic, and solve a multi-label graph cut on the vertices of the mesh, with a smoothness term and data term (distance to the ruled surface associated with the geodesic/label). This is enough to prune out redundant geodesics
  * However they still need a step to cover up any missed out parts of the mesh with additional patches.
  * The same method is used in this paper: https://dspace.mit.edu/handle/1721.1/100915 to find a subset of directions from which to define height-fields in the context of fabrication (3-axis milling or mold casting).
* Finally they find the piecewise developpable surface by projecting the original mesh onto the fitted developpable patches.
  * First they find an assignment of mesh face to patch by solving a graph cut, to get a smooth labelling
  * Then they solve an optimisation to satisfy multiple objectives: get a connected mesh (vertices may be assigned to different patches), get smooth seams, maintain developpability, smooth mesh.
