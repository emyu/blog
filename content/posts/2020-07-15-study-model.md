---
title: "Study Models"
date: 2020-07-15T13:55:37+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Examples of models for the study"
link: ""
---

*Note: in both videos, snapping encounters weird issues due to a new bug that is now fixed.*

Computer mouse (speed x2)

{{< video src="http://www-sop.inria.fr/members/Emilie.Yu/07-15-mouse-model.mp4" caption="Computer mouse (speed x2)" width="">}}

Desk lamp (speed x2)

{{< video src="http://www-sop.inria.fr/members/Emilie.Yu/07-15-lamp-model.mp4" caption="Desk lamp (speed x2)" width="">}}

{{< figure src="media/07-15/plane-with-model.PNG" caption="Simple airplane" width="600px">}}
