---
title: "Process in 3d Sketching"
date: 2020-10-26T10:09:35+01:00
draft: false
tags: ["Ressource"]
summary: "List all ressources about different artists' process when sketching in 3D"
link: ""
---

Process videos:

* Jama Jurabaev - GravitySketch (sketch + modelling):

  * robot: https://youtu.be/aNSRlvCr1Js 
  * spaceship: https://youtu.be/XXwbre_Frnc 
  * skull: https://youtu.be/VedX6PfZxes 
  * car: https://youtu.be/aruJw0JPAFc

* Jama Jurabaev - GravitySketch (modelling organic shape with strokes): https://youtu.be/ToPJPM3B-X0

* Durk - GravitySketch: https://youtu.be/o1oSfeSnP_o

* Vrhuman - TiltBrush (line sketch + dense sketch):

  * Robot: https://youtu.be/Gbe4WuIisUQ
  * Posed human: https://youtu.be/nOsLnkoMNfY

* Joey Khamis - GravitySketch shoes: https://youtu.be/zHDcFx5DeJE

* GravitySketch live sketching sessions, eg: https://youtu.be/iastFgz_IZQ?t=524

* James Robbins - GravitySketch (sketch + modelling):

  * Skeletal sketching of frame: https://youtu.be/R0ioaLFkXLE

    

Tutorials:

* Introduction to VR for Artists (GravitySketch): https://gumroad.com/l/GPpWG?fbclid=IwAR2KWqLzt-rsN4mLfsPgkSGJvqBQKvyJB9vUCFh4HsC257rlGgrdm0te6X4
* James Robbins car (GravitySketch):
  * Ideation, sketching: https://youtu.be/SqZZ5amRqSc
  * Details about surfacing and topology: https://youtu.be/Km2JEZBmRbk

