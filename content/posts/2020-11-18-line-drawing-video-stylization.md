---
title: "Line Drawing Video Stylization"
date: 2020-11-18T18:05:49+01:00
draft: false
tags: ["Paper"]
summary: "Stylize a video using a library of artist generated strokes. The challenge is to track matching curves across frames to maintain temporal coherence in the stylized result."
link: "https://faculty.idc.ac.il/arik/site/videoSketch.asp"
---

* Problem: match curves or portions of curves between frames $t$ and $t+1$. The curves can appear/disappear/split into multiple curves between. The set of curves matched between multiple frames will correspond to the same stylized stroke from the artist strokes library.
* They match individual points on curves of frame $t+1$ to points on curves of frames $t$ with the objective:
  * Minimize the total cost of all matches (each match between 2 points has a cost equal to their distance)
  * Find as many matches as possible (leave few unmatched points from both frames)
  * Match contiguous points on curves at a frame to the same curve on the next/previous frame
* They express the wish to match contiguous points on curves by using a metric that favorises concentrating the discontinuous matching at a small number of points on the curves. This yields curve segments matched to other segments, such that the curves can be easily split into sub-curves that match entirely
* They relax their problem to use non-binary variables, solve with linear programming (linear energy, linear inequality constraints) and round the variables to get the matchings: $w_{i,j} \in \{0, 1\}$ between point $i$ and $j$
* Once they have the points matching, they can express stroke matching across frames (with the possibility of one to many or many to one curve matching). Two strokes are matched if they have some points matching. This gives a graph. On this graph they perform splits at nodes with multiple matches, in order to get simple paths. Splitting a node means splitting the curve between the points where the matching changes.

