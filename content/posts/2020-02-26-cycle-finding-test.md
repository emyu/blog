---
title: "Finding cycles, a test"
date: 2020-02-26T18:02:43+01:00
draft: false
tags: ["VR Sketching Project"]
summary: "Using the executable from Zhuang et al. I generated a few cycles and surfaces from curve networks drawn in my application."
link: ""
---

[A general and efficient method for finding cycles in 3D curve networks, Yixin Zhuang, Ming Zou, Nathan Carr, Tao Ju](https://www.cse.wustl.edu/~taoju/zoum/projects/CycleDisc/)

{{< figure src="media/02-26/weird-chair.gif" caption="" width="600px">}}

{{< figure src="media/02-26/bump-surface.gif" caption="" width="600px">}}

These shapes were drawn in VR, exported to the format compatible with the executable and fed to it. Some minor but annoying modifications to the curve endpoints had to be done to ensure a well-connected curve network.

The method works well but the executable that the authors provided only accepts as input very clean curve networks, with well defined intersections. This is still a bit tricky to obtain from my application, as I don't necessarily have these intersections (sometimes a curve will just cross another one in the middle and not on an endpoint, or the intersection is not perfectly coincident). This is why only these basic shapes were used.
