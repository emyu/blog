---
title: "Depixelizing Pixel Art"
date: 2020-05-09T13:32:33+02:00
draft: false
tags: ["Paper"]
summary: "Method to make vector graphics from pixel art, preserving all the details intended by the artist and reducing staircasing artifacts."
link: "https://johanneskopf.de/publications/pixelart/"
---


The main trick of the method is to disambiguate diagonal connections between pixels. They use heuristics based on what is commonly observed in pixel art to disambiguate crossing diagonal edges in the color similarity graph (each pixel is a node, edge between 2 pixels if they are close in color).
