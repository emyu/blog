---
title: "Smart Scribbles for Sketch Segmentation"
date: 2020-11-18T18:06:01+01:00
draft: false
tags: ["Paper"]
summary: "User-guided segmentation of a sketch. The user provides a few scribbles with a label assigned, and the method finds the best label for every stroke in the sketch, based on similarity between the label scribbles and the stroke, and in-between strokes (smooth labelling)."
link: "https://dcgi.fel.cvut.cz/home/sykorad/smartscribbles.html"
---

* They work on segments sampled on the strokes, instead of complete strokes/scribbles. Contiguity of segmentation along a stroke is not explicitly enforced, but encouraged through similarity measures such as proximity, alignment and creation time.
* Problem: find a labelling for each segment to minimize the combination of 2 energies:
  * Smoothness: similar strokes should share the same label
  * Respect user-guidance data: strokes similar to the scribbles should have the user-defined label
* Their similarity measure compares 2 segments by looking at: proximity, alignment, difference of radius of curvature, creation time. They use a Gaussian fall-off function.
* The "data" term finds the most pertinent scribble for each stroke (or defaults to "unassigned" label if no similar enough scribble exists) and measures similarity to that one
* They solve the labelling problem as a multi-cut problem, with each labelled scribble as a terminal. The graph has segments as nodes and edges between all segments, with a weight equal to their similarity measure. There are also edges between segments and labelled scribbles with the "data" similarity measure as weight. They solve the problem (approximate solution) by iteratively solving a max-flow/min-cut problem with only 2 terminals (choose one label as terminal $s$ and all other labels as terminal $t$, then remove the segments labelled with $s$ and continue by picking another label to be terminal $s$, while the rest are $t$, etc.)
* They show that they can use early strokes from the sketch as labelling scribbles, for example scaffold strokes
