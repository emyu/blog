---
title: "Deploying the Backend and Ui on a Server"
date: 2022-08-30T14:18:28+02:00
draft: false
tags: []
summary: ""
link: ""
---



## Use screen

```bash
# List available screens
screen -ls

# Create a new named screen
screen -S ui
screen -S backend

# Attach to a screen
screen -r 89150.ui

# Detach with Ctrl+A D
# Terminate screen with (Ctrl+C) Ctrl+D (while attached)

```

## Folders to transfer

```
video-doodles-preprocess
video-doodles-backend
video-doodles-ui
preprocessed-data
```

```
scp -v Archive.zip Session....:~/
```

Delete ply files

```
find . -name "*.ply" -type f -delete
```

## On the server

```bash
# Unzip
unzip deploy.zip
cd deploy

# Serve UI
screen -S ui
python3 -m http-server
# Detach with Ctrl+A D

# Launch backend websocket server
screen -S backend
cd video-doodles-backend
python3 app.py
# Detach with Ctrl+A D
```

