---
title: "Plane, Ray and Point"
date: 2020-05-18T11:48:08+02:00
draft: false
tags: ["Paper"]
summary: "Using bimanual interactions and simple hand gestures to create constraints on shape manipulation in VR, to help reduce the degrees of freedom of manipulation while retaining the ease of VR grab manipulation."
link: "https://dl.acm.org/doi/10.1145/3332165.3347916"
---

[Video demo](https://www.youtube.com/watch?v=BTuNCg6ahFY)

Plane, ray and point constraints can be created in the scene in VR. These constraints act as guides (like a physical plane, or a rail) and reduce the degrees of freedom with which an object can be moved in space. Hand gestures can serve to indicate the desired transformation type (translation or rotation) and direction (for the plane).

The figures are a really good example for figures in a VR paper.

User study: qualitative interviews of both expert users with 3D modelling tools and novices. They made them use the tool in a guided scenario (place objects in a room) and then interviewed them to evaluate whether the tool was relevant for experts, compared to other tools and whether the tool was learnable for novices.
