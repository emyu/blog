---
title: "Running Video Depth Playground"
date: 2022-08-31T14:23:13+02:00
draft: false
tags: []
summary: ""
link: ""
---

## Build

Docker:

```bash
cp -R /sensei-fs/users/emiliey/video-depth-playground ~/video-depth-playground
cd ~/video-depth-playground
# Build base image
docker build --memory=50g -t docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth-base:latest -f Dockerfile.base .
# Build image
docker build --memory=50g -t docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth:latest -f Dockerfile .

# Login to Artifactory
docker login -u $ldap_user -p $ARTIFACTORY_UW2_API_TOKEN docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com

# Push images
docker push docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth-base:latest
docker push docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth:latest

# Pull base image
docker pull docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth-base:latest

# Create container from image and run interactive session
docker run --name video_depth_playground --gpus all --pull -P --rm -t -i -v ~/video-depth-playground/io:/io  docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth:latest

# Run in the background
docker run --name video_depth_playground --gpus all --pull -P --rm -i -t -d -v ~/video-depth-playground/io:/io  docker-matrix-experiments-snapshot.dr-uw2.adobeitc.com/devbox/emilie-video-depth:latest /bin/bash

# In the container:
python /scripts/process_vids.py --vid /io/data/<vid_name>/vid.mp4 --output_root /io/output/<vid_name>

# Execute command in container
docker exec video_depth_playground python /bin/sh -c "python /scripts/process_vids.py --vid /io/data/vid_name/vid.mp4 --output_root /io/output/vid_name"
```

Directly on a devbox:

```bash
# Install CUDA
# Followed instructions: https://adoberesearch.slack.com/archives/CLU9ZA539/p1649356760201529
# Super important: set LD_LIBRARY_PATH

# Install Ceres
wget -q http://ceres-solver.org/ceres-solver-2.1.0.tar.gz 
tar xvf ceres-solver-2.1.0.tar.gz 
rm ceres-solver-2.1.0.tar.gz 
cd ceres-solver-2.1.0
mkdir build && cd build
cmake .. 
make -j  # might need sudo
make install  # might need sudo

# Install Colmap
git clone https://github.com/colmap/colmap
cd colmap
git checkout 0a71274e1dc5ad1b81ef8cade0999f9067d2774f
mkdir build && cd build
cmake -DCMAKE_C_COMPILER=/usr/bin/gcc-8 ..
make -j # might need sudo
make install # might need sudo

# Build libraries
cd video-depth-playground/app/tasks
mkdir -p build
cd build
sudo cmake -DCMAKE_BUILD_TYPE=Release ..
sudo make
sudo chmod 755 opt_cameras

cd ..
g++ task_ext.cpp -I/usr/include/python3.8 -fPIC -o task_ext.so -lglog -std=c++17 -lceres -O3 -I/usr/include/eigen3 -lcholmod -shared -lcxsparse -fopenmp -pthread -L/usr/local/cuda/lib64/ -lcudart -lcuda -lcusolver
```



## Resuming devbox

```bash
sensei devbox resume -sid Session-45e79cf0-d1a8-4027-8210-91b890b62f23
```



## Connecting to devbox for the first time

```bash
sensei devbox ssh_debug -sid <DEVBOX_ID> 
```



## Transfer in the data

```bash
scp -v train_vid.mp4 Session-45e79cf0-d1a8-4027-8210-91b890b62f23:~/video-depth-playground/data

# With docker system (repeat for each archive given by Emilie)
scp -v <file>.zip <Session-id>:/sensei-fs/users/emiliey/video-depth-inputs

```



## Run the script

```bash
cd video-depth-playground
python scripts/process_vids.py --vid data/train_vid.mp4
```



## Look at logs

```bash
ssh Session-45e79cf0-d1a8-4027-8210-91b890b62f23 "tail -f ~/video-depth-playground/output/logs/2022-09-01T11:29:59+0200-example_train_vid.out"
```



## Transfer out the results

```bash
scp -v -r <Session-id>:/sensei-fs/users/emiliey/video-depth-outputs.zip .
```

