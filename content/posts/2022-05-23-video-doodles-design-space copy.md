---
title: "Video Doodles Design Space"
date: 2022-05-23T11:43:07+02:00
draft: false
tags: ["Video Doodles"]
summary: "Design space exploration."
link: ""
---

## Classification of effects

I tried to define axes (columns/letters) and categories (rows/numbers) based on which we can classify any example video we find. Any given example can fit in at least 1 category for each axis. Some examples might fit multiple categories of the same axis, as some effects can be composed (eg, a sketch can be moving by tracking an object A3, and exhibit secondary motion A4).

The following examples should help precise what each category means, but as you will see, some examples could be performed by different combinations of effects (eg, instead of generating secondary motion procedurally A4, a similar effect can be achieved by sketching frame-by-frame). It will be up to us to define the scope of our application, and which effects are automated VS hand crafted, or not supported.

{{<table "table table-striped table-bordered">}}

|        Sketch (global-level) animation in scene space        |       Sketch (strokes-level) animation       |     Sketch geometry      |     Sketch motion type      |
| :----------------------------------------------------------: | :------------------------------------------: | :----------------------: | :-------------------------: |
|                         (A1) Static                          |              (B1) Fixed sketch               | (C1) planar view-aligned |          (D1) None          |
|                  (A2) Moving (user defined)                  | (B2) User defined (frame-by-frame animation) |       (C2) planar        | (D2) Affine transformations |
|                (A3) Moving (tracking object)                 |  (B3) Procedural (agnostic of scene/motion)  |         (C3) 3D          |  (D3) Freeform deformation  |
| (A4) Procedural/secondary motion (controlled by scene motion) |                                              |                          |                             |
|    (A5) Procedural motion (interacts with scene geometry)    |                                              |                          |                             |
|     (A6) Procedural/secondary motion (agnostic of scene)     |                                              |                          |                             |

{{</table>}}

Moving forward we should pick a subset of effects we plan to support. My current take is that the most interesting combinations are:

* (A) Sketch animation in scene-space:
  * A1 is technically super simple to achieve but very useful in practice (found in lots of examples)
  * A3 is both technically challenging and useful
  * A4 is present in many previous work, but could be a nice addition for impressive results
* (B) Strokes-level animation
  * B1 + B2 to keep the nice frame-by-frame animation look, more creative freedom. The philosophy could be "we don't touch your individual strokes".
* (C) Sketch geometry
  * C1 + C2 are easy to achieve and very useful (almost all examples)
  * (C3 but with simple canvas geometry, like ellipsoids, cylinders, cubes. Or planes that can be deformed. I think a really cool aspect of this project could be to not go full into 3D sketches, but still give the illusion of 3D => What is the least 3D we can get away with?)
* (D) Sketch motion type
  * D1 + D2
  * D3 only as a way to support A4

I would leave aside (or only as "bonus" features to create cool results) any effect that does not directly benefit from knowledge of the 3D scene and 3D motions. For example, particle animation effects are nice but having it is not a contribution.


## Examples

### Kid playing with imaginary decor and toy trails

{{< video src="media/2022-05-23-video-doodles-design-space/kid_tracking_toy.mp4" caption="" width="600px">}}

Classification:

* Decor: A1+B1/B2+C1+D1
* Toy trails: A3+B2+C1+D2
* Rain drops: A5/A6+B2+C1+D2

Upon "colliding" with the ground, the rain drop sketch becomes a splash sketch. This could be seen as a (A5) procedural "switch" controlled by interaction with scene geometry, or simply as user-defined sketch keyframes (B2).

### Kid with swirl

{{< video src="media/2022-05-23-video-doodles-design-space/kid_swirl.mp4" caption="" width="600px">}}

Classification: A3+B2+C3+D2

### (Slightly) moving camera with 2D animation loop and shadow

{{< video src="media/2022-05-23-video-doodles-design-space/brill_bird_shadow2.mp4" caption="" width="600px">}}

Classification: A1+B2+C2+D1+E2

or if we consider the bird to be decomposed in different layered parts, we can imagine that they have some  secondary motion: A6+B1+C2+D1+E2

The shadow and character lie on different scene-space planes that are not view-aligned.

{{< video src="media/2022-05-23-video-doodles-design-space/brill_naga_occlusion.mp4" caption="" width="600px">}}

In this one we see also occlusion.

### Animated creatures and particles

{{< video src="media/2022-05-23-video-doodles-design-space/dby_creatures.mp4" caption="" width="600px">}}

Classification:

* Most little creatures: A1+B2+C1+D1
* Flying creatures at 0:05 and 0:22: A2+B2+C1+D2
* Big creature at 0:39: A1+B2+C3+D1

At 0:28 there is a cool example of sketches that really "float" in mid air and are not defined in close spatial relation to in-scene objects.

### In-scene text insertion

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_wheel.mp4" caption="" width="600px">}}

Classification: A2+B1+C1+D2

Here the text is affixed to the wheel but only in translation, its 2D rotation is fixed.

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_rotation.mp4" caption="" width="600px">}}

Classification: A2+B1+C2+D2

The text lies on a 3D plane that rotates with the athlete.

{{< video src="media/2022-05-23-video-doodles-design-space/seattle_reflexion.mp4" caption="" width="600px">}}

Classification: A1+B1+C2+D1

There is a reflexion effect between the text and the water.

### Animated lines traced over objects in the scene and labels

Classification:

* for lines: A1+B3+C3+D1
* for panels: A1+B3+C2+D1

Here the animations are purely stroke-based/decal-based procedures. Otherwise the sketches can be considered static wrt the scene.

{{< video src="media/2022-05-23-video-doodles-design-space/3d_animated_line.mp4" caption="" width="600px">}}

### Procedural rain particles that bounce off umbrella (from *Secondary Motion for Performed 2D Animation*)

Classification: A5+B1+C1+D2

With knowledge of partial 3D scene geometry, we could create procedural animations that interact with the scene, for example with collisions between procedurally animated particles and the scene.

{{< figure src="media/2022-05-23-video-doodles-design-space/collision.gif" caption="" width="600px">}}

### Train smoke that comes out of the train chimney (TODO: make video)

Classification: A3+B1/B2+C2+D2(+A4+D3)

The whole smoke sketch tracks the moving train (A3) with an affine transformation (D2), and it could be given some additional motion by computing secondary motion effects based on the motion (A4) which would result in freeform deformation of the canvas (D3). All of this can be composed with (B2) frame by frame sketched animation to give the impression that the smoke rises from the chimney to the sky and dissipates.

## Positioning to related work

| Papers | Scene aware (sketch looks placed in the scene when camera moves) | Depth aware (occlusions) | Tracking moving object | Secondary/procedural effects | 3D sketches |
|:--------:|:--------:|:--------:|:--------:|:--------:|:--------:|
| PoseTween |  |  | (limited to moving humans) |  |  |
| Keyframe-Based Tracking for Rotoscoping and Animation |  |  | (limited to curves around image contours) |  |  |
| RecReality |  |  | x | x | (planar non-view aligned) |
| Pronto | x |  |  |  |  |
| Secondary Motion for Performed 2D Animation |  | (by layering) |  | x |  |
| Unwrap Mosaics | x | x | (by sticking to in-scene surfaces) |  | (by mapping to in-scene surfaces) |
| Layered Neural Atlases | x | x | (by sticking to in-scene surfaces) |  | (by mapping to in-scene surfaces) |
| Video Object Annotation | (by "tracking" static scene regions) | (occlusion can be detected roughly) | x |  | (planar non-view aligned, by fitting homographies) |
| SweepCanvas | x |  |  |  | x |
| After Effects | x (3D Camera Tracker) |  | (screen space tracking) |  | ? |

Are there any related work on inserting digital 3D objects into 2D videos? From technical or UI perspective.

## Technical and UI challenges

* (A1) Let the user place a static sketch in the scene (with moving camera)
  * Technical
    * Camera parameters inference at each frame
    * Placing the sketch at the correct depth from multi-view 2D info
    * Depth map inference at each frame
  * UI
    * Defining 3D position through 2D affine transformation keyframes
* (A2) Let the user define a 3D trajectory for a sketch (with moving camera)
  * UI
    * How to let the user set a 3D trajectory with only a 2D UI? Keyframing or puppeteering in 2D seems like a natural interaction
  * Technical
    * From a 2D screen-space trajectory, determine a "plausible" 3D scene-space trajectory => how to regularize this ill-posed problem?
* (A3) Let the user affix a sketch to a moving object in the scene
  * Technical
    * Detect main 3D motions in the scene (track sparse features across the whole video => segment all trajectories into a set of main motions, each motion represented by an affine transform). This could be done with an iterative algorithm, EM, PEARL, etc. Working on a set of sparse features can help with performance. We could do the segmentation with a smoothness term that takes a 3D distance metric into account
    * From user interaction (eg puppeteering/keyframes) detect most plausible affixing target?
  * UI: same interaction as above, or some kind of affixing scribble as in RecReality. But keeping only keyframing/puppeteering interactions would be the best imo.
* (A4) Let the user add automatically generated secondary motions to a given sketch
  * Similar to other papers, such as *Secondary Motion for Performed 2D Animation*
* (B2) Frame-by-frame animation
  * provide a simple sketching/onion skinning UI
* (C2/C3) let the user define 3D planes or other 3D geometry as canvas for sketches
  * UI
    * how should we let users indicate the 3D rotation of the 3D planes? If we want to keep a 2D UI feel, maybe we can let the user sketch out or edit a plane in image-space to show what they want the plane to look like in the image.
    * how can users define non-planar canvases?