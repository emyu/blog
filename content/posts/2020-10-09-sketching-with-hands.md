---
title: "Sketching With Hands"
date: 2020-10-09T11:17:07+02:00
draft: false
tags: ["Paper"]
summary: "2D sketching system using tracked non-dominant poses to create visual sketching context and 3D geometry that can be used to define sketching planes quickly."
link: "http://sketch.kaist.ac.kr/pdf/publications/2016_uist_sketchingwithhands.pdf"
---

They start by providing a litterature review of best practices in product design, in order to motivate their interface design choices.

Cool interface design stuff:

* To access secondary features such as erasing or navigating the scene, they use quasi modes: each mode is enabled only when the non dominant hand is pressing a specific button. To associate more modes with a small number of buttons (4 buttons on the tablet), they add an "intensifier" button, which serves to go in a variation of the main mode associated with the button (eg: delete whole strokes/delete parts of strokes, rotate/pan)
* Delete mode enables users to delete strokes but also constraints (on the sketch plane position) and sketch planes. They do say that some users got into situations where unexpected things got erased by mistakenly crossing them, so there would need to be a smart selection of what thing to delete to prevent that
* Fixing hands in space is done simply by not moving for a few seconds, and modifying hands is done by inversely "melting" the hand previous position. They fix hand position and rotation by default (but can be unconstrained by tap interactions if needed), enabling users to finely modify fingers without messing up the main hand pose
* Responsive spangles allow to visualize hand contact or overlaps with the sketch
