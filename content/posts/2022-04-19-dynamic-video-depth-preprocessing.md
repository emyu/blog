---
title: "Dynamic Video Depth Preprocessing"
date: 2022-04-19T10:35:39+02:00
draft: false
tags: []
summary: "Trying to figure out what the preprocessing for this paper is."
link: ""
---

* Estimate a sparse feature point cloud and camera poses:
  * ORB-SLAM2 + COLMAP
  * Motion masks (Detectron2?) + multi-view stereo stage of COLMAP (with moving parts masked)
    * Motion masks: DAVIS dataset, or run Detectron2 using script from robust_cvd
    * COLMAP: use script from `consistent_depth` repo to get:
      * cameras.txt, images.txt, points.txt
    * Convert those into the format expected by next stage
* Expected output from preprocess stage:
  * file.intrinsics.txt
  * file.matrices.txt
  * file.obj
* Then run "preprocess" scripts from the repo
  * Generate per-frame data `python3 ./scripts/preprocess/davis/generate_frame_midas.py` : scale camera translations so that the scale of the reconstructed world is such that the depth scale roughly matches the scale from single-view depth estimates (from single-frame NN).
  * Generate optical flow between pairs of frames `python3 ./scripts/preprocess/davis/generate_flows.py`
  * Pack camera calibrations and images into training batches `python3 ./scripts/preprocess/davis/generate_sequence_midas.py`