---
title: "Scaffold Examples"
date: 2020-06-15T16:31:27+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Some examples of scaffold use cases and how the current method behaves around those cases."
link: ""
---

Here the user draws a straight scaffold line to plan ahead where the top of the half dome will be. With the scaffold line present, the cycle detection algorithm fails. The user can continue sketching, and when they don't need the scaffold line anymore they delete it. Deleting it makes it possible for the algorithm to detect cycles correctly.

{{< video src="media/06-15/06-15-straight-scaffold.mp4" caption="" width="">}}

Here is an other example with a "skeleton" curve. The user first sketches the centerline of the shape. This scaffold stroke, again, makes it impossible to detect the correct cycles. Once it is deleted, the cycles are formed back as expected.

{{< video src="media/06-15/06-15-curved-skeleton.mp4" caption="" width="">}}
