---
title: "PEARL Tests"
date: 2021-01-19T09:55:25+01:00
draft: false
tags: ["Surfacing Project"]
summary: "Main problems at this stage."
link: ""
---

## Problems

* Similarity measure relies on a constant that depends on the scale and sparsity of the sketch. And the segmentation result is sensitive to this measure.

See on the sewing machine which has a sparser bottom part: varying the proximity radius yields better or worse segmentations for either top or bottom parts. (green segments are edges in the graph)

{{< figure src="media/2021-01-19/sewing-machine-02.png" caption="sigma_prox = 0.02, segmentation = 9 subsets" width="300px">}}

{{< figure src="media/2021-01-19/sewing-machine-04.png" caption="sigma_prox = 0.04, segmentation = 9 subsets" width="300px">}}

{{< figure src="media/2021-01-19/sewing-machine-05.png" caption="sigma_prox = 0.05, segmentation = 4 subsets" width="300px">}}

* Interior surfaces, section planes are detected
  * Introduce a check based on known/inferred surface normals (eg: at an intersection/near-intersection, we can guess the normal)

{{< figure src="media/2021-01-19/mouse-section.png" caption="Section plane on the mouse" width="300px">}}

* Depends on initialization (it can get stuck in a local minima very easily). When smooth initialization: this is sometimes directly a local minimum.
  * Best of multiple trials
  * Introduce subset splitting/merging strategies
  * Add label cost that depends on model complexity cost: penalize complex models

{{< figure src="media/2021-01-19/mouse-energy-74.png" caption="energy = 74, segmentation = 6 subsets" width="300px">}}

{{< figure src="media/2021-01-19/mouse-energy-98.png" caption="energy = 98, segmentation = 5 subsets" width="300px">}}

{{< figure src="media/2021-01-19/mouse-energy-120.png" caption="energy = 120, segmentation = 5 subsets" width="300px">}}

* Some strokes need to be assigned to multiple models

{{< figure src="media/2021-01-19/sewing-machine-missing-patch.png" caption="energy = 120, segmentation = 5 subsets" width="300px">}}

## To test

* Save the result to one solve and set it as initialization to see if the algorithm stays on the solution.
* Add a "no model" class, to try to capture strokes that don't lie on a surface
* Better similarity cost: enforce strong similarity at smooth intersections and low similarity across sharp intersections

{{< figure src="media/2021-01-19/mouse-subset-boundaries-wrong.png" caption="The boundaries between subsets are arbitrary, we would like to encourage separations at intersections that are sharp features, and discourage them at smooth intersections" width="300px">}}

* Control span of a model: prevent models from reaching too far (from what? what radius?)

## Using the result

* Cut patches along the "outer boundary" of the strokes (maybe convex hull)
* Unwrap strokes along its best model, but how to get a consistent map across the whole sketch?
  * Then reason in 2D space: coloring algorithms?