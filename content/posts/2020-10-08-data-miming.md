---
title: "Data Miming"
date: 2020-10-08T11:33:17+02:00
draft: false
tags: ["Paper"]
summary: "Study on how people describe 3D objects with their hands mid-air, without any feedback. Based on the insights from this study, they propose a prototype of shape retrieval from user's hand motion."
link: ""
---

## How do people describe objects with their hands

* Both hands are used to describe symmetric parts
* Surfaces are "wiped" with the hand, often in a repeated motion. If the surface is curved, the participant adapts the shape of their hand to match it
* Smaller components such as bars or legs of a chair/table are represented differently, by moving a closed fist or in a pinching motion of index and thumb
* Shape was abstracted such that the participant focused on representing its main function, ignoring less important parts
* Participants moved their hand slower when actually representing a surface, and faster, in a relaxed position when moving in-between parts

Based on these insights, they implement a shape retrieval prototype, where they try to detect which shape the user is describing by matching to a database of potential shapes. They represent hand motion data by a voxel grid where each voxel holds a certain quantity, that becomes bigger the more the hand passes through it. They don't achieve such a good match rate, but still pretty good, at least the rough shape of the object is well described by the participants. Given that the participants received no visual feedback on their hand motions, it shows that the short-term spatial memory works quite well to anchor the object representation mid-air during the "mime".

## Modelling with hands related work

* SURVEY AND INVESTIGATION OF HAND MOTION PROCESSING TECHNOLOGIES FOR COMPLIANCE WITH SHAPE CONCEPTUALIZATION, Varga et al. -- State of the art in 2004 (most of the work presented did not have good enough hand tracking tech available at the time, still they present a good overview of the different tech used to track hands and the applications to shape design that emerged).
* Methods based on gestures:
  * Instant 3D design concept generation and visualization by real-time hand gesture recognition, Kang et al., 2013
  * Shape-It-Up: Hand gesture based creative expression of 3D shapes using intelligent generalized cylinders, Vinayak et al., 2012
* Methods based on hand motion:
  * Virtual Pottery, Han et al., 2013
  *  3-D Drawing System via Hand Motion Recgriition from Two Cameras, Abe et al., 2000 (this is about sketching and not modelling, the main challenge seemed to have been with hand tracking and hand pose detection tech)
  * Twister: A Space-Warp Operator for the Two-Handed Editing of 3D Shapes, Llamas et al., 2003
  * Conceptual design and modification of freeform surfaces using dual shape representations in augmented reality environments, Fuge et al., 2011 (this one seems pretty interesting, should read)
  * BodyAvatar: Creating Freeform 3D Avatars using First-Person Body Gestures, Zhang et al., 2013 (whole body motion based modelling)

