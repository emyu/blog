---
title: "3D Curve Creation on and Around Physical Objects With Mobile AR"
date: 2021-01-07T09:42:37+01:00
draft: false
tags: ["Paper"]
summary: "Using a phone with inside out ARKit tracking to sketch curves on and around objects. The tracking result is very noisy, especially due to occlusions close to objects. They provide a simple interface on the phone to correct the erroneous parts of the curves manually, guided by confidence information about each point of the strokes about tracking quality."
link: "http://sweb.cityu.edu.hk/hongbofu/doc/3D_Curve_Creation_Mobile_AR_TVCG.pdf"
---

* They look at different "pen tip point" and hand grip on the phone, both by looking at how people would intuitively do it and at how performant the chosen combination is for tracking.
* Inside out AR tracking has a lot of artefacts of different kinds: the world origin can shift, short tracking loss due to occlusion of the camera or lack of features in the background lead to broken curves or local drift of the correct position (ARKit tries to infer the correct position but is not very precise at that without vision tracking).
* They propose 2 correction procedures:
  * Automatic: they remove pieces of the curve that has known bad tracking (ARKit data) and replace them by a blend of forward and backward integration of the acceleration on known parts of the curve.
  * Manual: users can correct the curve by redefining world origin and constraining the curves to go through given points (all this is done on the phone)
* The manual correction is done by a global curve optimization which minimizes an energy of fidelity to the original shape (weighted by confidence in the tracked point), a soft constraint term for position constraints and a smoothness term. The curve is represented as a polyline.
* Application: measuring curved objects, some sketches by tracking on object surfaces and around them (some of those could be used to test surfacing method).