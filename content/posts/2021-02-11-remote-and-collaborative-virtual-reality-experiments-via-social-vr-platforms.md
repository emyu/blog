---
title: "Remote and Collaborative Virtual Reality Experiments via Social VR Platforms"
date: 2021-02-11T15:14:08+01:00
draft: false
tags: ["Paper"]
summary: "They show how quantitative and qualitative user studies for VR interactions can be run in social VR platforms like VR Chat, and what would be the advantages of doing so."
link: "https://osf.io/3crhg/"
---

* They provide interesting points and references as to what are the challenges of evaluating VR systems without depending on running all studies within a lab (and existing solutions)
  * Web platform to recruit participants: https://www.xrdrn.org/
  * Practical advice and ethical challenges for running VR user studies outside of the lab: http://interactions.acm.org/archive/view/july-august-2020/evaluating-immersive-experiences-during-covid-19-and-beyond
* VRChat is a promising platform to run user studies
  * cross-platform, respects privacy, free
  * supports user-generated content, written in c# and with access to part of the Unity SDK
  * big and active community, makes it easy to recruit participants equipped with hardware (23 particpants recruited in 1 day). These participants are also experienced with VR and the platform itself, mitigating some potential issues that arise with novices to VR
* They showcase 2 reproductions of user studies, both quantitative and qualitative research. They also provide lots of interesting advice on setting up such a study.
* Issues with VRChat:
  * some features lacking: transfer of data to outside of the client, making it hard to collect study data
  * the platform may disappear or change such that it isn't suitable anymore (cost, privacy, features): it may be worth creating a custom social VR platform specifically for VR user studies, a bit like Mechanical Turk

