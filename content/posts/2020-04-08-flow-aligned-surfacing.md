---
title: "Flow Aligned Surfacing"
date: 2020-04-08T15:10:34+02:00
draft: false
tags: ["Paper"]
summary: "Surface a curve network while taking into account curvature indications given by some curves of the network, using an iterative optimization to target surface curvature aligned with a smooth flow field (orthogonal frame field with magnitude). "
link: "https://haopan.github.io/curvenet_surfacing.html"
---

## Detecting flow lines among all curves

Their method is based on creating a smooth flow field constrained at the boundaries by directions given by the "flow lines". Flow lines are a family of curves among all the curves in the curve network, that represent dominant curvature lines. They are often represented by designers when depicting a 3D object with curves.

They classify a curve as being a flow line by looking at a rotation minimzing frame along the curve, starting at one endpoint where its orientation is defined by the direction of the curve tangents at the intersection. If the normal given by the RMF at the other endpoint is close to the normal computed by looking at the intersecting curves, then the curve is a flow line.

They also detect which lines of the network are smooth transition curves, ie curves across which the surface should stay smooth.

## Iterative surface optimization using a flow field aligned with flow lines to express curvature directions

Start with an initial surface mesh obtained eg with [[Zhuang and Zou]](https://www.cse.wustl.edu/~taoju/zoum/projects/CycleDisc/), then iteratively:

* Compute flow-field on the mesh obtained at step $i$
  * Directions: smooth, constrained at flow lines
  * Magnitudes: consistent with principal curvatures of the mesh at step $i$, found by local optimisation (by triangle of the mesh) and then control variation
* Modify the mesh of step $i$ such that its curvature field (principal curvature and principal directions at each point) aligns with the flow-field
  * Expressed by computing the target mean curvature by edge (using the flow field) and optimising for that
  * Add constraints at the flow line curves: enforce neighboring edges to the flow line edges to be perpendicular to the curve normal with an additional term in the objective function
  * Add smoothness term to the objective function: surface should be smooth (minimise mean curvature) on edges corresponding to smooth transition curves

Until the variation of vertex positions are negligible.

In practice, they only need a few iterations. Their whole method is quite fast, a few seconds only for a typical model.
