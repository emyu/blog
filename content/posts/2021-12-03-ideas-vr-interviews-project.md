---
title: VR sketching for 3D design/artistic expression
date: 2021-12-03T22:11:53+01:00
draft: false
tags: ["Physical 3D Paint"]
summary: "Where to go with the VR artists interview project."
link: ""
---



## Thoughts on the project


* Would be better to have a small system design component to the paper because we are NOT confident with writing a fully theoretical HCI paper, this is not playing to our strengths

  * Participatory / co-design approach
    * Find concrete examples of that method
    
  * There are many examples of papers where there are formative interviews beforehand to define some design guidelines
    
    * Preserving Hand-Drawn Qualities in Audiovisual Performance Through Sketch-Based Interaction
    * Extending Manual Drawing Practices with Artist-Centric Programming Tools
    
    * Would force us to focus on few artists that share a similar approach (eg: focus on Tiltbrush performers, or GravitySketch designers that use the ink tool + subdivision surfaces, or Quill animators)
    
  * What we will do in practice will fit in the previous ethics contract
  * How to go from the interview findings to a "system" for which we can derive design guidelines/goals?
    * Maybe 2 phases of interview: first broad, exploratory, to settle on a particular direction, then refine based on particular topic we choose
    * Or think beforehand of a few leads (eg: live/dance performance of sketching, viewpoint dependent art, relation between strokes and a volume, from 2D to 3D sketch, performing animation in VR on 3D sketches) and select participants based on that

* Narrowing down our subject will allow us to move away from theoretical and more towards one or multiple concrete take aways that we could apply in a small prototype that would be a part of the paper (+ evaluate with our artist co-designers)

* Go back to the list of participants, try to find out which sub group would be most easy to collaborate with
  * How probable is it that the person agrees to work with us?

  * Maybe establish a first contact to see how they feel about this project

  * Nb of contacts in the subgroup (eg: tiltbrush performers are probably the most developped community)

    

## Volumetric brushes idea

* Current VR painting applications follow a vector paradigm or 3D polygon/surface paradigm as opposed to a raster/volume paradigm
  * Cannot erase part of a stroke
  * Cannot blend strokes of different colors
  * Textured stroke are textured ribbon surfaces or a hand crafted parametric shader along a path
* Raster/vector opposition in 2D sketches? Influence on perceived creativity assistance of the digital tool?
  * Motivation, or hypothesis as basis for an observational study (technology probe, like Polyphony)
* Are there equivalent traditional mediums that can serve as inspiration for better interactions / feels of the rendering?
  * Light painting
  * Some parallel to physical painting process --> Alison Goodyear
* Are there any technical challenges in bringing a raster feel to VR painting?
  * Full volumetric approach: for rendering Dreams PS4 is SOA (no way we can do better), brush authoring maybe?
  * Mixed representation
* What are the *cool stuff* that we would like to do with a volumetric painting approach that can't be done with the surface-strokes?
  * Volumetric media: clouds, smoke
  * Color mixing: smudge tool, watercolor-like painting with paint mix, color palette
  * Intricate brushes: equivalent of "chinese ink brush" or brushes that have some texture
    * This needs very fine resolution

* Problems:
  * It seems like it is hard to beat simple billboards with appearance that adapts to the view angle, from an appearance perspective
  * Number of hard technical challenges in efficient rendering of volumetric sotrkes

## Other interview + prototype / technology probe projects

* Mutliple tools with common aspects
  * Palettes
  * Diagramming
  * Ikea Hacking
  * Related Work Sketch/Prototype paper
* Prototype after interviews
  * Supporting Reference Imagery for Digital Drawing
    * Formative interviews to identify current ad hoc solutions for the use of reference imagery, and the issues that they raise
    * Technology probe: provide limited amount of new features that are open-ended and see in which way users adopt them, inspire new workflows. Collect usage data and analyze it
  * Technology Probes

## Problems with VR sketching/painting

* Precision
  * what are the ad hoc solutions that artists use to help them with precision? (over-sketching, modifying strokes after the fact)
  * how important is precision to them?
  * how bad of a problem is it for trained artists?
* Defaulting to sketching in 2D on an imaginary plane
* Viewpoints for the finished artwork
  *  if viewpoints are known, no need to draw all the 3D, fake it
  * fixed viewpoint / viewpoint distribution(s) / known camera path / free viewpoint navigation for the viewer
  * how do artists define those viewpoints? how do they adapt their work based on this knowledge?
* The medium (software / brushes available) dictates the aesthetic (Alison Goodyear, What we can learn)
  * do artists feel constrained to a particular aesthetic when using Tiltbrush/GS/Quill? positive/negative perception of that?
  * what actions do they take to push the boundaries of that aesthetic and make it their own?
* Geometry, lighting and materials: which aspects of a 3D painting do the artists paint and which aspects do they expect to be defined computationally?
  * strokes acting as flat colored brush strokes VS strokes acting as clay / geometry: how do artists "light" their paintings?
  * do artists leverage the fact that the strokes form 3D geometry to automate lighting? what are obstacles to doing that? (I expect that stroke geometry is not good enough to have nice lighting, lack of controllability)
