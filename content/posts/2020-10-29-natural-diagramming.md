---
title: "Natural Diagramming"
date: 2020-10-29T10:57:49+01:00
draft: false
tags: ["Paper"]
summary: "Study of how people author diagrams, to show that good diagramming tools are lacking and to establish design guidelines for future diagramming tools."
link: "https://www.cs.cmu.edu/~woden/assets/chi-20-natural-diagramming.pdf"
---

* Semi-structured interviews with 18 participants from diverse backgrounds but who all regularly author diagrams
  * Interviews followed a [script](media/diagramming-interview-script.pdf)
  * Participants were asked to bring diagrams they've done before, and given pen and paper so that they could doodle to explain stuff
* Thematic analysis was done on the transcripts to extract main themes. Quotes and illustrations back the overall claims for each theme.
* Finally they boil down their findings into guidelines for new diagramming tools

