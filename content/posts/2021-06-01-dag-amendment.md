---
title: "Dag Amendment"
date: 2021-06-01T18:35:02+02:00
draft: false
tags: [Paper]
summary: "Control hyper parameters of parametric shapes by direct interaction in shape space. They define a way to compute the Jacobian of the 3D shape wrt the hyper parameters."
link: "https://perso.telecom-paristech.fr/boubek/papers/DAG_Amendment/"
---

* To allow for control of the hyper parameters from deformation in shape space (similar to inverse kinematics), they need to have the Jacobian of the action of hyper parameters onto a point of the mesh
* This jacobian matrix can not be measured easily because a change in hyper parameters introduce changes in connectivity, so there is no mapping between 2 different result shapes corresponding to different values of hyper parameters (so we can't measure the derivative as a difference between a tracked point)
* They define a co-parametrization, that defines a correspondence between points of different shapes (corresponding to different parameters). The co-parameter of each point is computed by leveraging the DAG of operations that construct the shape. Each point can be identified by the "path" of operations that led to its creation, and by a texture coordinate on the original shape at the leaf.
* They "amend" the DAG to add some nodes that manage the computation of these "paths" ID
* Once they have the Jacobian they solve for the hyper parameter change given a point displacement in 3D space by using IK methods
