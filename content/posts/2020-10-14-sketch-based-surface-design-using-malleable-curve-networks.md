---
title: "Sketch-Based Surface Design Using Malleable Curve Networks"
date: 2020-10-14T08:42:23+02:00
draft: false
tags: ["Paper"]
summary: "Automatic surfacing of a sketched curve network (with manually enforced connectivity) with a subdivision surface that is iteratively smoothed to minimize curvature variation. The user can decide to let surface smoothing modify some curves of the network, by projecting those back on the smoothed surface."
link: "https://www.sciencedirect.com/science/article/pii/S0097849312001446?casa_token=8eT-f1aWlsMAAAAA:TjOBpnj3_QYsM02tyfs7V4s_Mze6VSDWz3sx_wukpcbi7_rMXfQ42N7oNWZFzXnSJVl6NqBLZQ0"
---

Video demos: https://youtu.be/bbzQ_yXEEbE, https://youtu.be/KeQInVIX_BM

## Motivation

* Feature curves are very important in object design as they describe specific constraints in overall look of the object (can be defined prior to 3D modelling by a 2D sketch for example).
* But it is difficult to obtain the desired feature curves as intersections of smooth patches, it is easier to define the surface from those curves.
* It is also difficult to create a curve network that satisfies the requirements to create a smooth G2 surface (for example some curve junctions should be G1).

## What they do

* Sketch-based interface similar to ILoveSketch
* The user should draw the curve network by joining curves only at the endpoints, and further indicate intersections by an additional interaction. So network topology should be always carefully planned
* The user can define free, fixed and crease curves that define different constraints
* From the network, they infer which cycles should be surfaced. It seems that they look for cycles by assuming that cycles most probably have a defined number of edges and query these cycle topologies in the graph, until all edges are manifold (2 bounding cycles). Some cycles also need to be excluded, if they cause edges to be non manifold. They cite *Identification of faces in a 2d line drawing projection of a wireframe object* but it is very unclear what the link to that method is.
* They fit bicubic Coons patches to 4-sided loops and NURBS patches to other loops. They take samples from these patches to define the initial control mesh vertices. The control mesh defines a limit subdivision surface (Catmull-Clark subdivision).
* They have 2 objectives once these initial surfaces are defined:
  * Smooth the limit surface: V-spring smoothing
  * Constrain the limit surface to lie on the curve network (or at least on a subset of fixed curves): moving the control mesh vertices that lie on the curves to bring the limit surface closer to the curve vertex, then apply Laplacian reconstruction to the unconstrained control mesh vertices
* They iteratively modify the control polygon mesh to satisfy these 2 objectives
* Across crease curves they allow the limit surface to have 2 different normals that are free to deviate from each other in the smoothing operation
* Once the surface is smoothed and fitted to the fixed and crease curves, the user can choose either to fit the free curves to the surface (ensuring best smooth surface) or to fit the surface to the remaining curves
* Side cool thing: adjusting crease sharpness to define sharp edges or smooth bevels
