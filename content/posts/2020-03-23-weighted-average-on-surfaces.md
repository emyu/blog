---
title: "Weighted Average on Surfaces"
date: 2020-03-23T10:27:45+01:00
draft: false
tags: ["Paper"]
summary: "Method to compute weighted averages of points on a surface (triangle mesh). They also address the inverse problem of finding the weights associated with a target point, given some anchors on the surface. All these results work at interactive speed, once some pre-computation on the mesh is done."
link: "https://igl.ethz.ch/projects/wa/"
---

They show that finding the Fréchet mean of points on a surface (with the geodesic distance as metric) is equivalent to finding the weighted average of all the anchors in an euclidean embedding space $R^D$ (of dimension d > 3), and then projecting on the embedded mesh.

So their method consists of the following blocks:

* Construct a Euclidean-embedding metric, such that with this euclidean metric the distance between a pair of points is approximately the geodesic distance between them. They do so by applying MMDS (Metric Multidimensional Scaling) on a subset of points of the mesh, then find the embedding of the other points by using least squares meshes [Sorkine 2004]. This takes a few minutes, but they only need to do it once per mesh.
* When they need to compute a weighted average, they compute it in the Euclidean-embedding metric, and then use Phong projection to find the point lying on the mesh that is closest to that average in $R^D$. Phong projection is a smooth projection on the discrete triangle mesh, it projects along the interpolated vertex normals, similar to those used for Phong shading. However it is not trivial to define such a projection in  $R^D$, they define a method to do so.

