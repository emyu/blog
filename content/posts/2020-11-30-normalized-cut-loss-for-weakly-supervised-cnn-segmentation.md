---
title: "Normalized Cut Loss for Weakly Supervised CNN Segmentation"
date: 2020-11-30T18:03:30+01:00
draft: false
tags: ["Paper"]
summary: "Weakly supervised learning method to learn a CNN from scribble labelling of the training images. They introduce a new loss based on normalized cut energy, which enables them to beat state of the art segmentation CNNs for weakly supervised data."
link: "https://openaccess.thecvf.com/content_cvpr_2018/papers/Tang_Normalized_Cut_Loss_CVPR_2018_paper.pdf"
---

* Other weakly supervised methods based on scribble annotations like [SribbleSup](https://openaccess.thecvf.com/content_cvpr_2016/papers/Lin_ScribbleSup_Scribble-Supervised_Convolutional_CVPR_2016_paper.pdf) are based on iteratively inferring a full image segmentation using eg a graph cut method, then learning the CNN parameters based on those full image segmentations for supervision, and doing that until convergence.
* Instead they propose to use a loss based on similar ideas: [normalized cut](https://people.eecs.berkeley.edu/~malik/papers/SM-ncut.pdf) loss, which expresses a kind of regularization objective. Similar pixels (in RGBXY space) should have the same label. They express a gradient for this energy, so they can use it to train their network. Normalized cut 
* They use a cross-entropy loss that is partial, where they only look at the pixels with labels from the scribbles to train the network. Once this is done, they fine-tune it with the normalized cut loss.
