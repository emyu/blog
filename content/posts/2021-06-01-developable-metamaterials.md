---
title: "Developable Metamaterials"
date: 2021-06-01T18:34:33+02:00
draft: false
tags: [Paper]
summary: "Introducing a metamaterial composed of ruffles of a flat sheet of material, that can present different bending resistance. It can be fabricated flat and then assembled in a straightforward way."
link: "https://igl.ethz.ch/projects/developable-metamaterials/"
---

