---
title: "FiberMesh"
date: 2020-03-23T10:51:23+01:00
draft: false
tags: ["Paper"]
summary: "Method to design 3D freeform surfaces by sketching and deforming 3D curves."
link: "https://igl.ethz.ch/projects/FiberMesh/fibermesh.pdf"
---

## Interface

The user can define a 3D surface by sketching closed 2D curves, extruding from a base or cutting as in Teddy. They can also iteratively refine the shape of a curve, and thus, the surface, by pulling on the sketched curves. The sketched curves act as handles that can control the deformation of the surface. Other tools that they provide: smoothing a curve by rubbing it, changing the type of a curve from a sharp curve to a smooth curve (whether the surface should be smooth across the curve).

This modelling method seems to make it easier for some users as they can refine their goal gradually, they don't need to draw the exact shape from the start.

## Surface optimization from curves

To create a surface that smoothly interpolates the control curves, they use a sequence of optimization problems. Their solution is inspired from [Schneider and Kobbelt 2011](https://www.graphics.rwth-aachen.de/media/papers/geom_fair1.pdf) and it consists of solving 2 second order problems one after the other and iterating, to optimize the fourth order condition $\Delta H = 0$, $H$ being the mean curvature. Effectively, the goal is to have a surface with mean scalar curvature values that are smooth and interpolate the known values at the boundary (input curves).

* Solve least square problem to find target Laplacian Magnitudes $c_i$ at each vertex (the energy requires that they vary smoothly across vertices and that they are close to the values at the previous iteration, starting with fixed values only on the curves)
* Using these target Laplacian Magnitudes, least square solve for the vertices position that best fit the condition: $L(v_i) = A_i \cdot c_i \cdot n_i$ (1) with $A_i$ an area estimate and $n_i$ a normal estimate obtained from the vertex position at the previous iteration. Here a term also enforces the position constraints on the curves.

Similarly to the Laplacian Magnitudes, the target edge lengths are also optimised to vary smoothly so that the discretized Laplacian that they use is a good approximation and (1) holds.

Their method is fast thanks to the use of the discretized Laplacian to express smoothness condition at each step of the resolution (it only depends on connectivity so it doesn't change), so that it only needs to be factorized once and can be re-used.

## Curve deformation

Sketched curves can be used as handles by the user. The user drags one vertex of the curve and the position and rotation of local coordinate frames are solved for all other vertices and edges.

{{< figure src="media/fibermesh.png" caption="" width="400px">}}
