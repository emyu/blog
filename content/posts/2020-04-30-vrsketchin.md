---
title: "VRSketchIn"
date: 2020-04-30T09:34:11+02:00
draft: false
tags: ["Paper"]
summary: "Defines the design space for mid-air pen and pen + tablet interactions for sketching, classifies previous work in this space and implements a hybrid mid-air pen + tablet VR sketching app"
link: "https://www.uni-ulm.de/in/mi/mi-forschung/uulm-hci/projects/vrsketchin-exploring-the-design-space-of-pen-and-tablet-interaction-for-3d-sketching-in-virtual-reality/"
---

Defining the design space by crossing interaction devices and desired operations gives a number of possible interaction metaphors to implement in a VR sketching app. They choose to implement a number of those that were not looked at by previous work in VRSketchIn. Their main focus is on combining mid-air pen, mid-air tablet, and pen + tablet interactions. They have drawing surfaces (planar) embedded in the 3D space via positionning the tablet, on which the user can draw indirectly via the tablet. They have a miniaturized view of the whole space that is attached to the tablet. They have primitive extrusions.

The user study consists of 6 participants that sketched freely after being introduced to the interaction metaphors of the system. Then they were interviewed in a structured way. No quantitative data is presented. They state that 3D mid-air sketching remains the prefered interaction metaphor in their system (compared to creating planar drawing surfaces for pen+tablet).
