---
title: "Flow Complex Based Shape Reconstruction From 3D Curves"
date: 2021-02-11T13:35:28+01:00
draft: false
tags: ["Paper"]
summary: "Surfacing of unstructured 3D curves by using topological analysis of the flow complex of the sketch. This method does not require all strokes to form a connected network, but doesn't support fuzzy intersections (they must be snapped)."
link: "http://www.cs.toronto.edu/~sadri/resources/curve-net-reconstruction.pdf"
---

* The flow complex is a set of elements (0​-cells: vertices, 1-cells: edges, 2-cells: surfaces, 3-cells: volumes). The elements are sequentially constructed based on a proximity analysis of the points: for example an edge connects 2 points when the spheres growing around those points touch each other. A surface is created when more than 2 spheres meet at a point. (Illustrations from the paper are good at explaining this concept).
* They analyse the persistence of elements: the distance the spheres have to grow between the creation of an element (eg creating a face that closes off a volume) and its destruction (eg the moment where the spheres from all vertices in the faces meet). This is based on the intuition that collection of strokes that enclose a well-defined volume should act as "prison bars" that are both restrictive (the smallest sphere that could escape from inside the volume is small) and roomy (the biggest sphere that can fit inside the volume is big). So volumes that are the most persistent should be kept.
* They must resample strokes in order to ensure that all stroke edges are "Gabriel", meaning that its diametric circle does not contain any other points. They subdivide all edges of the poly-lines until that is true. Also they make sure that the curves are sampled into polylines thinly enough so that the edges are at most as long as the distance to the closest curve.
* The number of surface patches to keep depends on the topology of the reconstructed object, which can be either indicated by the user or inferred by analysing the sketch connectivity.

