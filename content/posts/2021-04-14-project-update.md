---
title: "Project Update"
date: 2021-04-14T11:03:06+02:00
draft: false
tags: ["Surfacing Project"]
summary: "Progress on using implicit polynomial surface models"
link: ""
---

## State of the project

* This week we focus on the "Segment and Fit" part of the pipeline

{{< figure src="media/2021-04-14/method-overview.png" caption="Overview of the pipeline" width="800px">}}

{{< figure src="media/2021-04-14/segment-and-fit.png" caption="Segment and fit" width="600px">}}

* L2 regularization for implicit polynomial surfaces
* Robust fitting
* Use implicit polynomial surface as a surface primitive in PEARL segmentation
* Tried to use label cost in graph-cut to encourage selection of lower degree polynomials (simpler surface models)

## For next time

* Better initialization:
  * Heuristics for "seed point" placement (eg: at intersections between strokes)
  * Try different heuristics for initial zone flooding (eg: spectral clustering on the graph, to prefer not going across strokes)
  * Propose a lot of models (doing this is recommended to avoid initialization dependency, in previous work)
* Initialization independent testing of different pieces
  * Fix random initialization with seed (make it always the same across tests)
  * Pipeline to average across a large number of initialisations
* PEARL: check that our modifications (proposing new models, splitting subsets into connected components, etc) all respect the rule that every iteration decreases the overall energy. If not, make it conform to that.

## Remarks/ideas

* Adrien: making initialization deterministic and based on a well-chosen heuristic may help
* Rahul: do we still respect the condition of always having a decrease in overall energy in our modified version of PEARL?
* Andreas/Rahul: we could take the polynomial degree selection out of the graph-cut step, like selecting the model before feeding the models to the labelling algorithm

{{< figure src="media/2021-04-14/open-cylinder.png" caption="Cylinder sketch where all points from the proxy are labelled with a unique label, corresponding to this open cylinder." width="300px">}}

* Adrien: it is an acceptable limitation, can be fixed by the user by adding strokes
* Andreas/Emilie: our result shoud respect topology of the proxy
* Andreas: we can detect by analyzing the topology of the graph, whether each subset of points has disk topology or not, maybe we should enforce that to break up such degenerate cases