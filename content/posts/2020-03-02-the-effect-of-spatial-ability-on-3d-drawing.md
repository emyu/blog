---
title: "The Effect of Spatial Ability on Immersive 3d Drawing"
date: 2020-03-02T10:51:25+01:00
draft: false
tags: ["Paper"]
summary: "A user study on whether spatial ability affect how well someone can draw in an immersive (VR) setting and what are some common behaviors in this setting."
link: "https://dl.acm.org/doi/pdf/10.1145/3325480.3325489"
---

This study had users reproduce a target shape by sketching a wireframe model in VR. The target shapes were geometrical shapes that are quite complex but only composed of lines and circles. These shapes are not symmetric and have almost only orthogonal corners.

Some interesting insight:

* Users preferred sketching paralell to the view plane. They will even move around rather than drawing in the perpendicular direction.
* Most users kept their head static while drawing.
* The result of the sketch is better if the participant moves around the sketch than if they are static. It seems to help to have a "rocking movement" (orbiting back and forth in front of a part of the drawing) as it helps planning the next stroke in 3D.
* They found out that users performed better with walking to change the view point rather than using hand-based control.
* They advise to use visual guides inside the virtual environment to provide depth cues to correctly identify hand position in space.

Further reading on this subject: https://www.sciencedirect.com/science/article/pii/S0097849309000855
