---
title: "Vector Heat Method"
date: 2021-01-06T14:24:15+01:00
draft: false
tags: ["Paper"]
summary: "Parallel transport vectors across a mesh with linear solves by computing the diffused vector field from one or multiple source vertices with an initial vector. The diffusion is defined by a connection Laplacian which resembles the cotan Laplacian but additionally describes how vectors are transported between vertices. Applications: logarithmic map, Karcher mean on curved surfaces."
link: "http://www.cs.cmu.edu/~kmcrane/Projects/VectorHeatMethod/index.html"
---

* Parallel transport of a vector along a curved surface is dependent of path (or curve) along which it is transported. It means moving the vector without change along the tangent direction of the curve.
* Link between parallel transport and Levi Civita connection: parallel transporting a vector $Y_{|q}$ along a very short curve of tangent $Z_{|q}$ to a point $p$  ($P_{q \to p}(Y_{|q})$) enables us to compare the vector $Y_{|q}$ with another vector at $p$, so it defines a notion of derivative of a vector field $Y$ along another vector field $Z$. And the Levi Civita connection $\nabla$ is the operator that defines this *covariant* derivative: $\nabla_Z Y_{|p} = \lim_{s \to 0}(P_{q \to p}(Y_{|q}) - Y_{|p}) / s$
* A connection connects nearby tangent space, so it prescribes a way to differentiate tangent vector fields, and it also defines a way to do parrallel transport on a surface.
* It is possible to define a connection Laplacian by taking the trace of the second covariant derivative. And so they write the heat equation using this Laplacian: $\Delta^\nabla X_t = \frac{d}{dt} X_t$
* And it turns out that the diffusion (or "smearing out") described by this heat equation, if taken for a short time $t$, corresponds to the parallel transport of vectors *along shortest geodesics*, with a scalar factor.
* So the algorithm is:
  * Compute and integrate $\Delta^\nabla X_t$ for a short time $t$, normalize the vectors $X_t$
  * Compute the norm of the vectors by scalar diffusion (similar process but with the usual cotan Laplacian)
  * Scale the vectors by the correct norms
* On a triangle mesh:
  * A vector at a vertex is defined in local polar coordinates, by its norm and an angle, defined wrt a reference edge outgoing from the vertex. The angles are all normalized such that the sum of the angles of the 1-ring is $2\pi$.
  * Parallel transporting a vector along an edge is expressed as a change of basis from the basis of one vertex to the basis of the destination vertex. This is encoded as a multiplication by a complex number (a rotation of some angle, in order to have a parallel vector in the new basis).
  * The connection laplacian is a complex matrix with the same structure as the cotan Laplacian except that some elements are multiplied by complex numbers that encode the rotations along the edges (to have parallel transport of vectors across edges)
  * So integrating $\Delta^\nabla X_t$ can be done in a single solve, with a backward Euler step, starting from some initial condition $X_0$ that describes the vector(s) that we wish to transport/extrapolate on the surface.
* It is also possible to do something similar on point clouds, polygonal meshes. Using an intrinsic Delaunay triangulation for a triangle mesh is safer since it guarantees positive cotan weights (=> no flipped vectors).
* Computing the global logarithmic map:
  * The logarithmic map gives a 2D coordinate system on the entire surface relative to a chosen origin point, where the distance in the parameterization from any point to the origin point corresponds to the geodesic distance
  * Log map of a point $x$: for any point $y$ of the surface, the log map (of $x$) gives a vector $v$ in the tangent plane at $x$ and the distance $r$ such that shooting a geodesic from $x$ in the direction $v$ and for a distance $r$ will arrive at $y$.
  * To compute it: parallel transport a reference vector and radial vectors from $x$ to all other vertices, compute the angle between those vectors at each vertex (this gives the angle corresponding to the vector in tangent space at $x$). The geodesic distance is computed by integrating the radial vectors.
* The logarithmic map can be used to:
  * add decals on a surface
  * compute the Karcher mean of many points on the surface: the logarithmic map enables us to compute an update vector, and to get iteratively closer to the correct point.
