---
title: "Handpainter"
date: 2021-06-01T18:33:38+02:00
draft: false
tags: [Paper]
summary: "Using the hand as a canvas for painting on a constrained planar surface in VR"
link: "https://yingjiang96.github.io/handpainter"
---

* Using a tablet as a support to sketch in VR can be tiring for long periods of time (holding a heavy object)
* So they propose to use the non-dominant hand as a support to sketch on. They show this is less tiring.
* Functionalities: pan the 2D canvas on the hand surface to draw bigger things (since hand area is limited), use the hand as a deformer for the 2D canvas by curling the fingers
* User study: comparison with (1) 3D sketching in Tiltbrush and (2) 3d sketching with the addition of a tablet as support. Criteria: accuracy (similar tasks as in Rahul's study) and fatigue

