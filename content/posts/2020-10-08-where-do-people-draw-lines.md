---
title: "Where Do People Draw Lines"
date: 2020-10-08T11:33:01+02:00
draft: false
tags: ["Paper"]
summary: "Study on how artists depict given 3D objects as a 2D sketch. They propose a protocol to collect aligned sketches and use the data to analyze where people draw lines and compare this with NPR algorithm results."
link: "https://gfx.cs.princeton.edu/proj/ld3d/"
---

## Protocol

Two-steps in order to get registered line drawings (aligned to the prompt image) without influencing the artist by having them trace on an image directly:

* The artist draws on a blank paper
* The artist reproduces every individual line sketched previously by tracing over a faint picture, aligning their strokes with the corresponding ones on the image

They provide interesting insights regarding the selection of 3D models to sketch, style of rendering and instructions given to the artists.

## Analysis

* Artists agree on many lines (75% of pixels on a line in a drawing are present on all other drawings)
* They measure precision and recall for line drawings generated using different criteria such as Canny edges, suggestive contours, ridges and valleys. They find that each of these methods are able to explain parts of the human drawn lines.
* A learning approach based on using local object and image space features to predict whether a pixel would be drawn on by a human tells them that the most important criteria to get a correct prediction (by quite a margin) are image space gradients. These lines generally also correspond to a combination of object space criteria such as ridges and valleys.
* It seems like some artist-drawn lines are not explained by any of the local criteria they measured. This choice could depend on context or object semantics.

## Further reading

Other perceptual studies on how people draw or understand shapes:

* [How well do line drawings depict shape?](https://gfx.cs.princeton.edu/proj/ld3d/)
* On expert performance in 3D drawing tasks
* Schelling points on 3D surface meshes
* A benchmark for 3D shape segmentation

This study looked into how humans abstract a 3D shape to a 2D line drawing, I haven't found work on how humans abstract a 3D shape to a 3D line drawing, but there are methods to do so algorithmically:

* Flowrep
* Abstraction of man-made shapes (this is more about abstracting a complex 3D model to a simple 3D model)
* Exoskeleton
