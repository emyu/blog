---
title: "Draw on Mesh"
date: 2020-10-01T15:13:36+02:00
draft: false
tags: ["Paper"]
summary: "How to project freehand 3D stroke onto a 3D mesh surface. The projection happens as the stroke is being drawn (one sample at a time). They introduce a novel technique based on Phong projection."
link: "https://arxiv.org/abs/2009.09029"
---

The paper goes over a wide range of projection techniques:

* Raycasting from the hand in a given direction
* Projecting the hand position to the closest point on the surface
* Closest point on the surface in a smooth way ([Phong projection]({{< ref "posts/2020-03-23-weighted-average-on-surfaces.md" >}}))
* Anchored stroke projection: computing the current point to project as an offset of the previous projected sample. This is based on the observation that users will approximately mimic the shape of the surface while drawing.
* Variations of that technique (encouraging local planarity, taking only into account motion along the local tangent plane of the surface)

The best technique (and most general) is basic anchored stroke projection, using Phong projection to find the closest point on a smooth approximation of the underlying mesh.

The paper evaluates the benefits and issues of this technique over the most commonly used one (raycasting in a fixed direction wrt the hand, like with a spraycan). It features cool stuff for general evaluation of immersive sketching:

* Description of how the target strokes on surfaces were sampled randomly
* Filtering potentially bad user data (due to non-supervised testing)
* Measures of stroke accuracy and aesthetic of an on-surface curve (based on geodesic curvature)
* Measures of physical effort
