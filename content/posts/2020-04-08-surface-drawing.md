---
title: "Surface Drawing"
date: 2020-04-08T15:10:18+02:00
draft: false
tags: ["Paper"]
summary: "TiltBrush precursor. Sketching interface where the motion of the hand directly creates a shape in AR. Some tangible tools are also used to convey other functionalities than sketching such as erasing and moving the sketch."
link: "https://dl.acm.org/doi/10.1145/365024.365114"
---

[Another paper](https://authors.library.caltech.edu/26847/5/CSTR99.pdf) by the same authors that details a bit more the surface creation from the input samples.

