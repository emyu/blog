---
title: "Surface Patches From Unorganized Space Curves"
date: 2020-05-18T10:32:57+02:00
draft: false
tags: ["Paper"]
summary: "Method to automatically find cycles in curve networks such as those from ILoveSketch by greedily selecting cycles to form a cycle basis according to some criteria."
link: "https://www.cs.ucdavis.edu/~amenta/pubs/patches.pdf"
---

They pose the problem of finding all the cycles corresponding to surface patches of a curve network as finding the best-suited cycle basis for the set of all cycles in the graph. They define "best-suited" with a set of heuristic to compute a weight for each cycle, then the best-suited basis is the one with the lowest weight.

The weight of a cycle is a combination of 3 factors:

* Cycle length: short cycles are preferred
* Non-separating cycles are preferred over separating cycles (would removing the cycle cut the graph in multiple pieces?)
* Cycles that are nearly planar, in a plane aligned with axis are preferred. This is measure by taking the volume of an axis-aligned bounding box for each cycle

The algorithm then consists of first enumerating all cycles in the graph, sorting them by weight, then in this order adding them to the selection of cycles, as long as adding a cycle increases the rank of the matrix representing the selection of cycles (binary matrix with cycles as columns and edges as lines). If the rank would not increase, they don't add the cycle and go on.

They also provide a user interface to allow the user to indicate if some patches are mistakenly surfaced. The main interaction is to remove patches in this way, then the next-best patch is suggested by the system. The system also suggests closing patches to the user (those are not part of the cycle basis).
