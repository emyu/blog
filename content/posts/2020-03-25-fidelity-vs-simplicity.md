---
title: "Fidelity vs Simplicity"
date: 2020-03-25T09:25:53+01:00
draft: false
tags: ["Paper"]
summary: "Vectorization of rough sketches by optimizing jointly for fidelity and simplicity of the resulting Bezier curves."
link: "https://www-sop.inria.fr/reves/Basilic/2016/FLB16/"
---

Vectorization of a sketch is more useful if it expresses the sketch with the simplest possible curve network, so that the total number of control points stays reasonable and does not prohibit manual tweaking.

This method first overfits the input sketch with many Bézier curves, with endpoints on each junction, sharp turn and stroke endpoint. From this over-segmentation, they seek to merge edges, create overlaps (and re-fit a curve to the resulting edges) or reduce the degree of the curves in order to optimize an energy composed of one term for simplicity and one term for fidelity.

Fidelity energy for each edge is the sum of the squared errors between pixels of the input bitmap stroke associated with this edge and matching points on the Bézier. The error for each point is weighted based on the thickness of the line: a thicker zone is weighted less because it is more uncertain.

$E_{fidelity} = \sum_e \sum_{p \epsilon S^e}\ (1 - \frac{w_p}{2})\ ||B^e(t_p) - p||^2$

Simplicity energy is the number of edges + the sum of the degrees of each Bézier.

$E_{simplicity} = \sum_e (1 + Deg(B^e))$

The search for the optimal solution is done as a random exploration, by applying small perturbations (merge/split edges, reduce/increase degree, add/remove overlap) and accepting the perturbation if it decreases sufficiently the energy.

Hard constraints to enforce curve connectivity (at endpoints and at midpoints) are applied only to the last optimization, when the optimum hypergraph of edges has already been found, to reduce the cost of this constrained optimization.
