---
title: "Variational Mesh Segmentation via Quadric Fitting"
date: 2020-12-08T10:48:45+01:00
draft: false
tags: ["Paper"]
summary: "Segment an input mesh into parts that are well described by a quadric surface."
link: "http://archive.ymsc.tsinghua.edu.cn/pacm_download/38/283-2012_CAD_quadric.pdf"
---

* Inspired by similar work such as:
  * [Variational Shape Approximation](http://www.geometry.caltech.edu/pubs/CAD04.pdf) (similar method but only supports planar proxies)
  * [Structure Recovery via Hybrid Variational Surface Approximation](https://www.graphics.rwth-aachen.de/media/papers/hybrid1.pdf) (different proxy geometries: sphere, cylinder, rolling-ball patch)
* The goal is to segment the mesh into parts that are well-represented by a single quadric surface
* Fitting a quadric surface to a set of triangles from the mesh is done by minimizing an energy that combines squared distance and the normal deviation (metric described in [VSA](http://www.geometry.caltech.edu/pubs/CAD04.pdf) paper)
  * They compute these energies by taking integrals over triangle faces
  * First initialize the result by minimizing only the squared distance metrics (similar to Taubin 1993)
  * Then use the gradient of $f$ computed with the result from initialization to make the problem linear
* They find the optimal partition of triangles into connected regions that can be fitted by a quadric surface by proceeding as in VSA. They use Lloyd algorithm (K-means clustering):
  * Initialize with random seed triangles / proxies (plane defined by the triangle face)
  * Distortion-minimizing flooding step: using a priority queue that prioritizes assignments of faces to well-fitting proxies, "grow" the assignement regions from each "seed". Stop when all faces are assigned to a region.
  * Fit a quadric to each region, and change the seed to the triangle face with lowest error. Repeat.
* There are also operations to merge 2 neighboring regions by fitting a quadric to the union of those, or to create a new region (choose the triangle with worst error as seed)
* Finally they explain how to smooth out the boundaries between regions to create a nice segmentation

