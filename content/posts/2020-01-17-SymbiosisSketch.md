
---
title: "SymbiosisSketch"
date: 2020-01-17T09:04:40+01:00
draft: false
tags: ["Paper"]
summary: "**Rahul Arora et al., CHI 2018**. SymbiosisSketch's target use is for early concept design, for artists used to 2D sketching and usual illustrating techniques. It helps them sketch as they are used to, but have the result be visualised in 3D and in real life scale, potentially interacting with real life props thanks to augmented reality."
link: "https://dl.acm.org/doi/pdf/10.1145/3173574.3173759"
---




## Method

The user holds a tablet in their non dominant hand and a custom built stylus that is tracked in 3D. They wear Hololens.

The user defines drawing canvases in 3D space that can be curved (open surface) by drawing mid-air with the stylus. Drawing on the 2D canvas of the tablet will then project the strokes onto the 3D canvas. The stroke appears mid-air on the virtual 3D canvas.

A virtual pointer appears on the 3D canvas to help users see where their 2D stroke would land in 3D. A regular grid on both canvases also gives visual cues.

Surface of physical objects scanned from the room can also define drawing canvases. Only planar surfaces are supported due to limitation in Hololens API.

To draw both at a small or large scale comfortably, the workspace in which the user draws mid-air can be scaled to represent arbitrarily small or large volumes of real-life space.

Filled, solid surfaces can also be rendered, to help sketching by providing occlusion. They are computed by Constrained Delaunay triangulation with input strokes of a given closed curve (or joined curves).

## Benefits of the combined workflow

* 2D sketching: artists can depict precise details and textures because they can afford greater precision than in mid-air sketching

* 3D sketching: depth is known

* Interactive surfacing: helps reduce visual clutter
* Augmented reality: artists can use reference objects from the real world
