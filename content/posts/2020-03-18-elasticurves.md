---
title: "Elasticurves"
date: 2020-03-18T11:02:43+01:00
draft: false
tags: ["Paper"]
summary: "Local stroke beautification that happens while the stroke is being created, by commiting the beautified stroke with a short lag compared to the input stroke, depending on drawing speed."
link: "http://www.dgp.toronto.edu/~ythiel/Elasticurves/"
---

Method based on the idea that it would be good to allow users to specify the precision they intend along the stroke while they are sketching it. A simple way to do so is by modulating the sketching speed. It is also coherent with speed-accuracy trade-off: fast => less accurate.

They also hypothesize that other methods of beautification that work on the complete stroke can lead to frustration for the users, as they can't see the final beautified result until they finish the stroke.

They have an almost-immediate beautification of a stroke (with a lag), as if the beautified stroke was dragged behind the pen with a zero-rest-length spring. They do that by defining connectors between the beautified "inked" result and the next input sample.

{{< figure src="media/elasticurves.png" caption="" width="400px">}}

The input samples (green points in figure) are sampled at constant time intervals. In this way, slower movement => more samples in a zone => beautification follows input more precisely.

## Evaluation

* Measuring actual sketching speed compared to intention to show that people could control sketching speed quite well (sketch a stroke as fast as they can, then slow down)
* Qualitative feedback and sketches from 6 users
* Comparison with 3 other beautification techniques, while tracing over the same target shape
