---
title: "Color by Numbers"
date: 2021-04-08T13:48:43+02:00
draft: false
tags: ["Paper"]
summary: "Interactive vectorization method inspired by work on user-guided colorization of sketches. The idea is to leverage a Delaunay triangulation of the sketch as a data structure to diffuse color based on user input. The colorization is used to extract zone boundaries, which correspond to the vectorized lines."
link: "https://hal.inria.fr/hal-03116986/document"
---
