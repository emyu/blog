---
title: "Sketchnote Components"
date: 2021-06-01T18:34:49+02:00
draft: false
tags: [Paper]
summary: "Analysis of a large corpus of sketch notes, complemented with semi structured interviews of experienced sketch noter. They list component that need to be taken into consideration to make a sketchnote, map out the design space of sketchnotes and showcase strategies adopted by sketch noters to overcome their challenges. This leads to implications for future digital tools to help in sketch note creation."
link: "https://visualinteractivedata.github.io/papers/zheng2021sketchnote.pdf"
---

* They start by analyzing a large corpus of sketchnotes found on Twitter by searching for a hashtag. It seems like they have asked for every artist to give copyright to put the sketch note in the paper. The artists are cited by name.
* Then they interview sketch noters (that have made one or multiple sketchnotes found in the corpus), to understand decisions/strategies. The participants are not explicitly linked to their sketchnotes, so they can stay anonymous
* Questions for the semi-structured interview are not provided