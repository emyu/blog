---
title: "A general and efficient method for finding cycles in 3D curve networks"
date: 2020-03-02T10:27:27+01:00
draft: false
tags: ["Paper"]
summary: "A method to find cycles from a clean 3D curve networks on both manifold and non-manifold geometry with arbitrary genus in a few seconds."
link: "https://www.cse.wustl.edu/~taoju/zoum/projects/CycleDisc/"
---

This method relies on reformulating the problem of finding all the cycles in a 3D curve networks as searching for local mappings at a vertex or a curve that describe how the incident curves are grouped into cycles.

These local mappings are relations between darts, where each curve is associated with k darts at each of its endpoint (k being the capacity of the curve):

* Corner mapping: maps a dart at a vertex to another dart at the same vertex
* Bridge mapping: maps a dart at one end of a curve to one of the darts at the other end of the same curve

Finding the best cycles is equivalent to finding the best set of such mappings.

## Cost metrics

The paper introduces local cost metrics to measure the cost of a bridge mapping for 3 curves {a, b, b}. These metrics are based on angles between parallel-transported families of normals at the joining vertices.

{{< figure src="media/cycles-cost-metric.png" caption="" width="400px">}}

## Finding the solution

They represent the problem as a graph where each node is a possible corner mapping at a vertex and the edges link together vertices that are connected by a curve. The weight of the edge is the minimal cost for bridging the 2 corner mappings. They use dynamic programming to search for a minimum-weight subgraph that covers all vertices. To reduce complexity they limit the number of considered corner mappings by pre-computing their potential to form low-cost bridges and keeping only the best few. Other strategies are employed to the same effect.

They observed that the error cases of their algorithm are often non-simple cycles (this is a consequence of the cost metric chosen). So they propose a method to break non-simple cycles into several simple cycles.
