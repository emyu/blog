---
title: "Free Form Skinning of 3d Curve Clouds"
date: 2020-10-16T11:10:24+02:00
draft: false
tags: ["Paper"]
summary: "Skinning an unstructured 3D curve cloud. Their method uses a combination of Guidance Vector Field and smoothness prior to deform an enclosing sphere to shrink wrap smoothly the curves."
link: ""
---

* Generate Guidance Vector Field on a discretized domain of the 3D space, such that the vector field is smooth and points towards the curves when close to them
* Iteratively deform a mesh that encloses the whole sketch with the "forces" corresponding to the guidance vector field and smoothing operations
