---
title: "Project Math Equations"
date: 2021-04-20T10:55:00+02:00
draft: false
tags: ["Surfacing Project"]
summary: "A place to put all equations in latex format, so that they can be easily reused in the future."
link: ""
markup: "mmark"
---

## PEARL segment and fit

### Total energy


We seek to find the labelling $$ l : v \rightarrow l(v) = i \ ,\ i \in [\![1, m]\!]$$ and the associated surface models $$ \{f_i\}_{i \in [\![1, m]\!]} $$, which are implicit polynomial surfaces, that minimize the total energy:

$$ E(l, f_1, ..., f_m) = w_{\text{fidelity}} * E_{\text{fidelity}}(l, f_1, ..., f_m) + w_{\text{smooth}} * E_{\text{smooth}}(l) + w_{\text{label}} * E_{\text{label}}(l) $$



$$ \text{argmin}_l\ E(l) = w_{\text{fidelity}} * E_{\text{fidelity}}(l) + w_{\text{smooth}} * E_{\text{smooth}}(l) + w_{\text{label}} * E_{\text{label}}(l) $$

The weights $$ w $$ can be used to balance those term relative to each other, improving one of those objectives at the expanse of the others.

### Fidelity energy

$$V$$ is the set of proxy vertices, $$l(v) = i$$ is the label of a vertex v, $$Z(f_i)$$ is the zero-level set of the implicit surface corresponding to label $$l_i$$. We define the fidelity energy as:

$$E_{\text{fidelity}} = \frac{1}{N_p * \delta^2} \sum_{v \in V} \text{dist}(v, Z(f_{l(v)}))^2$$

In the normalization term, $$N_p$$ is the total number of data points (or edge crossings) taken into account:

$$N_p = \sum_{v \in V} \sum_{e \in E_v} 1$$

and $$\delta$$ is a distance constant fixed heuristically by sketch, corresponding to some "small distance" wrt the scale of the sketch. Setting this distance could depend on both the scale of the overall sketch, but also the "sketchiness" we expect to find in the sketch, or how much imprecision we expect to have.

For a vertex $$v$$ we compute $$\text{dist}(v, Z(l(v)))$$, as the sum of the squared distance of all its associated edge crossings (ie: the places where the projected strokes cross an edge in the 1-star of the vertex):

$$\text{dist}(v, Z(f_{l(v)}))^2 = \sum_{e \in E_v} \text{dist}(p_e, Z(f_{l(v)}))^2$$

Note: taking the sum and not the mean is important because taking the mean leads to potential oscillating behavior due to minimizing a differently weighted sum while labelling than fitting.

Computing the exact distance could be done with a projection procedure, to find the closest point on the surface. However that is an iterative procedure, so we can instead use an approximation of the distance between a point and the zero-level set of an implicit surface $$Z$$. We call $$f$$ the function that defines the implicit surface.

We can consider the algebraic distance instead of the geometric distance:

$$\text{dist}(p, Z(f))^2 \approx f(p)^2$$

or the first-order approximation [An improved algorithm for algebraic curve and surface fitting, Taubin, 1993]:

$$\text{dist}(p, Z(f))^2 \approx \frac{f(p)^2}{||\nabla f(p)||^2}$$

When fitting models to data points $$p_e$$ associated with vertices in a subset $$V_i$$ we minimize the energy corresponding to the algebraic distance approximation, plus a $$L^2$$ regularization term on the coefficients of the polynomial function $$f$$:

$$\min_{f}\sum_{v \in V_i} \sum_{e \in E_v}\sum_{\delta \in \{\delta^-, \delta^0, \delta^+\}} f(p_e + \delta)^2 + \lambda ||W_f||^2$$

The issue is that the algebraic distance is not a good approximation of the Euclidian distance between a point and its closest poinr on the surface. Mostly, the scale of the algebraic distance is arbitrary, but there are also disparities that can't be solved by a proportional scaling. Using the first-order approximation is a lot better to measure the actual fitting error.

I think it'd be better to use the first-order approximation to compute the distances between a point and a model in the alpha-expansion step, since this will lead to a better choice of models. ~~However, since we won't be minimising the same quantity when re-fitting the models (the algebraic distance is used), we cannot guarantee energy decrease anymore. In practice we seem to observe small increases of energy caused by the re-fitting, which in the worse case can lead to some alternating states that don't converge.~~

EDIT: It seems like the alternating states issue was due to another issue in the way fidelity energy was computed (taking the mean among the edge crossings instead of the sum). After this fix, alternating solution is not observed anymore. We do still have small increases in fidelity energy following the fitting operation, due to the fact that the energy we minimize during fitting is slightly different (with the 3L method, we add points offset from the surface to improve stability). Preventing those energy increases can be simply done by rejecting the proposed re-fitted models, if those lead to an energy increase.

### Smoothness energy

The smoothness energy depends on the labelling $$l$$ and on the edge weights of the graph: $$w_{pq},\ \forall \ \{p,\ q \} \ \in \mathcal{N}$$

It is independent of the models.

$$E_{\text{smooth}} = \frac{1}{\sum_{\{p,\ q\} \in \mathcal{N}}w_{pq}} \sum_{\{p,\ q\} \ \in \mathcal{N}} w_{pq} * \delta(l(p), l(q))$$

with

$$\delta(l(p), l(q)) =\begin{cases}0 & \text{if}& l(p)=l(q) \\\\ 1 & \text{if} & l(p) \ne l(q)\end{cases}$$

and the normalization term $$N_e$$ is the total number of edges in the graph.

### Label energy

The label energy depends on the labelling $$l$$, and on the cost associated with each model/label:

$$E_{\text{label}} = \frac{1}{\sum_{f_i \in F^0}c(f_i)} \sum_{i \ \in [\![1, m]\!]} c(f_i) $$

with $$\{ i \ \in [\![1, m]\!] \}$$ the set of labels available with labelling $$l$$, and $$c \ : f \rightarrow c(f)$$ is the cost function associated with a given surface model $f$. We define this cost function to describe the degrees of liberty that different degrees of polynomial implicit surfaces have:

$$c(f) =\begin{cases}1 & \text{if}& \text{deg}(f) = 1 \\ 2.5 & \text{if} & \text{deg}(f) = 2  \\ 4.75 & \text{if} & \text{deg}(f) = 3\end{cases}$$

the values come from the ratios of degrees of freedom between those polynomial functions: a degree $$ 1 $$ polynomial has $$4$$ degrees of freedom, a degree $$2$$ polynomial has $$10 = 4 * 2.5$$ degrees of freedom and a degree $$3$$ polynomial has $$19 = 4 * 4.75$$ degrees of freedom.



## Final mesh optimization

We optimize the proxy vertices $$v \in V$$ to minimize the total energy:

$$ E = E_{\text{fit}} + E_{\text{mesh}} + E_{\text{boundaries}} + E_{\text{align}}$$

Where

* $$E_{\text{fit}}$$ encourages the mesh to match the geometry of the implicit surface patches corresponding to the local labelling
* $$E_{\text{mesh}}$$ encourages the mesh to stay piecewise smooth in every surface patch
* $$E_{\text{boundaries}}$$ encourages each boundary polyline to stay smooth

### Energies

For the fitting energy, we consider an augmented labelling $$\hat{l}$$ that can assign up to 3 labels to a vertex (and assigns minimum 1 label)

$$\hat{l} : v \rightarrow \begin{cases} & [i] & \text{,} \\ & [i, & j] \text{,} \\ & [i, & j, & k]\end{cases}$$

and we consider all the surface models $$\{ f_i \}$$

$$E_{\text{fit}}(V) = \sum_{v \in V}\sum_{i \in \hat{l}(v)} \text{dist}(v, Z(f_i))^2$$

We can approximate the distance between a point and the zero-level set of an implicit surface by using the same first-order approximation as before:

$$\text{dist}(p, Z(f))^2 \approx \frac{f(p)^2}{||\nabla f(p)||^2}$$ 

With the mesh energy we want to encourage smoothness of the triangle mesh (with the exception of vertices along the boundaries between patches where we wish to recover sharp features), so we minimize the squared-norm of the Laplacian of the vertex positions:

$$E_{\text{mesh}} = \sum_{v \in V_{\text{smooth}} } ||L(v)||^2$$

And for the boundary energy, we want to encourage the smoothness of the boundary polyline, so we minimize the squared-norm of the Laplacian on the vertex positions at the polyline:

$$E_{\text{boundaries}} = \sum_{v \in V_{\text{boundaries}} } ||L(v)||^2 $$

Energy to attract seam vertices to corresponding strokes:

$$E_{\text{align}} = \sum_{(v_s,\ p_s) \in S} || v_s - p_s ||^2$$



### Proxy mesh pre-processing

Before doing the full mesh optimization, we need to process the proxy mesh to extract the boundary polylines between each pair of neighboring surface patches. We are given as input a labelling $$l$$ that defines the segmentation of all proxy vertices, and the initial proxy mesh. We output an updated proxy mesh where we have inserted vertices and edges in the mesh so that the patches have explicit boundaries on the mesh topology (defined as vertex paths).

First we use the labelling of all vertices to detect:

* $$E_{b}$$ the set of edges of the proxy mesh that have 2 differently labelled endpoint vertices
* $$F_b$$ the set of faces of the proxy mesh that have 3 differently labelled vertices

Then we insert new vertices $$V_b$$, in order:

1. Insert one vertex at the center of every face $$f \in F_b$$ (and the 3 appropriate edges)
2. Insert one vertex at the midpoint of every edge $$e \in E_b$$ by splitting it (and adding the 2 approrpiate edges)

We assign multiple labels to each newly inserted vertex: 2 labels for an edge split vertex (labels of the endpoint vertices), and 3 labels for a face split vertex. This yields the augmented labelling $$\hat{l}$$.

In practice, we keep track of the subsets of vertices instead of the labels, and so we store for each label a list of all the vertex indices that have this label assigned. This enables us to seamlessly encode 1 or multiple labels per vertex, and is more practical for the following optimization.

Next we apply a pass of min-angle maximizing edge-flipping, with the constraint that edges between vertices of $$V_b$$ must not be flipped. (TODO) This should improve mesh quality which is important for further mesh optimization (esp. if we use the cotangent Laplacian).

Finally we obtain our updated proxy mesh, with explicit boundaries between differently labelled zones of vertices.

### Initialization

We can initialize from the proxy mesh vertices position. We can also use a better initialization by first optimizing the proxy mesh to minimize the least squares distances between the projected stroke points (lying on faces of the proxy) and the 3D stroke points. This can be solved with a linear system (least-squares + smoothing with position Laplacian squared norm).

We weigh smoothness term differently for boundary vertices depending on whether the patches are locally well-aligned or not. We measure local patch alignment by projecting each proxy boundary vertex onto the corresponding 2 surface patches (onto the intersection or near-intersection) and compute the dot product between the patch normals at this point:

With $$p_{i,j} = \pi_{i,j}(v_{i,j})$$

$$w(v_{i,j}) = \frac{\nabla f_i(p_{i,j})}{||\nabla f_i(p_{i,j})||} \cdot \frac{\nabla f_j(p_{i,j})}{||\nabla f_j(p_{i,j})||}$$

where we compute $$\pi_{i,j}(v_{i,j}) = p_{i,j}$$ for all boundary points $$V$$ by solving the optimisation problem:

$$\pi(V) = P^* = \text{argmin}_{P} \sum_{v \in V}\sum_{i \in \hat{l}(v)} \text{dist}(v, Z(f_i))^2 + w_{\text{smooth}}\sum_{v \in V } ||L(v)||^2$$

### Solving the optimization





## Implicit polynomial cubic surface derivatives

An implicit cubic polynomial surface is defined by the function:

$$f(p) = X(p)W$$

where $$X$$ is a row vector cubic polynomial function of the point coordinates:

$$X(p) = \begin{bmatrix} 1 & x & y & z & x^2 & xy & xz & y^2 & yz & z^2 & x^3 & y^3 & z^3 & xy^2 & xz^2 & yx^2 & yz^2 & zx^2 & zy^2 & xyz\end{bmatrix}$$

$$X(p) = \begin{bmatrix} 1 & x & y & z & x^2 & \dots & x^3 & y^3 & z^3\end{bmatrix}$$

and $$W$$ is a column vector holding the weights that define the polynomial.

We can compute analytically the derivatices of the function $$X$$ with respect to the point coordinates.

$$\nabla X(p) = \frac{d}{dp}X(p) = \begin{bmatrix} 0 & 1 & 0 & 0 & 2x & y & z & 0 & 0 & 0 & 3x^2 & 0 & 0 & y^2 & z^2 & 2xy & 0 & 2xz & 0 & yz \\ 0 & 0 & 1 & 0 & 0 & x & 0 & 2y & z & 0 & 0 & 3y^2 & 0 & 2xy & 0 & x^2 & z^2 & 0 & 2yz & xz \\ 0 & 0 & 0 & 1 & 0 & 0 & x & 0 & y & 2z & 0 & 0 & 3z^2 & 0 & 2xz & 0 & 2yz & x^2 & y^2 & xy\end{bmatrix}$$

And with that the derivative of the function $$f$$ is:

$$\nabla f (p) = \nabla X(p) W$$

If we take the derivative of the matrix $$\nabla X$$ with respect to the point $$p$$ we get a tensor:

$$\frac{d}{dp}(\nabla X)(p) = \begin{bmatrix} \frac{d}{dx}(\nabla X(p)) \\ \frac{d}{dy}(\nabla X(p)) \\ \frac{d}{dz}(\nabla X(p)) \end{bmatrix}$$

where every block is defined by:

$$\frac{d}{dx}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 6x & 0 & 0 & 0 & 0 & 2y & 0 & 2z & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2y & 0 & 2x & 0 & 0 & 0 & z \\ 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 0 & 2x & 0 & y \end{bmatrix}$$

$$\frac{d}{dy}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2y & 0 & 2x & 0 & 0 & 0 & z \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 6y & 0 & 2x & 0 & 0 & 0 & 0 & 2z & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 2y & x \end{bmatrix}$$

$$\frac{d}{dz}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 0 & 2x & 0 & y \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 2y & x \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 6z & 0 & 2x & 0 & 2y & 0 & 0 & 0 \end{bmatrix}$$

With that we can compute the first-order distance approximation:

$$\text{dist}(p, Z(f))^2 \approx \frac{f(p)^2}{||\nabla f(p)||^2}$$ 

And its derivative:

$$\frac{d}{dp}(\frac{f(p)^2)}{||\nabla f(p)||^2}) = 2 \frac{f(p) \nabla f(p) * ||\nabla f(p)||^2 - f(p)^2 * J(\nabla f)(p)^T* \nabla f(p)}{||\nabla f(p)||^4}$$

with $$J(\nabla f)$$ the Jacobian matrix of the gradient of the polynomial function, which is a $$(3,3)$$ matrix:

$$J(\nabla f)(p) = \frac{d}{dp}(\nabla X)(p) W$$

To deal with multiple points at once, we stack the matrices and vectors for each point into a single one. In practice, we avoid explicitly constructing the order 3 tensor $$\frac{d}{dp}(\nabla X)$$ and directly build the matrix $$J(\nabla f)$$ as a sparse matrix (in the case of $$N$$ points this is a $$(3N, 3N)$$ matrix).



## Degree 4

We define the polynomial vector X as a 1xM row vector:

$$X(p) = \begin{bmatrix} 1 & x & y & z & x^2 & xy & xz & y^2 & yz & z^2 & x^3 & y^3 & z^3 & xy^2 & xz^2 & yx^2 & yz^2 & zx^2 & zy^2 & xyz & x^4 & y^4 & z^4 & x^3y & x^3z & y^3x & y^3z & z^3x & z^3y & x^2y^2 & x^2yz & x^2z^2 & y^2z^2 & y^2xz & z^2xy \end{bmatrix}$$

We can compute analytically the derivatices of the function $$X$$ with respect to the point coordinates.

$$\nabla X(p) = \frac{d}{dp}X(p) = \begin{bmatrix} 0 & 1 & 0 & 0 & 2x & y & z & 0 & 0 & 0 & 3x^2 & 0 & 0 & y^2 & z^2 & 2xy & 0 & 2xz & 0 & yz & 4x^3 & 0 & 0 & 3x^2y & 3x^2z & y^3 & 0 & z^3 & 0 & 2xy^2 & 2xyz& 2xz^2& 0 & y^2z & z^2y \\ 0 & 0 & 1 & 0 & 0 & x & 0 & 2y & z & 0 & 0 & 3y^2 & 0 & 2xy & 0 & x^2 & z^2 & 0 & 2yz & xz & 0 & 4y^3 & 0 & x^3 & 0 & 3y^2x & 3y^2z & 0 & z^3 & 2yx^2 & x^2z & 0 & 2yz^2 &2xyz & z^2x \\ 0 & 0 & 0 & 1 & 0 & 0 & x & 0 & y & 2z & 0 & 0 & 3z^2 & 0 & 2xz & 0 & 2yz & x^2 & y^2 & xy & 0 & 0 & 4z^3 & 0 & x^3 & 0 & y^3 & 3z^2x & 3z^2y & 0 & x^2y & 2zx^2 & 2zy^2 & y^2x & 2zxy\end{bmatrix}$$

And with that the derivative of the function $$f$$ is (with W a Mx1 column vector):

$$\nabla f (p) = \nabla X(p) W$$

If we take the derivative of the matrix $$\nabla X$$ with respect to the point $$p$$ we get a tensor (3 x 3 x M):

$$\frac{d}{dp}(\nabla X)(p) = \begin{bmatrix} \frac{d}{dx}(\nabla X(p)) \\ \frac{d}{dy}(\nabla X(p)) \\ \frac{d}{dz}(\nabla X(p)) \end{bmatrix}$$

where every block is defined by:

$$\frac{d}{dx}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 6x & 0 & 0 & 0 & 0 & 2y & 0 & 2z & 0 & 0 & 12x^2 & 0 & 0 & 6xy & 6xz & 0 & 0 & 0 & 0 & 2y^2 & 2yz & 2z^2 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2y & 0 & 2x & 0 & 0 & 0 & z & 0 & 0 & 0 & 3x^2 & 0 & 3y^2 & 0 & 0 & 0 & 4xy & 2xz & 0 & 0 & 2yz & z^2 \\ 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 0 & 2x & 0 & y & 0 & 0 & 0 & 0 & 3x^2 & 0 & 0 & 3z^2 & 0 & 0 & 2xy & 4zx & 0 & y^2 & 2zy \end{bmatrix}$$

$$\frac{d}{dy}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2y & 0 & 2x & 0 & 0 & 0 & z & 0 & 0 & 0 & 3x^2 & 0 & 3y^2 & 0 & 0 & 0 & 4xy & 2xz & 0 & 0 & 2yz & z^2 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 6y & 0 & 2x & 0 & 0 & 0 & 0 & 2z & 0 & 0 & 12y^2 & 0 & 0 & 0 & 6xy & 6yz & 0 & 0 & 2x^2 & 0 & 0 & 2z^2 & 2xz & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 2y & x& 0 & 0 & 0 & 0 & 0 & 0 & 3y^2 & 0 & 3z^2 & 0 & x^2 & 0 & 4yz & 2xy & 2xz \end{bmatrix}$$

$$\frac{d}{dz}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 0 & 2x & 0 & y & 0 & 0 & 0 & 0 & 3x^2 & 0 & 0 & 3z^2 & 0 & 0 & 2xy & 4xz & 0 & y^2 & 2yz \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2z & 0 & 2y & x & 0 & 0 & 0 & 0 & 0 & 0 & 3y^2 & 0 & 3z^2 & 0 & x^2 & 0 & 4yz & 2xy & 2xz \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 6z & 0 & 2x & 0 & 2y & 0 & 0 & 0 & 0 & 0 & 12z^2& 0 & 0 & 0 & 0 & 6xz & 6yz & 0 & 0 & 2x^2 & 2y^2 & 0 & 2xy \end{bmatrix}$$

$$\frac{d}{dx}(\nabla X(p)) = \begin{bmatrix} \frac{d^2}{dx^2}X(p) \\ \frac{d^2}{dxdy}X(p) \\ \frac{d^2}{dxdz}X(p) \end{bmatrix}$$



Taking the derivative of that wrt the vector p gives a 4th order tensor (3x3x3xM)

$$\frac{d^2}{dp dp^T}(\nabla X)(p) = \begin{bmatrix} \begin{bmatrix} \frac{d^2}{dx^2}(\nabla X(p)) \\ \frac{d^2}{dxdy}(\nabla X(p)) \\ \frac{d^2}{dxdz}(\nabla X(p)) \end{bmatrix}  \\ \begin{bmatrix} \frac{d^2}{dydx}(\nabla X(p)) \\ \frac{d^2}{dy^2}(\nabla X(p)) \\ \frac{d^2}{dydz}(\nabla X(p)) \end{bmatrix} \\ \begin{bmatrix} \frac{d^2}{dzdx}(\nabla X(p)) \\ \frac{d^2}{dzdy}(\nabla X(p)) \\ \frac{d^2}{dz^2}(\nabla X(p)) \end{bmatrix} \end{bmatrix}$$

Where some of the matrices are the same, leaving only 6 different matrices (3xM) to compute

$$\frac{d^2}{dx^2}(\nabla X(p)) = \begin{bmatrix} 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 6 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 24x & 0 & 0 & 6y & 6z & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 6x & 0 & 0 & 0 & 0 & 0 & 4y & 2z & 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 0 & 2 & 0 & 0 & 0 & 0 & 0 & 0 & 6x & 0 & 0 & 0 & 0 & 0 & 2y & 4z & 0 & 0 & 0 \end{bmatrix}$$

