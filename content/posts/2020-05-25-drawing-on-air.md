---
title: "Drawing on Air"
date: 2020-05-25T10:52:27+02:00
draft: false
tags: ["Paper"]
summary: "Controlled 3D drawing method inspired by tape drawing and using force feedback via a Phantom device."
link: "http://vis.cs.brown.edu/docs/pdf/Keefe-2007-DOA.pdf"
---

This paper presents 2 methods for controlled 3D drawing:

* 1 handed drag: the brush is dragged behind the dominant hand as if attached by a rod (the hand controls the direction towards which the curve will be drawn)
* 2 handed tape drawing: as in traditional tape drawing, the dominant hand holds the brush and the non dominant hand controls the tangent direction of the curve at the point that is being drawn

The second method relies on the haptic force provided by the Phantom device to constrain the brush holding hand to move effectively towards the non dominant hand. Both methods allow for erasing a part of the curve and redrawing it, guided by haptic feedback.

They use both ribbon and tube brush geometry, stating that ribbons are good at inferring a larger covered surface, while tubes suggest that they are the form themselves.

They have drawing layers and observe that often at least one layer is used to draw rough guidelines to figure out proportions. They also mention how picking the right lines to draw is difficult in order for the drawing to look good from all viewpoints.

## User study

They compare their 2 drawing conditions with 2 control conditions:

* Some default friction and viscosity force is applied to the input device
* Freehand

The task is to trace on some curves, the curves for this task are inspired by anatomical drawings. They justify how tracing is an appropriate task for the evaluation, saying that they are targeting a very precise and deliberate style of illustrations, not like sketching. Then they have some measures to quantify the errors in position and direction.
