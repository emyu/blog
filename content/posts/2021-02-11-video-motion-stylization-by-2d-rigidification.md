---
title: "Video Motion Stylization by 2D Rigidification"
date: 2021-02-11T14:26:10+01:00
draft: false
tags: ["Paper"]
summary: "Using PEARL algorithm to segment semi-automatically each frame from a video, in order to rigidify motions to make it look more like traditional animation with moving pieces of paper."
link: "http://www-sop.inria.fr/reves/Basilic/2019/DBH19/video_stylization_delanoy.pdf"
---

* Every frame in the video is segmented in zones of pixels whose motions across multiple frames can be well-approximated by a single similarity-transform
* A GMM over color space is also used to group similar pixels under the same model
* PEARL algorithm is used to solve the labelling problem (they use super-pixels instead of individual pixels to speed up things)
* Manual labelling of some pixels (scribbles) is used to fix the number of models/labels and to initialize those models. The scribbled pixels are encouraged by an energy term to be always assigned to their giben label (this term replaces the usual "fitting" term).