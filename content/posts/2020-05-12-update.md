---
title: "Update: node normals and sorted segments"
date: 2020-05-12T10:48:03+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Trying to find cycles by walking the graph based on ordered segments at each node (sorted by angle around the node normal)"
link: ""
---

{{< figure src="media/05-12/adding-to-network.gif" caption="When a segment is added to a node, the normal is recomputed and the segment is inserted at the right order in the list of segments (sorted by angle around the normal)." width="">}}

The normal is computed as the normal to the best fit plane to the tangent endpoints. If there are only colinear tangents at a node, no normal is computed. When the normal is recomputed due to addition or removal of a node, we must prevent a complete flip (otherwise the order of the sorted vectors become reversed). To do that we may reverse the sign of the new normal. In the current implementation, a normal that changes significantly without flipping could pose a problem, by making the previously added segments wrongly sorted.

To sort the segments at a node, they are projected on the best fit plane and sorted by angle (one of the vector is chosen as origin, such that the angles of other vectors are relative to this one).

{{< figure src="media/05-12/sorted_curves.jpg" caption="" width="">}}

{{< figure src="media/05-12/sorted_curves_inconstitency2.jpg" caption="The orientation of the normal is arbitrary, so it is often inconsistent between nodes in the same graph." width="">}}

{{< figure src="media/05-12/parallel_transported_normals.PNG" caption="Parallel transporting normals along each segment." width="">}}

{{< figure src="media/05-12/parallel_transport_figure.jpg" caption="Maybe the orientation of the parallel transported normals (dashed arrows) compared to the node normals (line arrows) can be used to disambiguate orientation?" width="">}}

{{< figure src="media/05-12/non-smooth-net.PNG" caption="In the case of a non smooth object, the normals at nodes are not very reliable." width="">}}
