---
title: "SecondSkin"
date: 2020-03-13T11:51:31+01:00
draft: false
tags: ["Paper"]
summary: "Method for sketch-based modeling of layered 3D models such as cloth or armor."
link: "http://www.dgp.toronto.edu/~depaolic/projects/secondSkin/"
---

## Curve classification

They infer the position of the 2D strokes along the viewing rays by first finding out the class of the curve. They argue that the kind of curves that artists draw to represent a layered geometry relative to some 3D base can be classified into 4 distinct classes.

{{< figure src="media/secondskin-types.png" caption="" width="600px">}}

To classify an input curve, they use simple tests such as looking at whether the sketched 2D curve lies partly or completely on an existing surface. They also attempt to match it with nearby contour or feature curves on the underlying geometry.

{{< figure src="media/secondskin-classification.png" caption="" width="400px">}}

To distinguish between lines and curves they use the average of the Menger curvature at each stroke point.

## Position the curve in 3D

When they find out in which class the curve belongs, they use specialized rules that disambiguate the depth of some or all points relative to the underlying geometry (base or underlying layer). They also rely heavily on the fact that the curve probably lies in a minimum skew viewplane, ie a plane that is best aligned with the viewplane. They use proximity snapping on the endpoints such that they will snap to pre-existing strokes if they are close. This will help disambiguate the stroke as well (for the shell projections and tangent plane curves it will give the height, ie how far they lie away from the underlying geometry).

To project a curve onto an offset surface (in the case of shell projection curves), they approximate an inflated surface with the right offset using a vertex shader. They don't specify but I guess that they find the intersections between the view-rays going through the sketched points and this inflated surface.

They allow the user to toggle between the default interpretation and another by pressing a key. They realize that their system is unable to deal with some ambiguities but propose an easy way to remedy to this.

Also after interpreting the 3D positions, they evaluate the quality of the curve by measuring its max variation in view depth and its foreshortening. If the curve is of low quality they give up their initial interpretation and replace it by a planar curve (either the minimum skew viewplane coherent with some endpoint positions, or the least foreshortened XYZ plane). They also allow the user to default to this via pressing a key.

## Curves and surfaces

They represent their curve network as a graph, and when they detect close loops of curves they create a surface. The surfaces are 3 and 4 sided Coons patches. They use a heuristic to define always 3 or 4 sides to patches even for loops with more than 4 curves.

They also provide a simple tool to extrude surfaces into volumes.

## Evaluation

They prove that their classification of curves into 4 types is coherent with what human perceive by asking 15 participants to annotate curves and compare the results between people and with the algorithm. They prove that people are consistent (so their 4 types of curves make sense) and that their algorithm has a good success rate compared to people.

Their user study is pretty informal, 6 users tested the interface and they only show some visual results of what the users achieved to do. However they provide some detailed usage statistics for one user: they give the count of curves that were misclassified (the user has a special erase button to indicate that he is deleting because of classification error), the count of cases were the user used the controls to change the default behaviour. We can see that their method infers correctly in a large majority of cases.

The authors state clearly in which (rare) cases their method fails and show examples. They propose interactions for the user to remedy to those failures.
