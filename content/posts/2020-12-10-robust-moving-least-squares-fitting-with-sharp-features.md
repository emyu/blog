---
title: "Robust Moving Least Squares Fitting With Sharp Features"
date: 2020-12-10T14:46:51+01:00
draft: false
tags: ["Paper"]
summary: "Inspired by robust statistics methods, they propose a variant of moving least-squares that can be robust to noise while preserving sharp features."
link: "http://www.sci.utah.edu/~shachar/Publications/rmls.pdf"
---

* Moving least-squares is a method to define a smooth surface from a potentially noisy or redundant set of points. Concretely it consists of a projection procedure. To project a point it locally estimates (1) a reference plane, (2) the smooth surface by fitting a polynomial function wrt the reference plane to a small neighborhood of points (with a weight function depending on the distance to the projection of the point to the reference plane), and projects the point onto that polynomial surface. [Point Set Surfaces, Alexa et al., 2003]
* They propose an alternative to the polynomial fitting step, where instead of assuming that the neighborhood of points all represent one smooth surface, they can potentially represent multiple smooth surfaces, or be outliers. This leads them to consider a statistically robust fitting approach.
* Initialization: they fit a first model by least median of squares (LMS), by iteratively fitting (by least squares) a model to a small number of points. To limit the number of iterations, in practice they fit a model for a neighborhood of points around each of the points in the set. They select the model that has the minimum median error (in practice they don't take the median but the k-th residual from sorted residuals, since they expect that there may be a lot of outliers)
* Forward search: from this initial model, they iteratively add a few samples (taken from the immediate neighbours of the points already in the fitted set), re-fit the model and monitor the evolution of the residuals. This stops when the largest residual from the fitted set is above a threshold (this means that the model is starting to fit outliers)
* In this way they may fit multiple models to a point and its neighborhood in regions that are not smooth, and then they can project the point as in MLS, except they consider all potential smooth surfaces fitted at this point.
