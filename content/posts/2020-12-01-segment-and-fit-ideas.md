---
title: "Segment and Fit Ideas"
date: 2020-12-01T10:46:43+01:00
draft: false
tags: ["Surfacing Project"]
summary: "Trying to structure our first ideas regarding the segment strokes / fit surfaces algorithm."
link: ""
---

## Input data

* Stroke polylines: treated as individual segments that carry local stroke information
  * Position
  * Tangent
  * Creation time
  * View orientation?
* Scribble polylines: same info as strokes. Maybe also normal information? User draws small patches as scribbles

## Segment/Fit iterations

* Given an initial set of surface models / labels
* Find labelling for each segment that minimizes energy with 2 terms:
  * Data term: closest distance to surface from segment
  * Smoothness term: similar segment (wrt position, tangent, creation time, etc) have same label
* Re-fit surface models to the points assigned to it
* Continue until labelling doesn't change

We may need to consider merging operations, to avoid being stuck with small patches that come from bad initialization (if the surfaces have finite support).

The labelling optimisation step can be performed by either:

* alpha expansion (graph cuts) as in original PEARL paper: https://www.csd.uwo.ca/~yboykov/Papers/ijcv10_pearl.pdf
* Primal dual optimisation (with relaxation of labels to soft assignments): https://openaccess.thecvf.com/content_cvpr_2018/papers/Amayo_Geometric_Multi-Model_Fitting_CVPR_2018_paper.pdf

## Surface model

The surface model needs to have a small number of degrees of freedom, and be practical for:

* Closest distance computation
* Fitting to sparse point cloud

Patch surface models:

* Bezier patch, NURBS patch
  * Few degrees of freedom (ctrl points)
  * Should be possible to project points on it, not sure how to fit to points (for dense point clouds: [Non‐Uniform B‐Spline Surface Fitting from Unordered 3D Point Clouds for As‐Built Modeling](https://onlinelibrary.wiley.com/doi/abs/10.1111/mice.12192))
* Mesh (with smoothness constraint)
  * Lots of degrees of freedom --> add some smoothness constraint (eg max dihedral angles, or through smoothness objective in fitting?)
  * Easy to project points on mesh, fit by using [least-squares meshes method](https://igl.ethz.ch/projects/Laplacian-mesh-processing/ls-meshes/ls-meshes.pdf)
* Polynomial surface (along with support plane)
  * Few degrees of freedom
  * Easy to project and fit
  * Infinite support

## Initialization

We need to find an overcomplete set of surfaces, such that all strokes could be roughly covered by at least one surface.

Practical initialization methods:

* Generate surfaces from intersections / near intersections between 2 strokes in the sketch --> places where there is the most reliable info about surfaces
  * Swept surface (Sweep Canvas), Bezier patch ([Parametric leaf model](https://openaccess.thecvf.com/content_ICCV_2017/papers/Chaurasia_Editable_Parametric_Dense_ICCV_2017_paper.pdf))
  * Fit a polynomial surface (Dual representation modelling)
  * Plane
* Generate surfaces from 1 stroke
  * Ruled surface (needs to define or randomly sample a normal direction)
* Initialize surface from scribbled patch (and ensure that this patch model is preserved during segment/fit)

Once we have these surfaces, we can convert them to the most practical representation for the next step if needed (eg: tesselate into mesh).

