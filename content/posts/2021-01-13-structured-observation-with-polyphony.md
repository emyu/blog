---
title: "Structured Observation With Polyphony"
date: 2021-01-13T13:24:19+01:00
draft: false
tags: ["Paper"]
summary: "To better understand how musicians compose, and what input modality they prefer using, they observe users’ practices with the goal of discovering patterns or differences, but they do it in a controlled frame: all users have the same task and use the same software that can record their interaction data for analysis."
link: "https://hal.inria.fr/hal-00988725/document"
---

* Introduce the practice of "structured observation" (see also the report: https://www.lri.fr/~bibli/Rapports-internes/2014/RR1571.pdf), which consists in setting up a controlled experiment, with a given task and specific software that records data, but instead of using it to test some pre-defined hypotheses, they just observe users' behavior in a systematic way and try to find similarities and differences between participants.
* A challenging step was to define the task that the participants would have to complete, since it had to be short, but still creatively challenging to be a good proxy of a real creative task.
* Among participants they observe what input modality is used throughout the task. They also note different approaches that participants had to solve the task, different orders of operations and strategies.

