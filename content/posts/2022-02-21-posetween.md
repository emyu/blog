---
title: "Posetween"
date: 2022-02-21T15:48:23+01:00
draft: false
tags: ["Paper"]
summary: "Automatic inbetweening for human pose related motion of objects, guided by a video"
link: ""
---

* From an input video, a human pose is inferred and tracked across frames
* They use the pose animation to infer relationships between an image placed at some keyframes of the video, and one or multiple joints of the body
* This lets them automatically create the correct inbetween transformations for the overlay images
* They also have an interface for the user to correct the inferred pose and inferred joint / object association