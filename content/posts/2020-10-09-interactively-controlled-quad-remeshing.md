---
title: "Interactively Controlled Quad Remeshing of High Resolution 3D Models"
date: 2020-10-09T12:35:41+02:00
draft: false
tags: ["Paper"]
summary: "Proposes a method to intelligently decimate a high resolution 3D mesh in order to be able to apply integer grid parameterization at near interactive speed (with different types of user-defined constraints) to control the creation of a quad mesh. Their method is able to propagate the parameterization computed on the low resolution mesh to the full resolution one."
link: "https://www.graphics.rwth-aachen.de/media/papers/esck2016_600dpi.pdf"
---

To enable users to interactively control a quad remeshing of a high resolution 3D model, it is necessary to work with a smaller resolution mesh because the methods to generate an integer grid map (IGM), and thus a quad mesh, are too slow on meshes with lots of vertices.

So the authors propose a way to:

* Decimate the input high res mesh into 2 coarser mesh (one intermediate and one fully decimated), while respecting 2 objectives:
  * Preserve edges and vertices necessary to apply user-defined constraints (eg the user brushes a region to describe edge flow direction, or specifies a hard edge that should be preserved, or specifies the position of irregular vertices). These features should still be present in the coarse mesh so that it is possible to apply those constraints in the IGM generation
  * The IGM computed on the coarse mesh should be reasonably close to what would be obtained on the fine mesh. Because the IGM parameterization methods often rely on the surface's Gaussian curvature distribution to place singularities in the field, it is important to minimize the change in local Gaussian curvature. Thus they use variation in the sum of Gaussian curvatures at each vertex as the quantity to minimize, when choosing which vertex to remove from the mesh
* Find a parameterization on the fine mesh that corresponds to the parameterixation on the coarse mesh. To do so they:
  * Store a log of each decimation operation, such that it is possible to know how to invert the change applied at each step of decimation (edge flip or edge collapse)
  * Define operators that translate each mesh operation (flip and collapse) into the equivalent change to apply to the parameterization (such that it would be correctly defined on the finer mesh, before the decimation operation)

They implement [an interactive application](https://youtu.be/5-P7G7EIUao) where the user can apply constraints on the parameterization, and see quite fast the change to the generated quad mesh.
