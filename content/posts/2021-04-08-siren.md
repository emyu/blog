---
title: "SIREN: Implicit Neural Representations with Periodic Activation Functions"
date: 2021-04-08T13:49:15+02:00
draft: false
tags: ["Paper"]
summary: "Using periodic activation function in a MLP enables learning a function from its derivatives, or from a differential equation."
link: "https://vsitzmann.github.io/siren/"
---

* They propose to use periodic activation functions in MLPs, instead of ReLU or tanh activations
* This seems to have 2 main advantages
  * Better representation of details (not sure why, but is is clear in the results)
  * The ability to learn a signal given information about its derivatives, or even a differential equation, since the derivatives of the function learnt by the MLP can be analytically computed, and their derivatives are "well-behaved" compared to other activation functions. Not sure what's the problem with other activation functions, but the intuition about SIREN quality is that a derivative of a SIREN MLP is itself a SIREN since derivative of sine is cosine etc.
* Applications: fitting a signal given its derivative
  * Fitting an image given the gradients, Poisson image reconstruction (fusing the gradients of 2 images and reconstructing a composite image by learning the signal given the gradient)
  * Reconstructing the SDF of a point cloud with normals, by having a loss with both the function (should be zero at the points) and the derivative of the function (should be aligned with the normals, and unit length)
* In SDF fitting, they have an additional term that penalises non zero SDF values far from the points, which could be some kind of regulariser