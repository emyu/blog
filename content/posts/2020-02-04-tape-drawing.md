---
title: "Creating Principal 3D Curves with Digital Tape Drawing"
date: 2020-02-04T17:04:44+01:00
draft: false
tags: ["Paper"]
summary: "**Grossman T., et al., CHI 2002**. Taking inspiration from  traditional tape drawing technic, the authors explored how to enable skilled tape drawing artists to create 3D models of a car. The contribution of this paper is to allow users to create non planar 3D curves, while retaining the interface metaphor of fastening tape on a 2D surface."
link: "https://d2f99xq7vri1nk.cloudfront.net/legacy_app_files/pdf/2002CHIPrinciple3DCurves.pdf"
---

## Tape drawing

Tape drawing is a traditional technic used in the automotive design industry to create large scale 2D drawings of cars. Skilled tape drawing artists can use this technic to create both straight and smoothly curved lines, characteristic of a car design.

{{< youtube PuZJO2jGGe0 >}}

The left hand is used to fasten the tape to the wall, while the right hand holds the roll of tape to release more, as it is needed. The right hand maintains tension in the non-fastened piece of tape and therefore controls the direction in which the tape will be fastened. By moving the right hand, the artist can create smoothly curved lines.

## Digital tape drawing interface

The authors created a digital interface for tape drawing that aimed at mimicking the medium as close as possible [[Digital Tape Drawing, 1999]](https://dl.acm.org/doi/pdf/10.1145/320719.322598). In this system both hands are used, as in traditional tape drawing, and the movement of the right hand controls whether the tape will be fastened in a straight line (fixed right hand) or in a curve (mobile right hand). In the case where the right hand moves, how far the right hand is from the left hand determines the curvature of the resulting curve.

## 3D Curves

To create 3D curves that are planar, the user can choose an axis-aligned plane embedded in the 3D working volume. Once a plane is chosen, they can draw using digital tape drawing.

To create 3D curves that are non-planar, the authors introduce a method in this paper: the user should first draw the "shadow" of the curve on an axis aligned plane (a). Then they can visualise this shadow curve in a perspective view (b) and choose a drawing plane among the axis aligned plane on which to draw the final curve (c). The final curve can then be drawn using the previously described digital tape drawing interface in orthographic view, and it will be projected on the green curved surface seen on (c).

{{< figure src="media/3d-tape-drawing.png" caption="Method to create a non-planar 3D curve, figure from this paper." width="200px">}}

## From orthographic to perspective view: draw-inspect workflow

In this system, curves can only be drawn from an orthographic view, but the user needs to see a perspective view in order to understand where the curves lay in 3D. The authors propose a fluid method to transition between these 2 views. When in orthographic view, the user can drag with their non-dominant hand to "move the camera" around the object and see it in a perspective view (the view matrix is smoothly interpolated). When the user releases the drag, the camera smoothly transitions back to the nearest orthographic view so that the user can continue drawing.

## Conclusion

While this work contains many interesting interface explorations, such as the "shadow curve", the camera trick and the 2-handed control of curvature, I think tape drawing is too far from traditional sketching to be adaptable for my current project.
