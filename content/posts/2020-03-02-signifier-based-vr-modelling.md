---
title: "Signifier Based VR Modelling"
date: 2020-03-02T10:19:42+01:00
draft: false
tags: ["Paper"]
summary: "Signify affordances on the model itself for a modelling interface in VR."
link: "http://people.compute.dtu.dk/jerf/papers/vrsignifiers.pdf"
---

Interesting ideas for the visualisation of signifiers that could also be applied to my project:

* Hide signifiers in a "viewing mode". The mode switch can be automatically detected based on user actions (eg: lowering hands below the model means that they are done modelling for now).
* Auto-scale signifiers based on distance to hand (like MacOS dock icons).
