---
title: "Physical Vr Painting Design Space"
date: 2022-02-21T12:06:18+01:00
draft: false
tags: ["Physical 3D Paint"]
summary: "Ideas."
link: ""
---

We strive to define a more "physical" way to do painting in a 3D space such as with VR tech. Physical VR painting may mean creating marks that share some of the qualities of marks made with traditional 2D media (in appearance or in how they behave), or marks that can interact with a physical art practice.

Such a painting tool is first defined through the qualities and behaviors of its marks:

* Mark appearance
  * Reproduces 2D physical marks? Paper grain, boundary effects caused by interaction with surface
  * Coherent 3D appearance? Like a 3D volume embedded in 3D space
  * Appearance depends on scene lighting and marks cast shadows? "Physicality" => appearance behaving as that of a physical 3D object?
* Mark controllability (user influence on marks)
  * Mark appearance strongly coupled with physical motion? (eg: speed influences deposition as in chinese ink, direction of motion influences appearance as in oil paint where the direction of the brush hair is visible, "learning by being physically engaged with the body")
  * Erasing / Modifying marks after the fact? (eg: impasto in oil painting, color blending in watercolors, smudging charcoal)
  * Stochasticity / randomness in marks? (or semi-controlled behaviors, eg watercolors)
  * Tool authoring for artists? How to surface the control of some behaviors to the user?
  * Can physical artifacts be used to influence mark appearance? or create marks?
* Mark influence on the space
  * Marks influence each other's appearance? Transparency, colors mixing
  * Digital marks influence physical artifact?