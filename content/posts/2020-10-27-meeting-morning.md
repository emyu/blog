---
title: "Meetings"
date: 2020-10-27T10:13:18+01:00
draft: false
tags: ["Thoughts"]
summary: "Notes from 2 meetings, about conducting interviews with artists, data collection, and ideas for technical projects."
link: ""
---

## Collecting sketches

* Maybe the ideation VS concept discussion is not so specific to 3D sketches, so it doesn't bring more to study it in VR (related work: studying the steps of the creation process with sketches and physical prototypes: http://www-sop.inria.fr/reves/Basilic/2016/BTOM16/)
* The tool influences the way people sketch:
  * TiltBrush: ribbons, colors, lighting => paintings, no "curve network" type sketches
  * ILoveSketch: encourages users to use few planar sections => curve network, not much "messy" details
  * CASSIE: could be used to attain some kind of mix of network and messy strokes, by weakening beautification, maybe making it semi-manual (only activated at will)
* In the context of trying to interpret a sketch with an algorithm, it may be easier to look at concept sketches, since we know that they contain interpretable meaning (as they are supposed to be interpretable by others, while the ideation part of the process is done for oneself)

## Interviewing artists

* It seems we should be able to gather enough relevant contacts (4 artists with whom we have already established contact + a dozen others who we could reach out to)
* We could interview them to better understand how 3D sketching is used in their process, what they value in it compared to other tools, what are their weak points compared to other tools
* Two possible outcomes:
  * Motivation and insights for some future projects: identify what would be good opportunities for algorithmic solutions where they are lacking (see: [Penrose](http://penrose.ink/siggraph20.html) and [study](https://www.cs.cmu.edu/~woden/assets/chi-20-natural-diagramming.pdf)), back geometric heuristics based on how artists say they use 3D sketches to represent shape (see: [True2Form](http://www.cs.ubc.ca/labs/imager/tr/2014/True2Form/index.htm) and sketching books)
  * Field study paper (eg: [Diagramming](https://www.cs.cmu.edu/~woden/assets/chi-20-natural-diagramming.pdf), [Color Palettes](https://hal.archives-ouvertes.fr/hal-01226494/document))
* Conducting interviews in VR so that people can showcase their work and do live demos?
* Gathering data from artists: ask them to sell/donate some sketches to us, so that we can gather a dataset of "sketches in the wild" to test our methods on

---

*EDIT: additional insights from afternoon meeting*

* We all agree on the interest of understanding how artists use 3D sketching in their workflow
* We can also look at artworks (eg: [those listed here]({{< ref "posts/2020-10-21-references-3d-sketching.md" >}}), [Google Poly](https://poly.google.com/) Tiltbrush sketches) and study the different depiction strategies used in VR sketching/painting, as many have no 2D equivalent
  * Ink like sketch with occluding volumes sketched manually
  * Sketching shading manually
  * Using stroke brushes as volumes, surfaces (analogy with sculpting, paper prototyping?), or only as a holder of geometric information (analogy with a pencil)
* Should we look at different VR mediums too, such as sculpting (Adobe Medium) or poly modelling (Google Blocks)? Or restrict to sketching?
  * Volume-based approach to shape exploration seems to be very different from sketch-based approach, so it may be interesting to look at how people use those differently, and if they have a clear preference
  * But this would make the scope of the study too large
* This google doc summaries the questions we raised so far: https://docs.google.com/document/d/1G4LB7pT7JHK1bDQBPoE8TBK7nzVcKTFg7nemvQdJkSY/edit?usp=sharing

## Stroke labelling for fuzzy surfacing

* Our guess is that a sketch can frequently contain strokes of different semantic meaning (curve network, skeletal, ridge/valley). This would motivate a surfacing tool capable of dealing seamlessly with different types of strokes, to propose a unique final surface

* We would need the following pieces:

  * Labelling strokes in a sketch based on some heuristics (eg: geometric measures as in [SecondSkin](http://www.dgp.toronto.edu/~depaolic/projects/secondSkin/), [Overcoat](http://zurich.disneyresearch.com/OverCoat/))
  * Each of the strokes generates a surface based on its label and geometry (some inspiration: [BendSketch](https://haopan.github.io/bendsketch.html), SecondSkin)
  * Aggregate all surfaces into a coherent, maybe somewhat smooth (or other global coherence criteria), single surface

* We can start by trying to find the labelling heuristics. Possible sources of data:

  * CASSIE freehand sketches
  * Sketchfab models
  * ILoveSketch data
  * We could ask some people to do a few more sketches with a version of CASSIE that has weaker beautification (to leave the possibility for non-curve network type sketches)

  ---

  *EDIT: additional insights from afternoon meeting*

  * Learning could be used, if we have significant amount of data, but it's not guaranteed that we can get that

## Misc ideas

* Strokes can define a parameterization of 3D space in a small neighborhood around them, could this be used for constrained manipulation of parts (inspiration: [MeshMixer](http://papervideos.s3.amazonaws.com/meshmixerSketch10.pdf))
