---
title: "Crossshade"
date: 2020-02-20T10:08:39+01:00
draft: false
tags: ["Paper"]
summary: "CrossShade is a method to automatically generate normals along the surface of a concept sketch to shade it. To do so it uses knowledge about cross-section curves to infer 3D normals from the 2D sketch."
link: "http://crossshade.com/"
---

## Knowledge about cross-section curves

"Cross-hairs", the intersections of 2 cross-section curves carry important perceptual shape information. The authors found these properties from studying sketching conventions and the way people naturally perceive shape from descriptive curves.

1. The 2 curves lie on orthogonal planes. With $n_i$ the normal to one of the planes:

   $n_1 \cdot n_2 = 0$

2. These curves are curvature lines (at least, close to the intersection). With $t_i$ the tangent to one of the curves:

   $t_1 \cdot t_2 = 0$

3. They are local geodesics (at least one is). The following property is most probably satisfied:

   $t_1 \times n_2 = 0, t_2 \times n_1 = 0$

4. They are drawn in an informative viewpoint, so foreshortening is minimized. With $z$ the direction orthogonal to the view plane, they minimize:

   $\sum (t_1^z)^2 +(t_2^z)^2 $

5. The orientation of the surface they define is most probably convex (normal pointing towards the viewer).

Moreover, silhouette curves are used to disambiguate orientations of the normals.

## Method

### Estimate 3D positions for cross-section curves

They use the interior point method to perform a constrained optimization to minimize the 2 weaker conditions (3 and 4) while enforcing the 2 first conditions (1 and 2). They further simplify the problem by assuming that the cross-section planes are not perpendicular to the view plane so they choose $n_i^z = 1$ for all planes.

### Propagate normals from cross-hairs to all the surface

First, curvature frames ($n, t_1, t_2$) are propagated along curves starting from the known normal at the cross-hair, by propagating from one cross-hair and minimally rotating the frame along the curve. If a curve is bounded by 2 cross-hairs, the rotation of the frames are smoothly blended between the 2 rotations obtained starting from the cross-hairs independently, with arc-length weighting.

Then the normal between curves is found by applying [Coons interpolation](http://www.farinhansford.com/dianne/materials/FarinHansford1999.pdf) to patches bounded by 4 segments. Because the surface is not formed of well-defined quad regions with cross-section curves as sides, some tricks are used to make it into quad regions (mid-point subdivision) and to define normals along boundary curves (copying normals from opposite segments and rotating those).
