---
title: "Project Update"
date: 2021-03-30T17:22:45+02:00
draft: false
tags: ["Surfacing Project"]
summary: "Meeting about using implicit surfaces, focusing on how to find the final mesh from the implicit surface patches."
link: ""
---

## State of the project

* Implicit surface patches can be used in conjonction with the proxy mesh to obtain the final surface. We can set the boundary is either sharp or smooth. This is solved as an optimization problem, initialized from the proxy mesh.
* This doesn't work so well if the proxy is initialized too far from the surface, or if the surface has an unexpected and complex topology close to the proxy.

{{< figure src="media/2021-03-30/shoe-sharp.png" caption="Sharp boundaries" width="300px">}}

{{< figure src="media/2021-03-30/shoe-sharp-2.png" caption="Sharp boundaries" width="300px">}}

{{< figure src="media/2021-03-30/shoe-smooth.png" caption="Smooth boundaries" width="300px">}}

{{< figure src="media/2021-03-30/shoe-smooth-2.png" caption="Smooth boundaries" width="300px">}}

## For next time

* Try to lift the proxy by simply constraining the projection of the strokes on the mesh to match the 3D strokes
  * This could serve as initialization to obtain a correct final mesh
  * This is a basline method we can compare against. Our intuition is that this simple method should fail due to imprecise stroke intersections and overlapping, and fail to recover sharp features
* Try to incorporate the implicit surfaces in PEARL segmentation, to evaluate the full pipeline
* Test the method on very simple sketches where we can imagine the correct solution easily (eg: a capsule, cylinder, cube)
* Better polynomial implicit fitting might be necessary for PEARL to work correctly, notably regularization