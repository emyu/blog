---
title: "Sketching Piecewise Clothoid Curves"
date: 2020-04-01T09:59:41+02:00
draft: false
tags: ["Paper"]
summary: "Fitting piecewise clothoid curves to an input polyline. The clothoid has G2 continuity, which means that its curvature is a continuous function of arc length. Moreover its curvature is a linear function of arc length."
link: "http://www.dgp.toronto.edu/~karan/papers/sbim2008mccrae.pdf"
---

## Clothoids

Nice for road design, rollercoaster design. Fitting a sketched curve by a piecewise clothoid makes the curve look naturally fair. It naturally encourages line segments and circles or circular arcs.

## Method to fit an input polyline with the smallest amount of connected clothoids

Estimate curvature at each point of the polyline so that all points can be plotted in curvature space (y = curvature, x = arc length).

Then they use dynamic programmig to find the best segmentation of the input so that each piece of the segmentation can be fitted with a line segment in curvature space. They minimize a sum of the fitting energy (takes into account the fitting error in curvature space) and the cost of adding more segments.

Once they have the piecewise linear fitting in curvature space, each line segment maps to a unique clothoid, then they translate and rotate each segment to connect end points and align tangents. Then they find the optimal rigid transformation to align the piecewise clothoid to the input stroke.

## Handling sharp corners ( G1 discontinuity)

Sharp corners correspond to big spikes in curvature space. Therefore they can easily threshold those out (high value of curvature and of variation in curvature) and apply the same method as before, the difference being that they need to find the optimal rotation for each separate piecewise clothoid (the sharp corners act as joints, fixed in translation but not in rotation).

## Handling position constraints and curve modifications by over sketching

To modify locally their piecewise clothoid so that it interpolates given position constraints or follows an over-sketched modification, they blend it with a quintic Hermite spline.
