---
title: "PortraitSketch"
date: 2020-04-01T10:32:01+02:00
draft: false
tags: ["Paper"]
summary: "Interactive tool to assist novices in sketching realistic portraits, while preserving as much as possible their sketching style."
link: "https://dl.acm.org/doi/pdf/10.1145/2642918.2647399"
---

Helps a novice trace a picture in a way that looks more aesthetically pleasing.

## Method

The method is based on image processing algorithms to detect edges, shaded zones and features on a picture of a face. This gives knowledge about where a trained artist would likely draw strokes to depict the face.

This knowledge is used to constrained strokes drawn by the user. Input strokes are fitted with cubic béziers and then adjusted so they match the nearest edge or feature. There is also a semantic correction: correct strokes (those coherent with the edges found in the first step) are emphasized by being rendered thicker, while others are de-emphasized.

The same kind of mechanism is applied for shading strokes.

## Evaluation

* Make each participant test 4 different levels of assistance
* 3 different target styles that the users could take inspiration from --> show that their method allows for variety in style, which they showcase as figures
* Drawings evaluated by independent judges (2 experts) on artistic quality. Classify the 4 drawings by the same participant (with different levels of assistance). Choose the best drawing between an unassisted drawing and an automatically beautified drawing a posteriori
* Participants would rate if they felt like they created the drawing themselves, and whether they felt like the system helped them (for each assistance level)
