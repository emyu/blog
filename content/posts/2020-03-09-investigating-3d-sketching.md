---
title: "Investigating 3D Sketching for early conceptual design"
date: 2020-03-09T09:46:23+01:00
draft: false
tags: ["Paper"]
summary: "This paper conducts a focus group and a user study with professional designers to determine whether 3D sketching can be beneficial for them."
link: "https://www.sciencedirect.com/science/article/pii/S0097849309000855"
---

## Focus group

With a focus group of designers, the authors gather insight on what are the properties that the experts find of value in traditional sketching and what they would expect from using a 3D sketching interface.

Core properties of sketches (from [Buxton](https://books.google.fr/books?id=2vfPxocmLh0C&lpg=PP1&ots=06Sskfe7_H&dq=buxton%20sketching&lr&hl=fr&pg=PP1#v=onepage&q=buxton%20sketching&f=false) and confirmed by focus group):

* Quick and inexpensive process
* Plentiful results (multiple sketches)
* Clear vocabulary
* Minimal detail, suggest rather than confirm

Advantages from 3D sketching that the experts desire:

* Spatiality: perceive spatial impact of the models, communicate the idea better
* One-to-one proportionality
* Associations with existing objects or spaces (by importing them in virtual space)
* Formability: possibility to warp and deform virtual sketches

## User study: 2D vs 3D sketching in a CAVE

Participants (students and professional designers) were asked to sketch furniture design in the CAVE in 2 configurations:

* 3D, they can and are encouraged to use the whole 3D space available
* 2D, they used the same system as in 3D but were asked to manually restrict themselves to drawing in an imaginary vertical plane

Then the users answered a questionnaire with both ratings on some scale and open questions to capture their subjective perceived advantages/disadvantages of both conditions.

Positive statements: develop creative design ideas, interact with the sketch, easily communicate design ideas to others and externalize ideas. Simplify spatial perception, support cognition and reflection processes, foster inspiration and creativity.

Negative statements: sensorimotor control in 3D environment, difficulty to find connection points, to draw straight lines and sketch at different depths. Ergonomics of the device used (pen has a wire connection, lag between drawing and visual feedback). Lack of functionalities (snapping, zooming, erasing).

The authors believe that the deficient sensorimotor control can be overcome with practice, and state that they have informally observed improvement with experience in their study.

They are confident that 3D sketching has the potential to become a new tool for creative work, but they state that new sketching tools should absolutely preserve the sketch character of the models to keep ambiguousness of sketches.
