---
title: "Sketchy Rendering for Information Visualisation"
date: 2020-05-09T13:32:48+02:00
draft: false
tags: ["Paper"]
summary: "Study on user response to the sketchiness of a visualisation, from quantitative ability to perceive sketchiness as a quantity to the effect on engagement."
link: "https://openaccess.city.ac.uk/id/eprint/1274/"
---

The authors present sketchy visualisations based on simple rules to distort lines and ellipses and to fill shapes with hachures.

Then 2 evaluations are carried out.

## Low-level evaluation

They want to find out whether sketchiness of a shape can be perceived on some sort of scale and whether sketchiness affects perception of common visualisation variables such as scale of a shape.

Study: 137 volunteers participated in a remote study were they had to compare 2 renderings of shape to determine which was smaller or which was less sketchy.

Results:

* Sketchiness can be perceived on a scale, but not very reliably by all participants (it is easy though to recognize whether something is sketchy or not, as an ordinal variable)
* Sketchy renderings result in greater error for size estimation

## High-level evaluation

They explore whether sketchiness improves engagement from users. They use a proxy measure of "willingness to annotate" (nb of annotations and time that user spends annotating the graph) to measure that.

Study: 52 volunteers for a remote study. They had to annotate 5 visualisations, with different themes, one group had only sketchy visualizations and the other had only non sketchy visualizations.

Results: they don't get significant results in favor or in disfavor of sketchiness, at least not something consistant across all 5 visualisations. They do note a positive aspect for a visualization consisting of one sketchy and one non sketchy graph (sketchy = estimation). Also the participants that had sketchy viz liked the study a lot more than the others.

