---
title: "Video Doodles Next Steps"
date: 2022-03-25T10:04:16+01:00
draft: false
tags: ["Video Doodles"]
summary: "To dos to start this project"
link: ""
---

* Build a minimal interface that we can experiment with
  * ~~Load an image sequence (and other metdata like depths and cameras)~~
  * Place keyframes
  * ~~Create a 3D object~~
  * ~~Render the 3D objects from the camera of each keyframe and composite depth~~
  * Apply 2D transforms to an object at a keyframe
  * ~~Data: sequence of (1) images (2) depth images (3) camera poses~~
* Build "backend" code that can solve for the space-time trajectory of a 3D object
  * Given 2+ keyframes of an object with different transforms + camera poses at frames, infer a space-time 3D trajectory that matches the keyframes
  * Given a keyframe of an object with some occlusion cue, infer its depth
* Pre-process modules:
  * Infer depth + camera poses for all frames --> Consistent Depth of Moving Objects in Video
  * Some kind of scene flow or motion segmentation?



Editing video doodles, different levels of assistance:

* Completely manual
* Screen space with depth masking and explicit control of depth
* Screen space with depth masking and assisted control of depth
* World space with depth masking and assisted control of depth (static doodles have a consistent position across frames)
* World space with depth masking and assisted control of trajectory (support doodles that track moving objects in the scene)



Two sources of uncertainty/challenge:

* Errors on camera poses and depth maps from inferrence
* 2D --> 3D : 2D interaction to 3D result