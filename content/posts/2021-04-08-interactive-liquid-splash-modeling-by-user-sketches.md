---
title: "Interactive Liquid Splash Modeling by User Sketches"
date: 2021-04-08T13:49:44+02:00
draft: false
tags: ["Paper"]
summary: "Using a cGAN to reproduce the effect of a liquid splash, based on some input 3D strokes that indicate fluid flow trajectories and velocity."
link: ""
---

* Simulation of liquid splash with physics-based methods is very time-consuming and it is hard for the artist to control the desired splash shape based on initial simulation condition. So they propose a learning based approach to reproduce directly the desired shape given by a few strokes and the velocity of sketching motion.
* Conditional GAN (cGAN) can learn to synthesize a liquid splash shape given as input ("conditional guidance") the sketch of flow-lines
  * Input = voxelized sketch, a scalar and velocity field in a uniform 3D grid. That's because the polyline representation would be inconsistent depending on the sketch, whereas the voxelized fields can have all the same dimensions for all input sketches.
  * Output = scalar and velocity field that represents the liquid splash in an implicit manner
* Data generation: simulations of liquid splashes with different initial conditions + extract some streamlines from the liquid flow and cluster those to stand for the "strokes" that a user would sketch
* There seems to be a good mapping between user strokes and streamlines
* Refinement steps: after getting the initial splash shape with the trained cGAN, they refine the shape by adding small scale detail with simplified simulation
  * Particle based simulation for small droplets
  * Surface wavelets

