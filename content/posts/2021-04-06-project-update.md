---
title: "Project Update"
date: 2021-04-06T19:18:42+02:00
draft: false
tags: ["Surfacing Project"]
summary: "Baseline to compare against, dataset and proxy mesh editing interface."
link: ""
---

## State of the project

* We have a method to go from a very approximate proxy mesh to a mesh that fits the 3D strokes in a least squares sense
  * This is a baseline we can compare against.
    * Pros of the baseline: fast
    * Cons of the baseline: can not recover sharp features, no user control, no segmentation
  * We can use the "better proxy" obtained in this way as an initialization for the final surface optimization.

{{< figure src="media/2021-04-06/baseline-mouse.png" caption="Smooth computer mouse surfaced with baseline method: good result" width="300px">}}

{{< figure src="media/2021-04-06/baseline-shoe.png" caption="Smooth shoe surfaced with baseline method: good result" width="300px">}}

{{< figure src="media/2021-04-06/baseline-cylinder.png" caption="Cylinder surfaced with baseline method: no sharp features" width="300px">}}

{{< figure src="media/2021-04-06/baseline-hat.png" caption="Hat surfaced with baseline method: the sharp feature between top and brim is lost." width="300px">}}

* I found Tibor's dataset which we can use to further test our method: http://www-sop.inria.fr/members/Emilie.Yu/3d-sketch-viewer/
* I experimented with a simple interface to create a proxy mesh for a sketch, using simple primitive shapes like a capsule. Further I would like to use implicit surface blending to combine multiple primitives, like for the shoe proxy mesh.

## For next time

* Try to incorporate the implicit surfaces in PEARL segmentation, to evaluate the full pipeline
* Test the method on very simple sketches where we can imagine the correct solution easily (eg: a capsule, cylinder, cube)
* Better polynomial implicit fitting might be necessary for PEARL to work correctly, notably regularization
* Try to add label cost to PEARL segmentation/fitting to penalize complex surface models

## Remarks/ideas

* Adrien: leverage PEARL label cost to encourage fitting the data with low-degree / simple polynomial implicit surfaces
  * Introduce multiple polynomial surfaces for each patch (with varying degree), with the hope that PEARL with label cost would be able to choose the right compromise between fitting the data faithfully and model simplicity
* Andreas: replace cotangent Laplacian with umbrella operator for smoothing, since we don't need to preserve proxy mesh initial edge lengths
* Andreas: adaptive remeshing could be introduced in the final mesh optimization, to refine the mesh in places where more resolution is necessary. It is not clear at which steps of the optimization we should do the refinement, probably not at every step since we would need to recompute the Laplacian matrix after every remeshing operation. Also it will be unclear what label the new vertices should have.
* Rahul: does the segmentation that our method naturally gives present any usefulness to the user?
  * No clear answer now, we should come back to that later
  * Maybe selective mesh edition?
  * Is there something to do with the curve network that arises from the boundary between patches?
* Andreas: the segmentation method seems to be able to recover the intent of the user, as in discover where sharp features should be placed, whereas the baseline (least-squares fitting the strokes with the proxy mesh) doesn't.
  * This narrative could be used to differentiate from baseline method

