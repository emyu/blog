---
title: "Finding the optimal subset of position constraints"
date: 2020-03-31T18:57:30+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Notebook explaining the energy terms for fidelity to input and constraint satisfaction, with demos on greedy constraint subset selection."
link: ""
---

[Jupyter Notebook](notebook/constraints_bundler_demo.html)

Additionally, [this other notebook](notebook/update_march_27.html) details a bit more the reasoning behind the update on the fidelity energy.

