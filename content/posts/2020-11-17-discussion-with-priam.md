---
title: "Discussion With Priam"
date: 2020-11-17T09:05:41+01:00
draft: false
tags: []
summary: "Summary from meeting with Priam. Feedback about CASSIE and more general discussion about VR creation tools."
link: ""
---

## Sketching

* It is about finding the form the artist want. Form finding as done with sketching is impossible to do with existing 3D tools (eg: Blender, Rhino, GS): there is too much complexity to deal with
* Should put the person in a state of flow, that is uninterrupted by arbitrary needs to take decisions (eg change mode, change tool)
* The desired shape is not clearly defined before drawing, but rather subconscious. So the act of drawing itself and the tool used influence the result (eg: the physical matter of sculpted clay)
* What's good about sketching in 3D: it is possible to create at 1:1 scale, to translate ones momentum ("élan") into the stroke, no space limit (as with a sheet of paper)
* Necessity to have a simple/direct tool that doesn't get in the way of this process (he appreciates that in CASSIE, compared with GravitySketch and the variety of tools and features available). So that we don't lose this subconscious momentum to create
* Need to control the curve after creation, but control points of NURBS/Bézier are not on the curve, what we want is to move the curve directly (control point on the curve)
* He appreciated sketch beautification in CASSIE, especially locking curves at intersections
* Needs more precise control over which strokes should be planar (and in which planes).

## Feedback

* Need more than visual feedback about the sketch, to fully understand how it lies in 3D (Priam is very interested about that aspect)

## Surfaces

* Appreciates the idea of coupling surface creation with sketching in CASSIE: real-time feedback, no mode or tool change, not different steps of a process, no specific rules about order of the process, the artist is free to start from wherever he likes
* Appreciates the economy in using the same curves to also create the surface, no need to re-create the curve again once it's sketched and he is happy with it
* The curves can represent either sharp or smooth edges, they can also represent a small scale valley or bump on the surface
* Sketching curve networks is natural in a lot of cases: the artist often needs that many curves to define how the surface should look anyway. A lot of objects in industrial design are basically shells, made of flat-ish panels that meet at feature lines. He does recognize that some cases do not require connectivity (detail on the side of a car), and it would be nice to support those.

{{< figure src="media/11-17/old-car.png" caption="He used a physical car maquette to point at what he wanted to show" width="300px">}}

* What's lacking in CASSIE is a way to precise how the surfaces should look once they are created: eg how to define smooth transitions between patches, deform patches once they are created (control points on surface). Idea: unified way to deform curves and surfaces, with some kind of handles. Curves and surfaces could be dynamically coupled (one deforms the other). For example curve on surfaces could act as handles or indicator for sharp features / bumps.
* Some surfaces (such as the clay wheel) would be challenging for the artist to create from curves, but he still thinks our approach (with CASSIE, and curves) is better than existing ones.

{{< figure src="media/11-17/organic-wheel.png" caption="Clay model of novel wheel shape" width="300px">}}

* Another approach: TiltBrush like brush strokes that should be merged into surface patches (with trimming tools to define feature curves as the boundary of a patch). But this is less controllable for the artist maybe, too much clutter and strokes to control them.

## Workflow

* Paper will not be replaced by VR sketching
* But anything 3D/sculptural is only done in VR sketching for him: no need to use primitives and fight with them to get a result
* Ideal workflow: a completely unified way to add more and more details to the first rough sketch, progressing on that one in a single software (up until adding very fine details like joints on the windows, door handles), and then export fabricable output.

## Involvment in the project

* Priam is aiming at starting a PhD in autumn 2021
* He would be interested in participating more actively in the project, proposing ideas

