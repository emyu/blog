---
title: "How Well Do Line Drawings Depict Shape?"
date: 2020-10-16T11:09:56+02:00
draft: false
tags: ["Paper"]
summary: "A large scale study on how well people can infer 3D shape from line drawings, compared to shaded renderings and different computer generated lines. They provide a methodology to gather such data at large scale and they give insight on both how accurate the predictions are compared to ground truth 3D model and how much the participants agree with each other, for different kind of shapes."
link: "https://pixl.cs.princeton.edu/pubs/Cole_2009_HWD/index.php"
---

## Methodology

* Randomly sampled gauges on the 2D shape (+ a few densely sampled lines manually chosen to test specific lines)
* Participants orient the gauges (visualised as ellipse + normal) to indicate their perceived 3D shape
* Participants recruited through Amazon Mechanical Turk (~500 participants), and filtered by checking the consistency of their orientation choices (they are given twice the same gauge during one task)
* Because of 2D bas-relief ambiguity, they adjust results such that this variability of interpretation can be factored out between different people (to compare them to each other)

## Results

* Some models are difficult to interpret, even in the shaded version (smooth, blobby shapes)
* People have roughly similar interpretation of a line drawing (all close to the median estimation), but different line drawings (human vs different algorithms) lead to different interpretations
* Error from ground truth really depends on the type of object: smooth bumpy objects give high errors, while the cube-ish shape is very well predicted
* The shaded images lead to less errors (statistically significant for a majority of models), meaning that some information is always lost in the line drawings
* Looking at precise zones of objects and the errors there, it is clear that some lines are indispensable for the viewer to provide a correct estimation of the shape (smooth inflection of the surface of the screwdriver handle). Without these lines, people seem to go for a smooth, constant curvature interpretation (Fig. 8 c-d).

