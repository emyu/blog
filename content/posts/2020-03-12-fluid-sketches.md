---
title: "Fluid Sketches"
date: 2020-03-12T15:31:44+01:00
draft: false
tags: ["Paper"]
summary: "Sketching beautification based on shape recognition and simultaneous correction while drawing."
link: "https://dl.acm.org/doi/pdf/10.1145/354401.354413"
---

The idea behind this 2000 method is to have strokes morph into a beautified results simultaneously as it is being drawn. They hypothesize that giving such immediate feedback can allow the user to make adjustments to their sketch, as they see how their intention is interpreted.

## Morphing motion

To give shape to the notion that a stroke is being continuously refined and morphed into a beautified version, they introduce an equation that defines the movement of the curve, from where the user initially drew, to the final beautified and settled result.

## Recognition of shapes

Their method only beautifies strokes into circles, axis aligned boxes and line segments.

For circles and lines, they fit it by least-squares. They also give a bigger weight to the more recently drawn parts.

For boxes, they do as if springs were pulling on the edges of the box from the nearest points on the input stroke.

## User study

Unlimited time for the subject to familiarize themselves with the interface. Comparison with a classic drawing interface.

Task: reproduce a previously-drawn collection of shapes.

The results are not that good, mixed answers as to which interface is the preferred one.
