---
title: "Kinetic Shape Reconstruction"
date: 2020-12-10T14:46:28+01:00
draft: false
tags: ["Paper"]
summary: "Efficient method to recover the set of planar segments that form a watertight model from a point cloud."
link: "https://hal.inria.fr/hal-02924409/file/tog2020.pdf"
---

* Fit planes to the point cloud, eg by RANSAC
* Decompose each planes into intersection free polygons (cut them at intersections with other polygons)
* Kinetic data structure: create a priority queue where polygons are sorted based on how soon they will collide with any other polygon, assuming that they all grow at constant speed (the way they grow depends on whether the vertex is free, or if it's partly colliding with another polygon already, see Fig. 4 and inset on page 2)
* Pop a polygon from the queue and update it (and potentially the collided one too) based on the collision event: some or all of their vertices may become fixed, one may cross over the other, they may both split. They can be put back in the queue if they are not yet fixed.
* Extract the surface by finding the binary labelling for each cell: which cells correspond to the inside and which to the outside of the shape. Min-cut with costs:
  * Data: the inlier points for each polygon side vote by comparing their normal to the direction towards the center of mass of the polyhedron
  * Smoothness: between adjacent cells, proportional to the area of their shared face (avoids zig-zagging of the boundary)
