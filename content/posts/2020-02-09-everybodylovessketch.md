---
title: "EverybodyLovesSketch"
date: 2020-02-09T21:14:31+01:00
draft: false
tags: ["Paper"]
summary: "This extension of ILoveSketch focuses on bringing the tools of perspective drawing to a broader audience."
link: "https://dl.acm.org/doi/pdf/10.1145/1622176.1622189"
---

In this work, the target users are people who are not familiar with drawing in 3D or analytically. The system provides a way for them to specify 2D sketch surfaces on which to draw. While the user can also choose a NURBS surface created from a closed loop of 3D curves, the authors choose to keep these surfaces only as scaffolds for further sketching, as **they think that showing the surfaces could be detrimental by "biasing one's visual thinking"**.

{{< figure src="media/ELS-nurbs-surface.png" caption="" width="400px">}}

With this tool the user **never explicitly draws a scaffold**, as this is "unintuitive to an untrained user". Instead, scaffold lines are **dynamically created** and they are displayed on the active sketch surface. In this work, scaffolds are used only to define sketch surfaces and provide some visual guides. There is no snapping.

A grid of custom resolution can also be displayed on any sketching plane to simplify measuring proportions (resolution is specified by two taps on the plane, the distance between those defines the size of a cell).

{{< figure src="media/ELS-grid.png" caption="" width="400px">}}

Sketch surfaces can also be defined by extruding existing curves. In this case, there are actually multiple possible sketch surfaces (different sides) but the active one is defined as the one with "maximum sketchability" (see [ILoveSketch]({{< ref "posts/2020-01-19-ILoveSketch" >}})). This way the user can change active sketch surface by changing view.

{{< figure src="media/ELS-extruded-surface.png" caption="" width="400px">}}

