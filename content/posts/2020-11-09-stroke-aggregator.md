---
title: "Stroke Aggregator"
date: 2020-11-09T15:12:58+01:00
draft: false
tags: ["Paper"]
summary: "Consolidating a sketchy input into a clean line drawing."
link: "https://www.cs.ubc.ca/labs/imager/tr/2018/StrokeAggregator/"
---

* Pairwise angular compatibility for a first rough clustering
  * Pairwise compatibility score based on the angular distance: fit an aggregate stroke to the pair of strokes and define the mapping between the 2 strokes to the aggregate as the closest point (only defined in a zone where both strokes project to the same segment of the aggregate stroke). The pairwise angular distance is the mean of the angles between the tangents at a point on a stroke and the tangent of its mapping on the aggregate.
  * Clusters found by correlation clustering, inspired by [[Keuper et al. 2015]](https://openaccess.thecvf.com/content_iccv_2015/papers/Keuper_Efficient_Decomposition_of_ICCV_2015_paper.pdf)
* In each of the rough clusters: find clusters based on proximity between strokes
  * Initialize with clusters formed by near intersecting strokes (that respect angular proximity)
  * Then merge clusters if their intra-cluster proximity is bigger than their inter-cluster proximity (the distance between 2 strokes is computed by fitting an aggregate stroke and finding pairs of closest points to the aggregate strokes)
* Refine clusters to separate branches recursively. For each cluster:
  * Generate potential split of a cluster into left and right cluster, by looking at inter-cluster gaps: if there is a gap between 2 strokes that is significantly larger than the neighboring gaps between strokes, they look like 2 different clusters. Strokes are then sorted into 2 clusters by propagating the separation found at the point with a large gap.
  * Then the potential split is assessed: if the inter-cluster gap is significantly bigger than the intra-cluster gaps, then they accept the split
  * They recursively check each new cluster created by splitting
* Finally assess each pair of clusters and merge them if they respect the criteria of:
  * Narrowness: the resulting cluster would still be kind of narrow, like a stroke (not too wide compared to its length)
  * Local angular compatibility (computed as in the initialization of the second step)
  * Relative proximity (comparing intra-cluster distances to inter-cluster distances)

## Things that could be applied to our problem

* Computing aggregate stroke from a pair of strokes by Moving Least Squares (Sec. 5). This gives a common reference to compare 2 strokes.
  * MLS: without needing to know the ordering between the input samples (points on the strokes to be aggregated), we can fit points that interpolate the input samples, and compute a target tangent for each point (= average tangent in a small neighborhood)
  * Find ordering between points by finding the largest path of the minimum spanning tree in the Euclidian proximity graph of the points (each point is connected to all its neighbors within a distance, except if the tangents are in opposite directions for some reason?)
  * Optimize points position so that the edges of the resulting polyline align with target tangents using iterated least-squares optimization
* Angular compatibility metric and correlation clustering method should be applicable to 3D strokes, provided we can:
  * compute the aggregate stroke for a stroke pair
  * find a good way to compare vectors in 3D: euclidean translation will not work for stroke tangents when the strokes lie on a curved surface. We would need some notion of parrallel transport of vectors in the sketch.
  * although from observation, in most cases simple translation may do the job? When the curves lie on a developpable surface or on parallel planes
