---
title: "Iso-Points: Optimizing Neural Implicit Surfaces with Hybrid Representations"
date: 2021-04-23T16:35:00+02:00
draft: false
tags: ["Paper"]
summary: "Introduce an efficient way to recover an explicit representation of an implicit surface in the form of 3D points on the surface to supervise training of deep neural networks that fit a neural implicit function to some data. They show that this helps reduce training time, be more robust to noise and yield result with better topology than without the iso-points supervision."
link: ""
---
