---
title: "Physical Guides"
date: 2020-10-01T18:51:24+02:00
draft: false
tags: ["Paper"]
summary: "Quantitative exploration of how accurately people can draw following a physical or virtual guide, depending on guide geometry."
link: "https://hci.rwth-aachen.de/publications/wacker2018a.pdf"
---

This paper measures user tracing accuracy in different conditions:

* Tracing on a virtual/physical object
* Tracing on a cylinder/cuboid object
* Tracing with no guidance/visual guidance/convex edge guidance/concave edge guidance

They observe a significatively better accuracy with physical objects as guide, and with using any type of guidance compared to no guidance at all. Surprisingly, concave geometry did not cause significantly better accuracy than convex geometry, or simple visual guidance.

They also show that it is easier to draw on the front on an object rather than on its back, both in virtual and physical case (the object and users were not allowed to move during the experiment).

**Takeaway**: Using physical objects as props to draw on poses some challenges

* Controller/pen shape to be able to draw as close to the object as possible
* User accuracy will depend on object geometry (but not so much as we could have thought), it is relatively good: ~5mm mean 3D deviation from target
* No quantitative insights about stroke quality, but the renderings of the strokes seems to show that some users did very poor strokes
* Seems like there would be some comfort issues around positioning oneself to draw wrt a fixed object (especially to draw always from the front of the surface). Maybe better if the object is allowed to be moved around?
* BUT there *is* some accuracy gain when using physical objects as guides (~10mm mean 3D deviation without guide VS ~5mm with guide)
