---
title: "Example Sketches"
date: 2020-07-03T18:54:41+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "A few sketches with completion time."
link: ""
---

{{< figure src="media/07-03/half-gamepad-5min.PNG" caption="Half a gamepad without buttons, 5min" width="400px">}}

{{< figure src="media/07-03/shoe-6min.PNG" caption="A shoe, 6min" width="300px">}}

{{< figure src="media/07-03/mouse-7min.PNG" caption="Computer mouse, 7min" width="400px">}}

{{< figure src="media/07-03/half-car-9min.PNG" caption="Half a car, 9min. This was really fun to draw." width="400px">}}

{{< figure src="media/07-03/armchair-10min.PNG" caption="Armchair, 10min. This was a huge pain to draw, I had lots of trouble figuring out how to draw the seat and arm rests without messing up the surface patch generation." width="400px">}}
