---
title: "3d Sketch and Surface Data"
date: 2020-10-08T15:00:06+02:00
draft: false
tags: ["Thoughts"]
summary: "List of potential sources of data to analyze relationship between an artist created sketch and surface."
link: ""
---

* "Where do people draw lines" - like study, but in 3D
* GravitySketch data
  * [Models publicly available on SketchFab](https://sketchfab.com/gravitysketch/models): needs to be sorted, a small portion of those are actually interesting.
  * [Workflow showcases](https://www.gravitysketch.com/workflows/): not sure whether the models are available somewhere, but it could be great to obtain some of them.
  * Demo models in the app: 8 models with both sketch and surface data. Can be exported to OBJ but curves would need to be converted to recover the centerline (exported as tubes) and there is no simple way to separate the different layers apparently... There is [a fancier way to export](https://youtu.be/i7-i_P5m95g) (curves as NURBS and surfaces as their control polygon but not currently available)

{{< figure src="media/10-09/GS-car-sketch.png" caption="" width="600px">}}

{{< figure src="media/10-09/GS-lines-surfaces.png" caption="" width="600px">}}

These sketches offer great insight into how artists depict surfaces with strokes, as we have "ground truth" data about the surfaces. It does raise the question as to whether the tool influenced the artists to draw in this way though. We can also wonder whether these sketches are the product of ideation and actually done before the surfaces, or whether they are just here as some kind of cosmetic showcase of GS features.

The GravitySketch line arts are a good showcase of how 3D strokes can represent a surface in different ways:

* the strokes can be thought as lying on the surface, or they are seen as a thin element (legs of the chair)
* they describe well-defined sections of the object, or they imply the surface by the position of smaller groups of strokes (helmet details)

There may be less benefits to carefully defining section strokes in 3D sketching compared to 2D sketching, as the viewer doesn't need as many visual cues to understand the 3D-ness of the object. Some users in the study also wished that they could do a less busy sketch.
