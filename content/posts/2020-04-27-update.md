---
title: "Update"
date: 2020-04-27T10:02:33+02:00
draft: false
tags: ["VR Sketching Project"]
summary: "Demos of new constrained fitting for curves"
link: ""
---

## Previous issues solved

Compared to the previous version, [the failure cases]({{< ref "posts/2020-03-12-current-issues-with-snapping" >}}) that we observed are now better behaved, as groups of constraints that are unlikely are rejected. Splitting of curves at intersection constraints is better implemented and issues linked to that are also gone (loss of G0 or G1 continuity).

{{< figure src="media/04-24/A-thing.gif" caption="" width="300px">}}

{{< figure src="media/04-24/S-curve.gif" caption="" width="300px">}}

## Bigger concept sketches

Because the beautification is more robust to dense networks, and thanks to the soft constraints such as tangent continuity and planarity that blend in well with the rest, it becomes easier to draw.

Below are some examples of short drawing sessions (accelerated x2 - x4 on the videos):

**Chair** - 1min30

{{< video src="media/04-24/chair-sketch.mp4" width="600px">}}

Tangent continuity at intersections and planarity (aligned with main axes of the scene) are used.

**Tent** - 4min

{{< video src="media/04-24/tent-sketch.mp4" width="600px">}}

Trying to get a sense of how different kinds of surfaces could be inferred via few strokes. The tent has different patches that are concave or convex and the boundaries between patches can have surface normal continuity accross them or not.

**Boat** - 8min

{{< video src="media/04-24/boat-sketch.mp4" width="600px">}}

It becomes hard to understand the shape when there are too many strokes and no occluding surfaces. Also it's difficult to draw smaller strokes (boat deck) at the default scale.

**Tree** - 10min

{{< video src="media/04-24/tree-sketch.mp4" width="600px">}}

This example is an attempt at drawing a more "organic" shape. Sometimes the tangent direction snapping is too aggressive.

## Next possible improvements on controlled curve sketching

* Better intersection snapping: favor snapping to existing intersections on curves, resolve for the right potential tangent direction better
* Weigh intersection constraints to favor some of them: those at an existing intersection or at a grid point, those at the endpoints of the stroke
* Allow for local undo of snapping: display all snapped properties on the stroke and allow user to cancel those that they didn't intend (could be a way to quantitatively evaluate the method or adjust the "magic numbers" that define the snapping thresholds)
* Grid: allow to hide/show grid and change its precision
* Zoom in/out
* Detect similarities/offsets between strokes?
* Fit circles/arcs
