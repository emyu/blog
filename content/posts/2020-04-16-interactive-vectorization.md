---
title: "Interactive Vectorization"
date: 2020-04-16T11:02:30+02:00
draft: false
tags: ["Paper"]
summary: "Semi automatic vectorization algorithm for arbitrary photographs that exposes high level controls such that novice users can locally refine the result."
link: "http://www.clairexie.org/interactive_vectorization/index.html"
---

## Method

This method first uses existing algorithms to find a connected graph where the nodes are the junctions of 3 contour edges (detected from the input image).

From this initial result, the user is provided with 3 tools that are represented as brushes that they can use to draw on the image: "split" brush to split regions into smaller sub regions, "merge" brush for the inverse operation, "smooth" brush to smooth edges.

The split brush actually has 2 possible modes: paint over a whole region to indicate that it should be subdivided, or paint a desired new edge. It is automatically detected in which mode the tool is intended to be used, via heuristics and observations on the input stroke.

## User study

The task: 12 novice users have to vectorize 2 input images as fast as possible, while seeing a reference target vectorization. They were familiarized with the tool before and shown some high-level methods to better the workflow.

They use expert users vectorizing the same images on Illustrator as baseline to compare with. They compare errors between each result and the target vectorization (Hausdorff distance), and completion times.

Additionally, they gather observations on how the different tools were used.

They state that there is no direct comparison between their tool and a baseline in this experiment.

