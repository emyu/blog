for file in $(find ./content/posts -type f -name '*.md'); do
    echo $file
    sed -i'.original' 's:posts\/(?=\d\d-\d\d-):posts/2020-:g' "$file"
    # sed -i'.original' 's/"rValue":72,"gValue":48,"bValue":230/"rValue":255,"gValue":143,"bValue":104/g' "$file"
    rm "${file}.original"
done