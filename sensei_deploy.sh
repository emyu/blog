

sessionID=$(echo $1)

# Build with corect baseUrl
baseUrl="https://$sessionID.devbox.training.adobesensei.io/blog/"
hugo --baseUrl $baseUrl --destination sensei_build

# Copy build folder to sensei session
# First zip
zip -r sensei_build.zip sensei_build

scp sensei_build.zip $sessionID:/sensei-fs/users/emiliey/blog_server

# SSH into devbox, unzip and replace content
ssh $sessionID 'cd /sensei-fs/users/emiliey/blog_server ; rm -rf blog ; unzip /sensei-fs/users/emiliey/blog_server/sensei_build.zip ; mv sensei_build blog'
